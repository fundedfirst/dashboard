<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CompanyDetails extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'company_name',
        'ein_number',
        'company_address',
        'city',
        'state_of_incorporation',
        'zip_code'
    ];
}
