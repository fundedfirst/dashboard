<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use App\Scopes\SkipDeletedRecord;
use Str;
use App\Notifications\EmailVerificationNotification;
use Illuminate\Support\Facades\Storage;



class User extends Authenticatable implements MustVerifyEmail
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name',
        'last_name',
        'role_id',
        'email',
        'password',
        'profile_image',
        'phone_number',
        'status',
        'phone_verification_code',
        'phone_verification_code_expiry',
        'remember_token',
        'uuid',
        'stripe_public_key',
        'stripe_secret_key',
        'plaid_public_key',
        'plaid_secret_key',
        'webhook_url',
        'denefit_token',
        'finmkt_key',
        'tier1_toggled',
        'tier2_toggled'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
    protected $appends = ['full_name', 'role'];
    public function getFullNameAttribute()
    {
        return "{$this->first_name} {$this->last_name}";
    }

    public static function boot()
    {
        parent::boot();
        self::creating(function ($model) {
            $model->uuid = Str::uuid();
        });
    }
    public function getStatusAttribute($key)
    {
        return ucwords($key);
    }
    public function getRoleAttribute()
    {
        $role = Role::whereId($this->role_id)->first();
        if ($role->name == 'super_admin') {
            return 'super_admin';
        }
        if ($role->name == 'admin' || $role->name == 'Admin') {
            return 'admin';
        } else {
            return 'client';
        }
    }
    public function getProfileImageAttribute($key)
    {
        // $foldername = '';
        // $role = Role::whereId($this->role_id)->first();
        // if($role->name == 'super_admin'){
        //     $foldername = 'admin';
        // }
        // if($role->name == 'admin' || $role->name == 'Admin' ){
        //     $foldername = 'admin';
        // }
        // if($role->name == 'client'){
        //     $foldername = 'client';
        // }
        // if($key){
        // }
        // else{
        //     return "";
        // }
        if ($key) {
            return Storage::disk('s3')->temporaryUrl($key, now()->addMinutes(30));
        }
    }
    protected static function booted()
    {
        static::addGlobalScope(new SkipDeletedRecord);
    }

    public function sendPasswordResetNotification($token)
    {
        $url = $token;
        $this->notify(new EmailVerificationNotification($url));
    }
}
