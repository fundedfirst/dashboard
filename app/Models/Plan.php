<?php

namespace App\Models;

use App\Scopes\SkipDeletedRecord;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Plan extends Model
{
    use HasFactory;
    protected $fillable = ['terms_and_conditions','success_url', 'down_payment', 'number_of_payment', 'youtube_video_embed_code', 'user_id', 'name', 'price', 'description', 'status', 'image'];
    protected static function booted()
    {
        static::addGlobalScope(new SkipDeletedRecord);
    }
    public function getStatusAttribute($key){
        return ucwords($key);
    }
    public function getImageAttribute($key){
        // if($key){
        //     return URL("images/client/")."/".$key;
        // }
        // else{
        //     return "";
        // }
        if($key) {
            return Storage::disk('s3')->temporaryUrl($key, now()->addMinutes(30));
        }
        return;
    }
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
