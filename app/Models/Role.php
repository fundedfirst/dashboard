<?php

namespace App\Models;
use App\Scopes\SkipDeletedRecord;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    use HasFactory;
    protected $fillable = ['name', 'status'];

    public function getStatusAttribute($key){
        return ucwords($key);
    }
    public function users()
    {
        return $this->hasMany(User::class);
    }
    protected static function booted()
    {
        static::addGlobalScope(new SkipDeletedRecord);
    }
}
