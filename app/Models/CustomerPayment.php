<?php

namespace App\Models;

use App\Scopes\SkipDeletedRecord;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CustomerPayment extends Model
{
    use HasFactory;
    protected $fillable = ['customer_id', 'plan_id', 'amount', 'type', 'status', 'finmkt_loan_id'];
    protected static function booted()
    {
        static::addGlobalScope(new SkipDeletedRecord);
    }
    public function getStatusAttribute($key){
        return ucwords($key);
    }
    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }


    public function plan()
    {
        return $this->belongsTo(Plan::class);
    }

}
