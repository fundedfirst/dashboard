<?php

namespace App\Models;

use App\Scopes\SkipDeletedRecord;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    use HasFactory;
    protected $fillable = ['user_id', 'first_name', 'last_name', 'email', 'address', 'zip_code', 'dob', 'phone', 'status'];
    protected $appends = ['full_name'];
    protected static function booted()
    {
        static::addGlobalScope(new SkipDeletedRecord);
    }
    public function getFullNameAttribute(){
        return "{$this->first_name} {$this->last_name}";
    } 
    public function getStatusAttribute($key){
        return ucwords($key);
    }
    public function user()
    {
        return $this->belongsTo(User::class);
    }
    public function customerpayments()
    {
        return $this->hasMany(CustomerPayment::class);
    }
    

}
