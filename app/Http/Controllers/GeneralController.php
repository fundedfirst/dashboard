<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Customer;
use App\Models\CustomerPayment;
use App\Models\Plan;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use DB;
use Validator;
use Hash;
use Mail;

class GeneralController extends Controller
{
    public function chooseLoign()
    {
        return view('index');
    }

    public function tire1PlanFinmktFail(Request $request)
    {
        return view('iframe.failed');
    }

    public function noTirePlanFail(Request $request)
    {
        return view('iframe.notirefail');
    }


    public function storePlan(Request $request)
    {

        // dd($request);

        $validator = Validator::make($request->all(), [
            'title'  => 'required|string',
            'price' => 'required|integer',
            'first_name' => 'required|string',
            "last_name" => 'required|string',
            "email" => 'required|string',
            'key' => 'required|string'
        ]);

        if ($validator->fails()) {
            // return redirect()->back()->withErrors($validator)->withInput();
            return view('iframe.failed');
        }

        $price = $request->price;
        $title = $request->title;
        $first_name = $request->first_name;
        $last_name = $request->last_name;
        $email = $request->email;
        $key = $request->key;

        $user = User::where('is_dynamic_price_enabled', '=', '1')->where('dynamic_price_key', '=', $key)->first();


        if (!empty($user)) {
            $data['details'] = array(
                'price' => $price,
                "title" => $title,
                'first_name' => $first_name,
                'last_name' => $last_name,
                'email' => $email
            );
            return view('iframe.store', ['data' => $data]);
        } else {
            return view('iframe.failed');
        }
    }

    public function planFinmktFail(Request $request)
    {

        $data_get = [];
        foreach ($request->input() as $key => $v) {
            $data_get[str_replace('amp;', '', $key)] = $v;
        }

        // $data_get['service_amount'] = 90700;
        $data_get['service_array'] = [['service' => $data_get['service_array'][0]['service'], 'price' => (int)$data_get['service_array'][0]['price']]];
        // $data_get['client_id']  = 2;
        // $data_get['downpayment_amount']  = 90200;
        $tempArr = array(
            'service_amount' => (int)$data_get['service_amount'],
            'downpayment_amount' => (int)$data_get['downpayment_amount'],
            'number_of_payments' => (int)$data_get['number_of_payments'],
            'start_date' => $data_get['start_date'],
            'customer_email' => $data_get['customer_email'],
            'customer_first_name' => $data_get['customer_first_name'],
            'customer_last_name' => $data_get['customer_last_name'],
            'customer_address' => $data_get['customer_address'],
            'date_of_birth' => $data_get['date_of_birth'],
            'zipcode' => $data_get['zipcode'],
            'customer_mobile' => $data_get['customer_mobile'],
            'service_array' => $data_get['service_array'],
            'plan_id' => $data_get['plan_id'],
            'payment_id' => $data_get['payment_generated_id'],
            'finmkt' => 'yes',
        );

        $token = '';
        $plan_data = Plan::find($data_get['plan_id']);
        if ($plan_data) {
            $user = User::find($plan_data->user_id);
            $token = $user->denefit_token ?? '';
        }

        $url = "https://endpoint.denefits.com/v1/payment_plan/receiveDetails?token=$token";
        $plancontroller  = new PlanController();
        $response = $plancontroller->paymentRequestDeneift($url, $tempArr, "POST", $token);
        echo"<pre>";
        print_r($response);die();


        if (count($response)) {
            if (isset($response['code'])) {
                if ($response['code'] == 'success') {
                    $iframeUrl          = $response['data']['payment_link'];
                    $data['iframe_url'] = $iframeUrl;

                    //add customer
                    $customer = Customer::whereEmail($data_get['customer_email'])->first();
                    if ($customer) {
                        $customerId = $customer->id;
                        Customer::find($customer->id)->update([
                            'first_name' => $data_get['customer_first_name'],
                            'last_name' => $data_get['customer_last_name'],
                        ]);
                    } else {
                        $customerDataAdd['user_id']     = $data_get['client_id'];
                        $customerDataAdd['first_name']  = $data_get['customer_first_name'];
                        $customerDataAdd['last_name']   = $data_get['customer_last_name'];
                        $customerDataAdd['email']       = $data_get['customer_email'];
                        $customerDataAdd['address']     = $data_get['customer_address'];
                        $customerDataAdd['zip_code']    = $data_get['zipcode'];
                        $customerDataAdd['phone']       = $data_get['customer_mobile'];
                        $customerDataAdd['status']      = 'active';
                        $customer = Customer::create($customerDataAdd);
                        $customerId = $customer->id;
                    }

                    $customerPayment = CustomerPayment::whereCustomerId($customerId)->whereStatus('pending');

                    if (!$customerPayment->exists()) {
                        CustomerPayment::create(['customer_id' => $customerId, 'plan_id' => $data_get['plan_id'], 'status' => 'pending']);
                    }

                    $data['customer_id'] = $customer->id;
                    $data['service_amount'] = $data_get['service_amount'];

                    //webhook
                    $plan = Plan::whereId($data_get['plan_id'])->first();
                    if ($plan) {
                        $user = User::whereId($plan->user_id)->first();
                        if ($user) {
                            $webhookUrl = $user->webhook_url;
                            if ($webhookUrl) {
                                $customerDataAdd['first_name']  = $data_get['customer_first_name'];
                                $customerDataAdd['last_name']   = $data_get['customer_last_name'];
                                $customerDataAdd['email']       = $data_get['customer_email'];
                                $customerDataAdd['address']     = $data_get['customer_address'];
                                $customerDataAdd['zip_code']    = $data_get['zipcode'];
                                $customerDataAdd['phone']       = $data_get['customer_mobile'];
                                $customerDataAdd['name']        = $data_get['customer_first_name'] . " " . $data_get['customer_last_name'];
                                $customerDataAdd['offer_name']  = $plan->name;
                                $customerDataAdd['offer_price'] = $plan->price;

                                $plancontroller->zapCurl($webhookUrl, $customerDataAdd, "POST");
                            }
                        }
                    }

                    return view('iframe.payiframe', ['data' => $data]);
                }
            }
        }

        return redirect(route('planiframe', ['id' => $data_get['plan_id']]))->with('message', $response['message']);
    }



    public function planFinmktSuccess(Request $request)
    {


        $data_get = [];
        foreach ($request->input() as $key => $v) {
            $data_get[str_replace('amp;', '', $key)] = $v;
        }

        if ($data_get['payment_generated_id']) {

            CustomerPayment::where([
                ['id', '=', $data_get['payment_generated_id']]
            ])->update(['status' => 'paid']);

            $c = CustomerPayment::where([
                ['id', '=', $data_get['payment_generated_id']]
            ])->first();
            if ($c) {

                $plan_data = Plan::where('id', $c->plan_id)->first();
                if ($plan_data) {
                    // dd($plan_data);
                    return     redirect($plan_data->success_url);
                }
            }
        }

        echo '<center>No Page Found</center>';
        // return redirect(route('home'));


    }



    public function validateEmail($email)
    {

        //true mail
        $url = "https://truemail.io/api/v1/verify/single?address_info=1&timeout=100&access_token=AntSh6Qa5vGQ1FJqSKHmEuJkHHY14sNZNdRiaf1SIktF8Dcjp1sus7jVpO5zeZ2U&email=$email";

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        // execute!
        $response = curl_exec($ch);

        $response = json_decode($response, true);

        // close the connection, release resources used
        curl_close($ch);

        // do anything you want with your response
        return $response['result'];
        // return 'valid';

    }
}
