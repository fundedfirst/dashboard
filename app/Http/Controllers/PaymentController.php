<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Customer;
use App\Models\CustomerPayment;
use Illuminate\Http\Request;

class PaymentController extends Controller
{
    public function addWebhookPayment(Request $request){
        $payload = @file_get_contents('php://input');
        $event = json_decode($payload, true);
        
        Log::info('denefit webhook response start');
        Log::info($request->all());
        Log::info('denefit webhook response end');


        if(count($event)){
            if(count($event['data'])){
                if(count($event['data']['contract'])){
                    $firstName  = $event['data']['contract']['customer_first_name'];
                    $lastName   = $event['data']['contract']['customer_last_name'];
                

                    $customer = Customer::where(['first_name'=> $firstName, 'last_name'=> $lastName])->first();
                    if($customer){
                        $dataToAdd['customer_id']   = $customer->id;
                        $dataToAdd['amount']        = $event['data']['contract']['financed_amount'];
                        $dataToAdd['type']          = 'installment';
                        $dataToAdd['status']        = 'paid';
                        $cPayment = CustomerPayment::create($dataToAdd);
                        if($cPayment){
                            echo 'Payment added.';
                        }
                        else{
                            echo "Error adding payment.";
                        }

                    }
                    else{
                        echo "Customer not found.";
                    }

                }

            }

        }
    }
   
}
