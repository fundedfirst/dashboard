<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use App\Models\Customer;
use App\Models\CustomerPayment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use DB;
class DashboardController extends Controller
{
    public function index(){
        $authUserId     = auth()->user()->id;
        $customerModel = new Customer();
        $customers      = $customerModel::whereUserId($authUserId)->count();
        $customerIds    = $customerModel::whereUserId($authUserId)->pluck('id');
        $payments       = CustomerPayment::whereIn('customer_id', $customerIds)->whereStatus('paid')->sum('amount'); 
        $customersWithodPayments = CustomerPayment::whereIn('customer_id', $customerIds)->whereStatus('pending')->count();
        
        $data['totalPayments']  = $payments;
        $data['totalCustomers'] = $customers;
        $data['abandonedCarts']  = $customersWithodPayments;

        return view('client.dashboard', ['data'=> $data]);
    }
    public function home(){
        $authUser =Auth()->user();
        return view('client.index');
    }
    
}