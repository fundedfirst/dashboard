<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Customer;
use App\Models\Plan;
use App\Models\Role;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Validator;
use DB;
use Stripe;
use Illuminate\Support\Facades\Hash;

class StripController extends Controller
{
    public function event(Request $request)
    {
        $payload  = @file_get_contents('php://input');
        $event    = json_decode($payload, true);
        $customerId = $event['customer']??die();
        // cus_Kv8Lhle9V0yYgo
        $stripe = new \Stripe\StripeClient(env('STRIPE_SECRET'));
       
        try {
            $customer = $stripe->customers->retrieve(
                $customerId,
                []
            );

            $customerEmail   = $customer->email;
            $customerName    = $customer->name;
            $customerPhone   = $customer->phone;

            if (User::whereEmail($customerEmail)->first()) {
                die('Email already exists!');
            }

            $nameParts = explode(" ", $customerName);
            if (count($nameParts) > 1) {
                $firstname = $nameParts[0];
                array_shift($nameParts);
                $lastname = implode(" ", $nameParts);
            } else {
                $firstname =  $customerName;
                $lastname = " ";
            }

            $role                       = Role::whereName('client')->first();
            $dataToAdd['role_id']       = $role->id;
            $dataToAdd['first_name']    = $firstname;
            $dataToAdd['last_name']     = $lastname;
            $dataToAdd['email']         = $customerEmail;
            $dataToAdd['phone_number']  = $customerPhone;
            $dataToAdd['password']      = Hash::make('asd123$A');;
            $addClient     = User::create($dataToAdd);
            if ($addClient) {
               echo "<pre>"; print_r($addClient);die;
            }
            die('Error adding Client!');
        } catch (\Throwable $th) {
            die('Oops! something went wrong.');
        }
    }

}
