<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Models\User;
use Validator;
use Illuminate\Support\Facades\Storage;


class ClientController extends Controller
{
   public function myProfile()
   {
      return view('client.profile.index');
   }
   public function editMyProfile()
   {
      return view("client.profile.edit");
   }
   public function updateMyProfile(Request $request)
   {
      $validator = Validator::make($request->all(), [
         'first_name'         => 'required|string|max:255',
         'last_name'          => 'required|string|max:255',
         'stripe_public_key'  => 'max:255',
         'stripe_secret_key'  => 'max:255',
         'plaid_public_key'   => 'max:255',
         'plaid_secret_key'   => 'max:255',
         'webhook_url'        => 'max:255',
         'denefit_token'      => 'max:255',
         'finmkt_key'         => 'max:255',
         'profile_image' => 'mimes:jpeg,jpg,png,gif'

      ]);

      if ($validator->fails()) {
         return redirect()->back()->withErrors($validator)->withInput();
      }
      if ($request->hasFile('profile_image')) {

         // $image            = $request->file('profile_image');
         // $extension        = $image->getClientOriginalExtension();
         // $ImgName          = $image->getClientOriginalName();
         // $fileNameWithoutEx= pathinfo($ImgName, PATHINFO_FILENAME);
         // $destinationPath  = public_path("/images/client");

         // if (!file_exists($destinationPath)) {
         //    //create folder
         //    mkdir($destinationPath, 0755, true);
         // }

         // $time = time();
         // $image->move($destinationPath, $fileNameWithoutEx . "_" . $time . "." . $extension);
         // $fullImagePath = $fileNameWithoutEx . "_" . $time . "." . $extension;
         // $dataToUpdate['profile_image'] = $fullImagePath;

         $dataToUpdate['profile_image'] = Storage::disk('s3')->putFile('client', $request->file('profile_image'));
      }
      $dataToUpdate['first_name']         = $request->first_name;
      $dataToUpdate['last_name']          = $request->last_name;
      // $dataToUpdate['stripe_public_key']  = $request->stripe_public_key;
      // $dataToUpdate['stripe_secret_key']  = $request->stripe_secret_key;
      // $dataToUpdate['plaid_public_key']   = $request->plaid_public_key;
      // $dataToUpdate['plaid_secret_key']   = $request->plaid_secret_key;
      // $dataToUpdate['webhook_url']        = $request->webhook_url;
      // $dataToUpdate['denefit_token']      = $request->denefit_token;
      // $dataToUpdate['finmkt_user_id']      = $request->finmkt_user_id;
      // $dataToUpdate['finmkt_base_url']      = $request->finmkt_base_url;
      // $dataToUpdate['finmkt_username']      = $request->finmkt_username;
      // $dataToUpdate['finmkt_password']      = $request->finmkt_password;

      $update = User::whereId(auth()->user()->id)->update($dataToUpdate);
      return redirect()->back()->with('success', 'Profile updated successfully.');
   }


   public function store()
   {
      return view("client.store");
   }



   public function logout()
   {
      Auth::guard('user')->logout();
      return redirect()->route('home');
   }
}
