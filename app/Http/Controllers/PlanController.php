<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Customer;
use App\Models\CustomerPayment;
use App\Models\Plan;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Auth;
use Validator;
use DB;

class PlanController extends Controller
{
    private $user;

    public function __construct()
    {
    }

    public function index()
    {
        $data['plans'] = Plan::orderBy('id', 'DESC')->whereUserId(auth()->user()->id)->simplePaginate(10);
        return view('client.plan.index', ['data' => $data]);
    }

    public function store(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'name'  => 'required|string|max:255',
            'price' => 'required|integer',
            'image' => 'image|mimes:jpeg,png,jpg',
            "youtube_video_embed_code" => 'url'
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        //validate Url for embeded url field 
        $url = $request->youtube_video_embed_code;
        $YouTubeUrl = 'https://www.youtube.com/';
        $VistiaUrl = 'https://rmreal.wistia.com/';
        $VadooUrl = 'https://api.vadoo.tv';
        $VimieoUrl = 'https://player.vimeo.com/';
        $dropbox = "https://www.dropbox.com/";
        $Gdrive = "https://drive.google.com/";

        if (str_contains($url, $YouTubeUrl) || str_contains($url, $VistiaUrl) || str_contains($url, $VadooUrl) || str_contains($url, $VimieoUrl) || str_contains($url, $dropbox) || str_contains($url, $Gdrive)) {

            $YouTubeUrlCheck = "embed/";
            if (str_contains($url, $YouTubeUrl) && str_contains($url, $YouTubeUrlCheck) == false) {
                return redirect()->back()->with('error', 'Provide valid Youtube URL');
            }

            // Validate Vimieo
            $VimieoUrlCheck = 'video/';

            if (str_contains($url, $VimieoUrl) && str_contains($url, $VimieoUrlCheck) == false) {
                return redirect()->back()->with('error', 'Provide valid Vimieo URL.');
            }

            // Validate Vadoo
            $VadooUrlCheck = '/landing_page';

            if (str_contains($url, $VadooUrl) && str_contains($url, $VadooUrlCheck) == false) {
                return redirect()->back()->with('error', 'Provide valid Vadoo URL.');
            }

            // Validate Vistia
            $VistiaUrlCheck = 'medias/';

            if (str_contains($url, $VistiaUrl) && str_contains($url, $VistiaUrlCheck) == false) {
                return redirect()->back()->with('error', 'Provide valid Vistia URL.');
            }

            // Validate dropbox
            $dropboxUrlCheck = '?dl=0';

            if (str_contains($url, $VistiaUrl) && str_contains($url, $dropboxUrlCheck) == false) {
                return redirect()->back()->with('error', 'Provide valid Dropbox URL.');
            }
            // Validate google-drive
            $googleUrlCheck = '?usp=sharing';

            if (str_contains($url, $VistiaUrl) && str_contains($url, $googleUrlCheck) == false) {
                return redirect()->back()->with('error', 'Provide valid Google drive URL.');
            }
        } else {
            return redirect()->back()->with('error', 'Invalid embed url or code');
        }

        // validate YouTube

        $dataToAdd['success_url'] = $request->success_url ?? '';
        // $dataToAdd['vsl_link'] = $request->vsl_link ?? '';
        $dataToAdd['status'] = ($request->status);
        $dataToAdd['name'] = $request->name;
        $dataToAdd['price'] = $request->price;
        $dataToAdd['description'] = $request->description;
        $dataToAdd['down_payment'] = $request->down_payment;
        $dataToAdd['number_of_payment'] = $request->number_of_payment;
        $dataToAdd['youtube_video_embed_code'] = $request->youtube_video_embed_code;
        $dataToAdd['terms_and_conditions'] = $request->terms_and_conditions;
        $dataToAdd['user_id'] = auth()->user()->id;
        $dataToAdd['created_at'] = date('Y-m-d H:i:s');
        $dataToAdd['updated_at'] = date('Y-m-d H:i:s');

        if ($request->hasFile('image')) {

            // $image = $request->file('image');
            // $extension = $image->getClientOriginalExtension();
            // $ImgName = $image->getClientOriginalName();
            // $fileNameWithoutEx = pathinfo($ImgName, PATHINFO_FILENAME);
            // $destinationPath = public_path("/images/client");

            // if (!file_exists($destinationPath)) {
            //     //create folder
            //     mkdir($destinationPath, 0755, true);
            // }

            // $time = time();
            // $image->move($destinationPath, $fileNameWithoutEx . "_" . $time . "." . $extension);
            // $fullImagePath = $fileNameWithoutEx . "_" . $time . "." . $extension;
            // $dataToAdd['image'] = $fullImagePath;

            $dataToAdd['image'] = Storage::disk('s3')->putFile('client', $request->file('image'));
        }


        $add = Plan::create($dataToAdd);
        return redirect()->route('clientplans.index')->with('success', 'Plan added successfully.');
    }




    public function create()
    {
        return view('client.plan.create');
    }






    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name'  => 'required|string|max:255',
            'price' => 'required|integer',
            'image' => 'image|mimes:jpeg,png,jpg'
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $plan               = Plan::findOrFail($id);
        $plan->name         = $request->name;
        $plan->price        = $request->price;
        $plan->description  = $request->description;
        $plan->down_payment = $request->down_payment;
        $plan->number_of_payment         = $request->number_of_payment;
        $plan->youtube_video_embed_code  = $request->youtube_video_embed_code;
        $plan->terms_and_conditions       = $request->terms_and_conditions;
        $plan->status       = $request->status;
        $plan->success_url = $request->success_url ?? '';
        // $plan->vsl_link = $request->vsl_link ?? '';
        if ($request->hasFile('image')) {

            // $image = $request->file('image');
            // $extension = $image->getClientOriginalExtension();
            // $ImgName = $image->getClientOriginalName();
            // $fileNameWithoutEx = pathinfo($ImgName, PATHINFO_FILENAME);
            // $destinationPath = public_path("/images/client");

            // if (!file_exists($destinationPath)) {
            //     //create folder
            //     mkdir($destinationPath, 0755, true);
            // }

            // $time = time();
            // $image->move($destinationPath, $fileNameWithoutEx . "_" . $time . "." . $extension);
            // $fullImagePath = $fileNameWithoutEx . "_" . $time . "." . $extension;
            $plan->image = Storage::disk('s3')->putFile('client', $request->file('image'));
        }
        $plan->save();
        if ($plan) {
            return redirect()->back()->with('success', 'Plan updated successfully.');
        }
        return redirect()->back()->with('error', 'Error updating Plan.');
    }


    public function edit($id)
    {
        $data['plan'] = Plan::findOrFail($id);
        return view('client.plan.edit', ['data' => $data]);
    }




    public function show($id)
    {
        $data['plan'] = Plan::findOrFail($id);
        return view('client.plan.show', ['data' => $data]);
    }


    public function destroy($id)
    {
        $plan = Plan::findOrFail($id);
        $plan->status = 'deleted';
        $plan->save();
        if ($plan) {
            return redirect()->route('clientplans.index')->with('success', "Plan - $plan->name deleted successfully.");
        }
        return redirect()->back()->with('error', 'Error deleting Plan.');
    }


    public function planIframe($id)
    {
        $data['plan'] = Plan::with('user')->findOrFail($id);
        // echo "<pre>";
        // print_r($data['plan']);die();
        return view('iframe.plan', ['data' => $data]);
    }


    public function planIframeAdd(Request $request)
    {


        $token = $request->denefit_token;
        $amount  = (int)$request->service_amount;

        session()->put('service_amount', $amount);

        // $url = 'https://endpoint.denefits.com/v1/payment/receiveDetails?token=b4c199fbe1ccfc2ee702e70a0a8e65ae';
        // $url = 'https://endpoint.denefits.com/v1/payment_process/receiveDetails?token=b4c199fbe1ccfc2ee702e70a0a8e65ae';
        $url = "https://endpoint.denefits.com/v1/payment_plan/receiveDetails?token=$token";

        $date = date('Y-m-d');
        $start_date = date('Y-m-d', strtotime($date . ' + 30 days'));

        $dateOfBirth        = $request->date_of_birth;
        $dateOfBirth        = str_replace('. ', '-', $date);
        $dateOfBirth        = date_create($dateOfBirth);
        $finalDateOfBirth = date_format($dateOfBirth, "Y-m-d");

        $tempArr = array(
            'service_amount' => $request->service_amount,
            'downpayment_amount' => (int)$request->down_payment,
            'number_of_payments' => (int)$request->number_of_payment,
            'start_date' => $start_date,
            'customer_email' => $request->customer_email,
            'customer_first_name' => $request->customer_first_name,
            'customer_last_name' => $request->customer_last_name,
            'customer_address' => $request->customer_address,
            'date_of_birth' => date('Y-m-d', strtotime($finalDateOfBirth)),
            'zipcode' => $request->zipcode,
            'customer_address' => $request->customer_address,
            'customer_mobile' => $request->customer_mobile,
            'client_id' => $request->client_id ?? 0,
            'service_array' => array(array('service' => 'Testing', 'price' => $amount)),
            'plan_id' => $request->plan_id
        );
        // return $dataToAdd = json_encode($tempArr);

        if (!$request->has('finmkt')) {
            $finmkt = $this->finmktPayment($tempArr, ['success' => '', 'fail' => '']);
        }

        $response = $this->paymentRequestDeneift($url, $tempArr, "POST", $token);


        if (count($response)) {
            if (isset($response['code'])) {
                if ($response['code'] == 'success') {
                    $iframeUrl          = $response['data']['payment_link'];
                    $data['iframe_url'] = $iframeUrl;

                    //add customer
                    $customer = Customer::whereEmail($request->customer_email)->first();
                    if ($customer) {
                        $customerId = $customer->id;
                        Customer::find($customer->id)->update([
                            'first_name' => $request->customer_first_name,
                            'last_name' => $request->customer_last_name,
                        ]);
                    } else {
                        $customerDataAdd['user_id']     = $request->client_id;
                        $customerDataAdd['first_name']  = $request->customer_first_name;
                        $customerDataAdd['last_name']   = $request->customer_last_name;
                        $customerDataAdd['email']       = $request->customer_email;
                        $customerDataAdd['address']     = $request->customer_address;
                        $customerDataAdd['zip_code']    = $request->zipcode;
                        $customerDataAdd['phone']       = $request->customer_mobile;
                        $customerDataAdd['status']      = 'active';
                        $customer = Customer::create($customerDataAdd);
                        $customerId = $customer->id;
                    }

                    $customerPayment = CustomerPayment::whereCustomerId($customerId)->whereStatus('pending');

                    if (!$request->has('finmkt')) {
                        if (!$customerPayment->exists()) {
                            CustomerPayment::create(['customer_id' => $customerId, 'plan_id' => $request->plan_id, 'status' => 'pending', 'finmkt_loan_id' => $finmkt['loanId']]);
                        } else {
                            $customerPayment->first()->update(['finmkt_loan_id' => $finmkt['loanId']]);
                        }
                    } else {
                        if (!$customerPayment->exists()) {
                            CustomerPayment::create(['customer_id' => $customerId, 'plan_id' => $request->plan_id, 'status' => 'pending']);
                        }
                    }

                    if (!$customerPayment->exists()) {
                        CustomerPayment::create(['customer_id' => $customerId, 'plan_id' => $request->plan_id, 'status' => 'pending']);
                    }

                    $data['customer_id'] = $customer->id;
                    $data['service_amount'] = $request->service_amount;

                    if (!$request->has('finmkt')) {
                        $data['finmkt_redirect_url'] = $finmkt['redirectUrl'];
                        $data['finmkt_loan_id'] = $finmkt['loanId'];
                    }

                    //webhook
                    $plan = Plan::whereId($request->plan_id)->first();
                    if ($plan) {
                        $user = User::whereId($plan->user_id)->first();
                        if ($user) {
                            $webhookUrl = $user->webhook_url;
                            if ($webhookUrl) {
                                $customerDataAdd['first_name']  = $request->customer_first_name;
                                $customerDataAdd['last_name']   = $request->customer_last_name;
                                $customerDataAdd['email']       = $request->customer_email;
                                $customerDataAdd['address']     = $request->customer_address;
                                $customerDataAdd['zip_code']    = $request->zipcode;
                                $customerDataAdd['phone']       = $request->customer_mobile;
                                $customerDataAdd['name']        = $request->customer_first_name . " " . $request->customer_last_name;
                                $customerDataAdd['offer_name']  = $plan->name;
                                $customerDataAdd['offer_price'] = $plan->price;

                                $this->zapCurl($webhookUrl, $customerDataAdd, "POST");
                            }
                        }
                    }

                    if (!$request->finmkt) {
                        return redirect($data['finmkt_redirect_url']);
                    }

                    return view('iframe.payiframe', ['data' => $data]);
                }
            }
        }
    }




    public function planIframeAddNew(Request $request)
    {

        //dd($request);
        // echo "<pre>";
        // print_r($request);die();
        $token = $request->denefit_token;
        $amount  = (int)$request->service_amount;

        $tier1IsToggled =  $request->tier1_toggle;
        $tier2IsToggled =  $request->tier2_toggle;

        session()->put('service_amount', $amount);


        // $url = 'https://endpoint.denefits.com/v1/payment/receiveDetails?token=b4c199fbe1ccfc2ee702e70a0a8e65ae';
        // $url = 'https://endpoint.denefits.com/v1/payment_process/receiveDetails?token=b4c199fbe1ccfc2ee702e70a0a8e65ae';
        $url = "https://endpoint.denefits.com/v1/payment_plan/receiveDetails?token=$token";

        $date = date('Y-m-d');
        $start_date = date('Y-m-d', strtotime($date . ' + 30 days'));

        $dateOfBirth        = $request->date_of_birth;
        $dateOfBirth        = str_replace('. ', '-', $date);
        $dateOfBirth        = date_create($dateOfBirth);
        $finalDateOfBirth = date_format($dateOfBirth, "Y-m-d");

        $tempArr = array(
            'service_amount' => $request->service_amount,
            'downpayment_amount' => (int)$request->down_payment,
            'number_of_payments' => (int)$request->number_of_payment,
            'start_date' => $start_date,
            'customer_email' => $request->customer_email,
            'customer_first_name' => $request->customer_first_name,
            'customer_last_name' => $request->customer_last_name,
            'customer_address' => $request->customer_address,
            'date_of_birth' => $finalDateOfBirth,
            'zipcode' => $request->zipcode,
            'client_id' => $request->client_id ?? 0,
            'customer_mobile' => $request->customer_mobile,
            'service_array' => array(array('service' => 'Testing', 'price' => $amount)),
            'plan_id' => $request->plan_id
        );
        // return $dataToAdd = json_encode($tempArr);

        if ($tier1IsToggled && $tier2IsToggled) {
            $customer = Customer::whereEmail($request->customer_email)->first();
            if ($customer) {
                $customerId = $customer->id;
                Customer::find($customer->id)->update([
                    'first_name' => $request->customer_first_name,
                    'last_name' => $request->customer_last_name,
                ]);
            } else {
                $customerDataAdd['user_id']     = $request->client_id;
                $customerDataAdd['first_name']  = $request->customer_first_name;
                $customerDataAdd['last_name']   = $request->customer_last_name;
                $customerDataAdd['email']       = $request->customer_email;
                $customerDataAdd['address']     = $request->customer_address;
                $customerDataAdd['zip_code']    = $request->zipcode;
                $customerDataAdd['phone']       = $request->customer_mobile;
                $customerDataAdd['status']      = 'active';
                $customer = Customer::create($customerDataAdd);
                $customerId = $customer->id;
            }
            $customerPayment = CustomerPayment::create(['customer_id' => $customerId, 'plan_id' => $request->plan_id, 'status' => 'pending', 'finmkt_loan_id' => '']);
            $tempArr['payment_generated_id'] = $customerPayment->id;

            $finmkt = $this->finmktPayment($tempArr, ['success' => route('plan.finmkt.success', $tempArr), 'fail' => route('plan.finmkt.fail', $tempArr)]);

            CustomerPayment::where('id', $customerPayment->id)->update(['finmkt_loan_id' => $finmkt['loanId']]);

            $data['customer_id'] = $customer->id;
            $data['service_amount'] = $request->service_amount;

            $data['finmkt_redirect_url'] = $finmkt['redirectUrl'];
            $data['finmkt_loan_id'] = $finmkt['loanId'];

            //webhook
            $plan = Plan::whereId($request->plan_id)->first();
            if ($plan) {
                $user = User::whereId($plan->user_id)->first();
                if ($user) {
                    $webhookUrl = $user->webhook_url;
                    if ($webhookUrl) {
                        $customerDataAdd['first_name']  = $request->customer_first_name;
                        $customerDataAdd['last_name']   = $request->customer_last_name;
                        $customerDataAdd['email']       = $request->customer_email;
                        $customerDataAdd['address']     = $request->customer_address;
                        $customerDataAdd['zip_code']    = $request->zipcode;
                        $customerDataAdd['phone']       = $request->customer_mobile;
                        $customerDataAdd['name']        = $request->customer_first_name . " " . $request->customer_last_name;
                        $customerDataAdd['offer_name']  = $plan->name;
                        $customerDataAdd['offer_price'] = $plan->price;

                        $this->zapCurl($webhookUrl, $customerDataAdd, "POST");
                    }
                }
            }


            return redirect($data['finmkt_redirect_url']);
        } else if (!$tier1IsToggled && $tier2IsToggled) {
            $response = $this->paymentRequestDeneift($url, $tempArr, "POST", $token);

            if (count($response)) {
                if (isset($response['code'])) {
                    if ($response['code'] == 'success') {
                        $iframeUrl          = $response['data']['payment_link'];
                        $data['iframe_url'] = $iframeUrl;

                        //add customer
                        $customer = Customer::whereEmail($request->customer_email)->first();
                        if ($customer) {
                            $customerId = $customer->id;
                            Customer::find($customer->id)->update([
                                'first_name' => $request->customer_first_name,
                                'last_name' => $request->customer_last_name,
                            ]);
                        } else {
                            $customerDataAdd['user_id']     = $request->client_id;
                            $customerDataAdd['first_name']  = $request->customer_first_name;
                            $customerDataAdd['last_name']   = $request->customer_last_name;
                            $customerDataAdd['email']       = $request->customer_email;
                            $customerDataAdd['address']     = $request->customer_address;
                            $customerDataAdd['zip_code']    = $request->zipcode;
                            $customerDataAdd['phone']       = $request->customer_mobile;
                            $customerDataAdd['status']      = 'active';
                            $customer = Customer::create($customerDataAdd);
                            $customerId = $customer->id;
                        }

                        $customerPayment = CustomerPayment::whereCustomerId($customerId)->whereStatus('pending');

                        if (!$customerPayment->exists()) {
                            CustomerPayment::create(['customer_id' => $customerId, 'plan_id' => $request->plan_id, 'status' => 'pending']);
                        }

                        $data['customer_id'] = $customer->id;
                        $data['service_amount'] = $request->service_amount;

                        //webhook
                        $plan = Plan::whereId($request->plan_id)->first();
                        if ($plan) {
                            $user = User::whereId($plan->user_id)->first();
                            if ($user) {
                                $webhookUrl = $user->webhook_url;
                                if ($webhookUrl) {
                                    $customerDataAdd['first_name']  = $request->customer_first_name;
                                    $customerDataAdd['last_name']   = $request->customer_last_name;
                                    $customerDataAdd['email']       = $request->customer_email;
                                    $customerDataAdd['address']     = $request->customer_address;
                                    $customerDataAdd['zip_code']    = $request->zipcode;
                                    $customerDataAdd['phone']       = $request->customer_mobile;
                                    $customerDataAdd['name']        = $request->customer_first_name . " " . $request->customer_last_name;
                                    $customerDataAdd['offer_name']  = $plan->name;
                                    $customerDataAdd['offer_price'] = $plan->price;

                                    $this->zapCurl($webhookUrl, $customerDataAdd, "POST");
                                }
                            }
                        }

                        return view('iframe.payiframe', ['data' => $data]);
                    }
                }
            }
        } else if ($tier1IsToggled && !$tier2IsToggled) {
            $customer = Customer::whereEmail($request->customer_email)->first();
            if ($customer) {
                $customerId = $customer->id;
                Customer::find($customer->id)->update([
                    'first_name' => $request->customer_first_name,
                    'last_name' => $request->customer_last_name,
                ]);
            } else {
                $customerDataAdd['user_id']     = $request->client_id;
                $customerDataAdd['first_name']  = $request->customer_first_name;
                $customerDataAdd['last_name']   = $request->customer_last_name;
                $customerDataAdd['email']       = $request->customer_email;
                $customerDataAdd['address']     = $request->customer_address;
                $customerDataAdd['zip_code']    = $request->zipcode;
                $customerDataAdd['phone']       = $request->customer_mobile;
                $customerDataAdd['status']      = 'active';
                $customer = Customer::create($customerDataAdd);
                $customerId = $customer->id;
            }
            $customerPayment = CustomerPayment::create(['customer_id' => $customerId, 'plan_id' => $request->plan_id, 'status' => 'pending', 'finmkt_loan_id' => '']);
            $tempArr['payment_generated_id'] = $customerPayment->id;

            $finmkt = $this->finmktPayment($tempArr, ['success' => route('plan.finmkt.success', $tempArr), 'fail' => route('plan.tire1.fail', $tempArr)]);


            CustomerPayment::where('id', $customerPayment->id)->update(['finmkt_loan_id' => $finmkt['loanId']]);

            $data['customer_id'] = $customer->id;
            $data['service_amount'] = $request->service_amount;

            $data['finmkt_redirect_url'] = $finmkt['redirectUrl'];
            $data['finmkt_loan_id'] = $finmkt['loanId'];

            //webhook
            $plan = Plan::whereId($request->plan_id)->first();
            if ($plan) {
                $user = User::whereId($plan->user_id)->first();
                if ($user) {
                    $webhookUrl = $user->webhook_url;
                    if ($webhookUrl) {
                        $customerDataAdd['first_name']  = $request->customer_first_name;
                        $customerDataAdd['last_name']   = $request->customer_last_name;
                        $customerDataAdd['email']       = $request->customer_email;
                        $customerDataAdd['address']     = $request->customer_address;
                        $customerDataAdd['zip_code']    = $request->zipcode;
                        $customerDataAdd['phone']       = $request->customer_mobile;
                        $customerDataAdd['name']        = $request->customer_first_name . " " . $request->customer_last_name;
                        $customerDataAdd['offer_name']  = $plan->name;
                        $customerDataAdd['offer_price'] = $plan->price;

                        $this->zapCurl($webhookUrl, $customerDataAdd, "POST");
                    }
                }
            }

            return redirect($data['finmkt_redirect_url']);
        } else if (!$tier1IsToggled && !$tier2IsToggled) {
            return redirect(route('plan.notire.fail'));
        }
    }

    public function plaidToken(Request $request)
    {

        $plaidURL = "https://production.plaid.com";
        $stripeURL = "https://api.stripe.com";
        $stripeAuth = "sk_test_EbY8NrsBEvbkFDUXLvoCTe9U:";
        $clientPublicPlaidKey   =  $request->plaid_public_key;
        $clientSecretPlaidKey   =  $request->plaid_secret_key;

        $plaidTokenParam = [
            'client_id'     => $clientPublicPlaidKey,
            'secret'        => $clientSecretPlaidKey,
            "language"  => "en",
            "country_codes"  => ["US"],
            "products"  => ["auth"],
            "user"  => [
                "client_user_id"    =>  "Stripe"
            ],
            "client_name"  => $request->email,
        ];

        $plaidToken = $this->CD_CurlRequest($plaidURL . "/link/token/create", json_encode($plaidTokenParam), ["Content-Type: application/json"]);
        $plaidToken = json_decode($plaidToken, true);
        //add customer
        $customer = Customer::whereEmail($request->email)->first();
        if ($customer) {
            $customerId = $customer->id;
        } else {
            $customerDataAdd['user_id']     = $request->client_id;
            $customerDataAdd['first_name']  = $request->customer_first_name;
            $customerDataAdd['last_name']   = $request->customer_last_name;
            $customerDataAdd['email']       = $request->email;
            $customerDataAdd['address']     = $request->customer_address;
            $customerDataAdd['zip_code']    = $request->zipcode;
            $customerDataAdd['phone']       = $request->customer_mobile;
            $customerDataAdd['status']      = 'active';
            $customer = Customer::create($customerDataAdd);
            $customerId = $customer->id;
        }

        $customerPayment = CustomerPayment::whereCustomerId($customerId)->whereStatus('pending');

        if (!$customerPayment->exists()) {
            CustomerPayment::create(['customer_id' => $customerId, 'plan_id' => $request->plan_id, 'status' => 'pending']);
        }

        return array('customer_id' => $customerId, 'link_token' => $plaidToken['link_token']);
    }
    public function getPayment(Request $request)
    {
        $plaidURL   = "https://sandbox.plaid.com";
        $stripeURL  = "https://api.stripe.com";
        $stripeAuth = "sk_test_EbY8NrsBEvbkFDUXLvoCTe9U:";

        $clientPublicStripeKey  =  $request->stripe_public_key;
        $clientSecretStripeKey  =  $request->stripe_secret_key;
        $clientPublicPlaidKey   =  $request->plaid_public_key;
        $clientSecretPlaidKey   =  $request->plaid_secret_key;
        $stripeAuth             = $clientSecretStripeKey;



        // $params = [
        //     'client_id' => '5ebaae5cc08af900129d2847',
        //     'secret' => 'efe21f3de29c9bac1c00780c379649',
        //     'public_token' => $request->publicToken,
        // ];

        $params = [
            'client_id'     => $clientPublicPlaidKey,
            'secret'        => $clientSecretPlaidKey,
            'public_token'  => $request->publicToken,
        ];

        $plaidToken = $this->CD_CurlRequest($plaidURL . "/item/public_token/exchange", json_encode($params), ["Content-Type: application/json"]);
        $plaidToken = json_decode($plaidToken);


        // $params = [
        //     'client_id' => '5ebaae5cc08af900129d2847',
        //     'secret' => 'efe21f3de29c9bac1c00780c379649',
        //     'access_token' => $plaidToken->access_token,
        //     'account_id' => $request->accountId
        // ];
        $params = [
            'client_id' => $clientPublicPlaidKey,
            'secret' => $clientSecretPlaidKey,
            'access_token' => $plaidToken->access_token,
            'account_id' => $request->accountId
        ];

        $bankTokenResponse = $this->CD_CurlRequest($plaidURL . "/processor/stripe/bank_account_token/create", json_encode($params), ["Content-Type: application/json"]);
        $bankTokenResponse = json_decode($bankTokenResponse);
        $stripeToken = $bankTokenResponse->stripe_bank_account_token;

        $params = [
            "email"   => $request->stripeCustomer
        ];

        $stripeCustomer = $this->CD_CurlRequest($stripeURL . "/v1/customers", http_build_query($params), ["Content-Type: application/x-www-form-urlencoded"], $stripeAuth);
        $stripeCustomer = json_decode($stripeCustomer, true);
        // print_r($stripeCustomer);die;
        $params = [
            "source"   => $stripeToken
        ];

        $sourceAttach = $this->CD_CurlRequest($stripeURL . "/v1/customers/" . $stripeCustomer['id'] . "/sources", http_build_query($params), ["Content-Type: application/x-www-form-urlencoded"], $stripeAuth);


        $stripeProduct = $request->service_amount;
        $paymentSuccessful = false;

        $params = [
            'amount'  =>   $stripeProduct * 100,
            'currency' => 'usd',
            'description'   => "Plaid Charge",
            'customer'  => $stripeCustomer['id']
        ];
        $chargeCreate = $this->CD_CurlRequest($stripeURL . "/v1/charges", http_build_query($params), ["Content-Type: application/x-www-form-urlencoded"], $stripeAuth);

        $paymentSuccessful = true;

        CustomerPayment::whereCustomerId($request->customer_id)
            ->whereStatus('pending')
            ->first()
            ->update(['amount' => $request->service_amount, 'type' => 'plaid', 'status' => 'paid']);


        if ($paymentSuccessful) {

            $headers = [
                'Authorization: Bearer 9c721fdf-6116-40b2-be1a-219b6b51e0cd',
                'Content-Type: application/x-www-form-urlencoded'
            ];

            $params = [
                'email' => $request->stripeCustomer,
                'firstName' => $request->firstName,
                'lastName' => $request->lastName
            ];

            if (!empty($request->phone)) {
                $params['phone'] = $request->phone;
            }

            // $ghl = $this->CD_CurlRequest("https://rest.gohighlevel.com/v1/contacts/", http_build_query($params), $headers);

            return 'Payment is successfully paid';
        }
        return 'error';
    }
    public function paymentRequestDeneift($url, $post = [], $postType = "", $token)
    {
        $headers = ['Authorization: Bearer ' . $token, 'Content-Type: application/json'];
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        if (!empty($post)) {
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($post));
        }

        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        if ($postType == "PUT") {
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
        }

        // execute!
        $response = curl_exec($ch);

        $response = json_decode($response, true);

        // close the connection, release resources used
        curl_close($ch);

        // do anything you want with your response
        return $response;
    }
    private function CD_CurlRequest($url, $post = [], $headers = [], $auth = "")
    {
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        if (!empty($post)) {
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
        }

        if (!empty($headers))
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        if (!empty($auth))
            curl_setopt($ch, CURLOPT_USERPWD, $auth);

        $response = curl_exec($ch);

        curl_close($ch);

        return $response;
    }
    function curlGeneral($url, $post = [], $postType = "")
    {

        $headers = [];

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        if (!empty($post)) {
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($post));
        }

        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        if ($postType == "PUT") {
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        }
        if ($postType == "PUT") {
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
        }
        if ($postType == "PATCH") {
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PATCH");
        } else {
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        }


        // execute!
        $response = curl_exec($ch);

        $response = json_decode($response, true);

        // close the connection, release resources used
        curl_close($ch);

        // do anything you want with your response
        return $response;
    }
    public function zapCurl($url, $post = [], $postType = "")
    {

        $data = http_build_query($post);

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => $data,
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/x-www-form-urlencoded'
            ),
        ));
        $response = curl_exec($curl);
        curl_close($curl);
        $response = json_decode($response, true);
        return $response;
    }

    public function updateDenefitsPayment(Request $request)
    {
        CustomerPayment::whereCustomerId($request->customer_id)
            ->whereStatus('pending')
            ->first()
            ->update(['amount' => $request->service_amount, 'type' => 'denefits', 'status' => 'paid']);

        return response()->json('success');
    }

    private function loadFinmrkt($plan_id)
    {
        $plan = Plan::find($plan_id);
        $user_data = User::find($plan->user_id);
        Http::macro('finmkt', function () use ($user_data) {
            return Http::withBasicAuth($user_data->finmkt_username, $user_data->finmkt_password)
                ->withHeaders([
                    'UserId' => $user_data->finmkt_user_id,
                    'Content-Type' => 'application/json',
                    'Type' => 'MERCHANT'
                ])
                ->baseUrl($user_data->finmkt_base_url);
        });
    }

    private function finmktPayment($data, $url)
    {
        $endpoint = '/gateway/v1/pos2/loan?offerCodeId=63242b5d3fe07d19ae1cc872';

        $body = [

            "loanType" => "POINT_OF_SALE",
            "loanCategory" => "CONSUMER_POS",
            "loanAmount" => $data['service_amount'],
            "applicant" => [
                "firstName" => $data['customer_first_name'],
                "lastName" => $data['customer_last_name'],
                "dateOfBirth" => $data['date_of_birth'],
                "identificationNumber" => "666698144",
                "address" => [
                    "zipCode" => $data['zipcode'],
                    "city" => "ATLANTaA",
                    "street1" => $data['customer_address'],
                    "street2" => "LENEXA",
                    "state" => "KS",
                    "country" => "US"
                ],
                "mobile" => $data['customer_mobile'],
                "email" =>  $data['customer_email'],
            ],
            "merchantUrls" => [
                "successUrl" => ($url['success']),
                "failureUrl" => ($url['fail']),
            ],
            "userInfo" => [
                [
                    "language" => "en",
                    "userAgent" => "Mozilla\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\/537.36 (KHTML, like Gecko) Chrome\/69.0.3497.100 Safari\/537.36",
                    "ipaddress" => "202.53.68.18",
                    "screenResolution" => "1920 x 1080"
                ]
            ],

        ];
        $this->loadFinmrkt($data['plan_id']);

        $response = HTTP::finmkt()->post($endpoint, $body);


        return $response;
    }

    public function checkFinmktLoanStatus($email)
    {
        $response = null;
        $customer = Customer::whereEmail($email)->first();
        $payment = CustomerPayment::whereCustomerId($customer->id ?? 0)->whereStatus('pending');

        $loanId = $payment->exists() ? $payment->get()->last()->finmkt_loan_id : null;

        $endpoint = "/gateway/v1/pos2/applications/status/" . $loanId;

        if ($loanId) {
            $response = HTTP::finmkt()->post($endpoint);

            $data = json_decode($response->body(), true);

            if ($data['event'] !== 'Approved') {
                $payment->update(['type' => 'finmkt', 'status' => 'paid', 'amount' => session()->pull('service_amount')]);
                return response()->json(['success' => true, 'finmkt' => true]);
            }

            return response()->json(['success' => false, 'finmkt' => true]);
        }

        return response()->json(['success' => false, 'finmkt' => false]);
    }

    public function finmktPaymentStatus($status, $message)
    {
        return redirect(route('client.customersPaments'))->with($status, $message);
    }
}
