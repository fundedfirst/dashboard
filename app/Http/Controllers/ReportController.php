<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use App\Models\Customer;
use App\Models\CustomerPayment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
class ReportController extends Controller

{
    public function customers(Request $request){
        $authUserId     = auth()->user()->id;
        if(request('from_date') && request('to_date')){
            $fromDate   = request('from_date');
            $toDate     = request('to_date'); 
            $data['customers'] = Customer::whereUserId($authUserId)->where(function($query) use($fromDate, $toDate){
                $query->whereBetween('created_at', [$fromDate, $toDate]);
            })->orderBy('id', 'DESC')->simplePaginate(10);
            return view('client.report.customers', ['data' => $data]);
           
        }
        $data['customers'] = Customer::whereUserId($authUserId)->orderBy('id', 'DESC')->simplePaginate(10);
        return view('client.report.customers', ['data' => $data]);
    }
    public function customersPaments(Request $request){
        $authUserId     = auth()->user()->id;
        $customerIds    = Customer::whereUserId($authUserId)->pluck('id');
        if(request('from_date') && request('to_date')){
            $fromDate   = request('from_date');
            $toDate     = request('to_date'); 
            $data['customerPayments'] = CustomerPayment::whereIn('customer_id', $customerIds)->where(function($query) use($fromDate, $toDate){
                $query->whereBetween('created_at', [$fromDate, $toDate]);
            })->orderBy('id', 'DESC')->simplePaginate(10);
            return view('client.report.customer-payments', ['data' => $data]);
           
        }
        $data['customerPayments'] = CustomerPayment::whereIn('customer_id', $customerIds)->orderBy('id', 'DESC')->simplePaginate(10);
        return view('client.report.customer-payments', ['data' => $data]);

    }
}
