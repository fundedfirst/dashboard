<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\LoginRequest;
use App\Models\Role;
use App\Providers\RouteServiceProvider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\User;

class AuthenticatedSessionController extends Controller
{
    /**
     * Display the login view.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        if(Auth::guard('user')->user()){
            return redirect()->route('client.dashboard');
        }
        return view('client.login');
    }

    /**
     * Handle an incoming authentication request.
     *
     * @param  \App\Http\Requests\Auth\LoginRequest  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    { 
        $roles  = Role::whereName('client')->first();
        if(!$roles){
            return redirect()->back()->withErrors('This role does not exist.')->withInput();
        }
        $roleId = $roles->id;
        
        $checkUser = User::whereEmail($request->email)->whereRoleId($roleId)->first();
        if(!$checkUser){
            return redirect()->back()->withErrors('Email Address does not exist.')->withInput();
        }
        if($checkUser->status == 'banned'){
            return redirect()->back()->withErrors('You have been blocked by Admin, Please contact Admin.')->withInput();
        }
        if($checkUser->status == 'inactive'){
            return redirect()->back()->withErrors('Your accout is inactive, Please contact Admin.')->withInput();
        }
        if($checkUser->status == 'deleted'){
            return redirect()->back()->withErrors('This account deleted earlier.')->withInput();
        }
        if (Auth::guard('user')->attempt(['email' => $request->email, 'password' => $request->password], $request->remember_me)) {
            return redirect(url('client/dashboard'));
        }

        return redirect()->back()->withErrors('These credentials do not match our records.')->withInput();
    }

    /**
     * Destroy an authenticated session.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Request $request)
    {
        Auth::guard('user')->logout();

        $request->session()->invalidate();

        $request->session()->regenerateToken();

        return redirect('/login');
    }
}
