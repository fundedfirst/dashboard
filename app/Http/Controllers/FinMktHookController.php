<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Response;

class FinMktHookController extends Controller
{
    public function finMktHook(Request $request){
        $header = $request->header('Authorization');
        if($header == '298ce0d4-5a75-48ee-8236-2ed09a092b07'){
            return Response::json([
                'message' => 'Valid key' 
            ], 201);
        }else{
            return Response::json([
                'message' => 'Unauthorized' 
            ], 401);
        }
   }
}
