<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Plan;
use Illuminate\Http\Request;
use Validator;

class PlanController extends Controller
{
    public function index()
    {
        $data['plans'] = Plan::orderBy('id', 'DESC')->get();
        return view('admin.plan.index', ['data' => $data]);
    }
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name'   => 'required|string|max:255',
            'price'    => 'required|integer',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        $dataToAdd['name'] = $request->name;
        $dataToAdd['price'] = $request->price;
        $dataToAdd['description'] = $request->description;
        $dataToAdd['down_payment'] = $request->down_payment;
        $dataToAdd['number_of_payment'] = $request->number_of_payment;
        $dataToAdd['youtube_video_embed_code'] = $request->youtube_video_embed_code;
        $dataToAdd['user_id'] = auth()->user()->id;
        $dataToAdd['created_at'] = date('Y-m-d H:i:s');
        $dataToAdd['updated_at'] = date('Y-m-d H:i:s');
        $add = Plan::create($dataToAdd);
        return redirect()->route('plans.index')->with('success', 'Plan added successfully.');
    }
    public function create()
    {
        return view('admin.plan.create');
    }
    public function update(Request $request, $id)
    {
        $plan               = Plan::findOrFail($id);
        $plan->name         = $request->name;
        $plan->price        = $request->price;
        $plan->description  = $request->description;
        $plan->down_payment = $request->down_payment;
        $plan->number_of_payment         = $request->number_of_payment;
        $plan->youtube_video_embed_code  = $request->youtube_video_embed_code;
        $plan->status       = $request->status;
        $plan->save();
        if ($plan) {
            return redirect()->back()->with('success', 'Plan updated successfully.');
        }
        return redirect()->back()->with('error', 'Error updating Plan.');
    }
    public function edit($id)
    {
        $data['plan'] = Plan::findOrFail($id);
        return view('admin.plan.edit', ['data' => $data]);
    }
    public function show($id)
    {
        $data['plan'] = Plan::findOrFail($id);
        return view('admin.plan.show', ['data' => $data]);
    }
    public function destroy($id)
    {
        $plan = Plan::findOrFail($id);
        $plan->status = 'deleted';
        $plan->save();
        if ($plan) {
            return redirect()->route('plans.index')->with('success', "Plan - $plan->name deleted successfully.");
        }
        return redirect()->back()->with('error', 'Error deleting Plan.');
    }
}
