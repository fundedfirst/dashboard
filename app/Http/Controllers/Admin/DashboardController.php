<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use App\Models\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use DB;
class DashboardController extends Controller
{
    public function index(){
        $authUserId         = auth()->user()->id;
        $roles              = Role::whereName('client')->first();
        $data['clients']    = User::whereRoleId($roles->id)->count();
        return view('admin.dashboard', ['data'=> $data]);
    }
    public function home(){
        $authUser =Auth()->user();
        return view('admin.index');
    }
}
