<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use App\Models\Role;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Models\User;
use Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;


class AdminController extends Controller
{
   public function myProfile(){
      return view('admin.profile.index');
   }
   public function editMyProfile(){
      return view("admin.profile.edit");
   }
   public function updateMyProfile(Request $request){
      $validator = Validator::make($request->all(), [   
         'first_name'   => 'required|string|max:255',
         'last_name'    => 'required|string|max:255',
         'profile_image'=> 'mimes:jpeg,jpg,png,gif'
      ]);

     if ($validator->fails()) {
         return redirect()->back()->withErrors($validator)->withInput();
     }
     if ($request->hasFile('profile_image')) {

         // $image            = $request->file('profile_image');
         // $extension        = $image->getClientOriginalExtension();
         // $ImgName          = $image->getClientOriginalName();
         // $fileNameWithoutEx= pathinfo($ImgName, PATHINFO_FILENAME);
         // $destinationPath  = public_path("/images/admin");

         // if (!file_exists($destinationPath)) {
         //    //create folder
         //    mkdir($destinationPath, 0755, true);
         // }

         // $time = time();
         // $image->move($destinationPath, $fileNameWithoutEx . "_" . $time . "." . $extension);
         // $fullImagePath = $fileNameWithoutEx . "_" . $time . "." . $extension;
         // $dataToUpdate['profile_image'] = $fullImagePath;
         $dataToUpdate['profile_image'] = Storage::disk('s3')->putFile('client', $request->file('profile_image'));
      }
      $dataToUpdate['first_name'] = $request->first_name;
      $dataToUpdatep['last_name'] = $request->last_name;
      User::whereId(auth()->user()->id)->update($dataToUpdate);

     return redirect()->back()->with('success', 'Profile updated successfully.');  

   }
   public function subAdmins(){
      $role = Role::whereName('admin')->first();

      $data['subAdmins'] = User::whereRoleId($role->id)->orderBy('id', 'DESC')->simplePaginate(10);
      return view('admin.sub-admins.index', ['data'=> $data]);
   }
   public function createAdmin(Request $request){
      return view('admin.sub-admins.create');
   } 
   public function storeAdmin(Request $request){
      $validator = Validator::make($request->all(), [   
         'first_name'   => 'required|string|max:255',
         'last_name'    => 'required|string|max:255',
         'email'        => 'required|email|unique:users',
         'password'     => 'required|string|max:255'
      ]);
      if ($validator->fails()) {
         return redirect()->back()->withErrors($validator)->withInput();
      }
      $role = Role::whereName('admin')->first();
      $password   = Hash::make($request->password);
      $add = User::create(['password'=> $password, 'role_id'=> $role->id, 'first_name' => $request->first_name, 'last_name' => $request->last_name, 'email'=> $request->email, 'status'=> 'active']);
      if($add){
         return redirect()->back()->with('success', 'Sub Admin Added Successfully.');  
      }
      return redirect()->back()->with('error', 'Error creating Sub Admin!');  

   }
   public function editAdmin($id){
      $user = User::whereId($id)->first();
      
      if($user->role == 'super_admin' || $user->role == 'client'){
         return redirect()->back()->with('error', 'Invalid Id given!');  
      }
      return view('admin.sub-admins.edit', ['data'=> $user]);

   }
   public function updateAdmin(Request $request){
      $validator = Validator::make($request->all(), [   
         'first_name'   => 'required|string|max:255',
         'last_name'    => 'required|string|max:255',
         'status'       => 'required|string|max:255'
      ]);
      if ($validator->fails()) {
         return redirect()->back()->withErrors($validator)->withInput();
      }
      
      $update = User::whereId($request->id)->update(['first_name' => $request->first_name, 'last_name' => $request->last_name, 'status'=> $request->status]);
      if($update){
         return redirect()->back()->with('success', 'Sub Admin updated Successfully.');  
      }
      return redirect()->back()->with('error', 'Error updating Sub Admin!');  

   }
   public function logout(){
        Auth::guard('admin')->logout();
        return redirect()->route('home');
   }
}
