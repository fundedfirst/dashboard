<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Models\User;
use App\Models\CompanyDetails;
use DB;
use Validator;

class ClientController extends Controller
{
    public function index()
    {
        $role            = Role::whereName('client')->first();
        $data['clients'] = User::whereRoleId($role->id)->orderBy('id', 'DESC')->get()->toArray();
        return view('admin.client.index', ['data' => $data]);
    }
    public function create()
    {
        return view('admin.client.create');
    }
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'email'     => 'required|email|max:255|unique:users',
            'password'  => 'required|string|max:255',
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        $role       = Role::whereName('client')->first();
        $password   = Hash::make($request->password);
        $client     = new User;

        $client->role_id = $role->id;
        $client->first_name = $request->first_name;
        $client->last_name = $request->last_name;
        $client->email = $request->email;
        $client->password = $password;
        $client->stripe_public_key  = $request->stripe_public_key;
        $client->stripe_secret_key  = $request->stripe_secret_key;
        $client->plaid_public_key   = $request->plaid_public_key;
        $client->plaid_secret_key   = $request->plaid_secret_key;
        $client->webhook_url        = $request->webhook_url;
        $client->denefit_token      = $request->denefit_token;
        $client->finmkt_user_id      = $request->finmkt_user_id;
        $client->finmkt_base_url      = $request->finmkt_base_url;
        $client->finmkt_username      = $request->finmkt_username;
        $client->finmkt_password      = $request->finmkt_password;
        $client->is_dynamic_price_enabled     = $request->dynamic_pricing;
        $client->dynamic_price_key    = $request->dynamic_price_key;
        $client->save();

        if ($client) {
            return redirect()->route('clients.index')->with('success', 'New Client added successfully.');
        }
        return redirect()->back()->with('error', 'Error adding Client.');
    }
    public function show($id)
    {
        $data['client'] = User::findOrFail($id);
        $data['company'] = CompanyDetails::where('user_id',$id)->first();
        return view('admin.client.show', ['data' => $data]);
    }
    public function edit($id)
    {
        $data['client'] = User::findOrFail($id);
        $data['company'] = CompanyDetails::where('user_id',$id)->first();
        return view('admin.client.edit', ['data' => $data]);
    }
    public function update(Request $request, $id)
    {
        echo "<pre>";
        print_r($request->all());
        $client = User::findOrFail($id);
        $client->first_name = $request->first_name;
        $client->last_name  = $request->last_name;
        $client->email      = $request->email;
        $client->stripe_public_key  = $request->stripe_public_key;
        $client->stripe_secret_key  = $request->stripe_secret_key;
        $client->plaid_public_key   = $request->plaid_public_key;
        $client->plaid_secret_key   = $request->plaid_secret_key;
        $client->webhook_url        = $request->webhook_url;
        $client->denefit_token      = $request->denefit_token;
        $client->finmkt_user_id      = $request->finmkt_user_id;
        $client->finmkt_base_url      = $request->finmkt_base_url;
        $client->finmkt_username      = $request->finmkt_username;
        $client->finmkt_password      = $request->finmkt_password;
        $client->tier1_toggled      = $request->tier1_toggle;
        $client->tier2_toggled     = $request->tier2_toggle;
        $client->is_dynamic_price_enabled     = $request->dynamic_pricing;
        $client->dynamic_price_key    = $request->dynamic_price_key;
        $client->save();

        $company = CompanyDetails::updateOrCreate(['user_id'=>$id],[
            'user_id'=>$id,
            'company_name'=> $request->company_name,
            'ein_number'=> $request->company_address,
            'company_address'=> $request->ein_number,
            'city'=> $request->city,
            'state_of_incorporation'=> $request->soi,
            'zip_code'=> $request->zip_code
        ]);
        if ($client) {
            return redirect()->back()->with('success', 'Client updated successfully.');
        }
        return redirect()->back()->with('error', 'Error updating Client.');
    }
    public function destroy($id)
    {
        $client = User::findOrFail($id);
        $time = time();
        $client->status = 'deleted';
        $client->email = $client->email . "_deleted_on_" . $time;
        $client->save();
        if ($client) {
            return redirect()->route('clients.index')->with('success', "Client - $client->full_name deleted successfully.");
        }
        return redirect()->back()->with('error', 'Error deleting Client.');
    }
}
