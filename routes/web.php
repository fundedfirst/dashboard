<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\GeneralController;
use App\Http\Controllers\Auth\AuthenticatedSessionController;
use App\Http\Controllers\Admin\Auth\AuthenticatedSessionController as AdminAuthenticatedSessionController;
use App\Http\Controllers\Auth\RegisteredUserController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\ClientController;
use App\Http\Controllers\Admin\AdminController;
use App\Http\Controllers\Admin\DashboardController as AdminDashboardController;
use App\Http\Controllers\Admin\ClientController as AdminClientController;

use App\Http\Controllers\Admin\PlanController as AdminPlanController;
use App\Http\Controllers\PlanController;
use App\Http\Controllers\StripController;
use App\Models\CustomerPayment;
use App\Http\Controllers\Admin\ErrorController;
use App\Http\Controllers\Admin\NotfoundController;
use App\Http\Controllers\PaymentController;
use App\Http\Controllers\ReportController;
use App\Http\Controllers\FinMktHookController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/',  [GeneralController::class, 'chooseLoign'])->name('home');
Route::get('denefit-webhook', [PaymentController::class, 'addWebhookPayment'])->name('addWebhookPayment');
Route::get('update-denefit-payment', [PlanController::class, 'updateDenefitsPayment'])->name('updateDenefitsPayment');
//webhook
Route::get('stripe/event', [StripController::class, 'event'])->name('stripEvent');
Route::get('stripe/event/customer', [StripController::class, 'eventCustomer'])->name('stripEventCustomer');


Route::get('/success', [GeneralController::class, 'planFinmktSuccess'])->name('plan.finmkt.success');
Route::get('/fail', [GeneralController::class, 'planFinmktFail'])->name('plan.finmkt.fail');
Route::get('/tire1Fail', [GeneralController::class, 'tire1PlanFinmktFail'])->name('plan.tire1.fail');
Route::get('/notirefail', [GeneralController::class, 'noTirePlanFail'])->name('plan.notire.fail');

//plan iframe
Route::get('/plan/{id}', [PlanController::class, 'planIframe'])->name('planiframe');
// Route::post('/plan/iframe', [PlanController::class, 'planIframeAdd'])->name('planiframe.add');
Route::post('/plan/iframe', [PlanController::class, 'planIframeAddNew'])->name('planiframe.add');
Route::post('/plan/plaidtoken', [PlanController::class, 'plaidToken'])->name('generatePlaidToken');
Route::post('/plan/getPayment', [PlanController::class, 'getPayment'])->name('getPayment');
Route::get('/email-validation/{email}', [GeneralController::class, 'validateEmail'])->name('validateEmail');
Route::post('/add-customer-payment', [CustomerPayment::class, 'customerPay'])->name('customerPay');

//dynamic pricing
Route::get('/dynamic-pricing/iframe', [GeneralController::class, 'storePlan'])->name('iframe.store');

//admin
Route::get('/admin/login', [AdminAuthenticatedSessionController::class, 'create'])->name('admin.login');
Route::post('/admin/login', [AdminAuthenticatedSessionController::class, 'store'])->name('admin.login');
Route::prefix('admin')->group(function () {
    Route::group(['middleware' => 'auth:admin'], function () {
        Route::get('/', [AdminDashboardController::class, 'home'])->name('admin.home');
        Route::get('dashboard', [AdminDashboardController::class, 'index'])->name('admin.dashboard');
        Route::get('error', [ErrorController::class, 'error'])->name('admin.error');
        Route::get('notfound', [NotfoundController::class, 'notfound'])->name('admin.notfound');
        Route::get('my-profile', [AdminController::class, 'myProfile'])->name('admin.myProfile');
        Route::get('edit-my-profile', [AdminController::class, 'editMyProfile'])->name('admin.editMyProfile');
        Route::put('update-my-profile', [AdminController::class, 'updateMyProfile'])->name('admin.updateMyProfile');
        Route::resource('clients', AdminClientController::class);
        Route::resource('plans', AdminPlanController::class);
        Route::get('sub-admins', [AdminController::class, 'subAdmins'])->name('admin.subAdmins');
        Route::get('create-admin', [AdminController::class, 'createAdmin'])->name('admin.createAdmin');
        Route::post('store-admin', [AdminController::class, 'storeAdmin'])->name('admin.storeAdmin');
        Route::get('edit-admin/{id}', [AdminController::class, 'editAdmin'])->name('admin.editAdmin');
        Route::put('update-admin', [AdminController::class, 'updateAdmin'])->name('admin.updateAdmin');
        Route::get('/logout', [AdminController::class, 'logout'])->name('admin.logout');
    });
});

//client
Route::get('/client/login', [AuthenticatedSessionController::class, 'create'])->name('client.login');
Route::post('/client/login', [AuthenticatedSessionController::class, 'store'])->name('client.login');
Route::prefix('client')->group(function () {
    Route::group(['middleware' => 'auth:user'], function () {
        Route::get('/', [DashboardController::class, 'home'])->name('client.home');
        Route::get('/store', [ClientController::class, 'store'])->name('client.store');
        Route::get('/dashboard', [DashboardController::class, 'index'])->name('client.dashboard');
        Route::get('my-profile', [ClientController::class, 'myProfile'])->name('client.myProfile');
        Route::get('edit-my-profile', [ClientController::class, 'editMyProfile'])->name('client.editMyProfile');
        Route::put('update-my-profile', [ClientController::class, 'updateMyProfile'])->name('client.updateMyProfile');
        Route::resource('clientplans', PlanController::class);
        Route::get('customers', [ReportController::class, 'customers'])->name('client.customers');
        Route::get('customer-payments', [ReportController::class, 'customersPaments'])->name('client.customersPaments');
        Route::get('finmkt-loan-status/{email}', [PlanController::class, 'checkFinmktLoanStatus'])->name('checkFinmktLoanStatus');
        Route::get('finmkt-payment-status/{status}/{message}', [PlanController::class, 'finmktPaymentStatus'])->name('finmktPaymentStatus');
        Route::get('/logout', [ClientController::class, 'logout'])->name('client.logout');
    });
});
Route::post('/finmkt/hook', [FinMktHookController::class, 'finMktHook'])->name('finmkt.hook');
require __DIR__ . '/auth.php';
