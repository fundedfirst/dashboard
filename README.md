	## Table of contents
* [General info](#general-info)
* [Technologies](#technologies-&-tools)
* [Launch](#launch)

## General info
OCC Dashboard where Super Admin, Admin, Clients can add plans, manage thier customers and create Iframe for each plan.

All the iframe plan options are within client profile settings

Tools:
Iframe options:-
https://business.denefits.com/login  (get finance option )
https://plaid.com/ (pay in full option)

truemail.io for validating emails while adding info at the time of iframe
usetiful.com (for custom tool tips for giving info to users)



API's used:

For Denefit:
All api's are used in file PlanController.php (app/Http/Controllers/PlanController.php) and plan.blade.php file (resources/views/iframe/plan.blade.php)

"https://endpoint.denefits.com/v1/payment_plan/receiveDetails?token=token here" (For payment request)

For Plaid:
https://plaid.com/docs/api/
https://stripe.com/docs/api

"https://sandbox.plaid.com/link/token/create";
"https://sandbox.plaid.com/item/public_token/exchange";
"https://sandbox.plaid.com/processor/stripe/bank_account_token/create";

Denefit login:
https://business.denefits.com/app/apiDocs?type=2
https://business.denefits.com/login

https://truemail.io/
https://developers.truemail.io/single-verification



## Technologies & Tools
* Denefit Dashboard is built with Laravel 8.71.0 & php 7
* Blade template
* truemail.io
* usetiful.com

## Launch
* clone project
* rename .env.example to .env & setup database credentials
* run command to install newly cloned project: composer install
* php artisan migrate
* php artisan db:seed
* php artisan serve
* Finally hit http://127.0.0.1:8000/ in your browser, thats it. (locally)