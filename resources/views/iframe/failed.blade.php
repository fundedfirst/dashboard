<x-iframe-header />
<style>
    .development-container {
        background-color: #efefef;
        padding: 30px;
        margin: 40px 0;
        border-radius: 6px;
        border: 2px solid #337ab7;
    }

    .message-container {
        display: flex;
        justify-content: center;
    }
</style>

<body>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="entry-content si-entry">
                    <div class="si-container">
                        <div class="development-container">
                            <div class="message-container">
                                <h1>Request for Plan Failed</h1>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
</body>

</html>
