<x-iframe-header />
<!-- Google Tag Manager -->
<!-- <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-NPFFMLH');</script> -->
<!-- End Google Tag Manager -->
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5D6B3RX');</script>
<!-- End Google Tag Manager -->
<style>
    .development-container {
        background-color: #efefef;
        padding: 30px;
        margin: 40px 0;
        border-radius: 6px;
        border: 2px solid #337ab7;
    }

    input.btn.btn-dark.loader-btn.sub-btn {
        outline: none !important;
    }

    input.btn.btn-dark.loader-btn.sub-btn:hover {
        color: #fff !important;
    }

    .the-datepicker__body.the-datepicker__body--swipeable {
        overflow-y: scroll;
        height: 200px;
    }

    .submit-btn-holder i {
        position: absolute;
        top: 39px;
        left: 91px;
        color: #fff;
    }

    .fa-spin {
        display: none;
    }

    /* #spiner{
        display:none;
      } */
    .spiner
    {
    position: relative;
    left: 550px;
    top: 300px;
    z-index: 9999;
    border: 12px solid #f3f3f3;
    border-radius: 50%;
    border-top: 12px solid #3498db;
    width: 70px;
    height: 70px;
    -webkit-animation: spin 2s linear infinite; /* Safari */
    animation: spin 2s linear infinite;
    }
    @-webkit-keyframes spin {
    0% { -webkit-transform: rotate(0deg); }
    100% { -webkit-transform: rotate(360deg); }
    }

    @keyframes spin {
    0% { transform: rotate(0deg); }
    100% { transform: rotate(360deg); }
    }
    .form-fields.submit-btn-holder.mb-0 {
        display: inline-block;
    }

    .relative.flex.items-top.justify-center.min-h-screen.bg-gray-100.dark {
        background-color: #fff;
    }

    .max-w-xl.mx-auto.sm {
        background-color: #fff;
    }

    .relative.flex.items-top.justify-center.min-h-screen.bg-gray-100.dark\:bg-gray-900.sm\:items-center.sm\:pt-0 {
        background-color: #fff !important;
    }

    .development-container .form-fields {
        margin-bottom: 15px;
    }

    .form-control {
        height: 44px;
        border-radius: 30px;
        border: 1px solid #337ab7;
    }

    .mb-0 {
        margin-bottom: 0 !important;
    }

    .sub-btn {
        background-color: #337ab7;
        color: #fff !important;
        border-color: #337ab7;
        margin-top: 6px;
        height: 42px;
        border-radius: 30px;
        width: 100px;
    }

    input[type="date"] {
        padding-right: 0;
    }

    input::-webkit-calendar-picker-indicator {
        color: #fff;
        opacity: 1;
        position: relative;
        background-color: #efefef;
        padding: 10px;
        color: red;
        border-radius: 30px;
        border-top-left-radius: 0;
        border-bottom-left-radius: 0;
        border-left: 1px solid #337ab7;
        padding: 12.5px 10px;
        cursor: pointer;
    }

    input::-webkit-calendar-picker-indicator:after {
        background-color: red;
        color: #fff;
        content: '';
        top: 0;
        left: 0;
        height: 100%;
        width: 40px;
    }

    span.the-datepicker__deselect {
        display: none;
    }

    #loading {
        position: fixed;
        /* Sit on top of the page content */
        display: none;
        /* Hidden by default */
        width: 100%;
        /* Full width (cover the whole page) */
        height: 100%;
        /* Full height (cover the whole page) */
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        background-color: rgba(0, 0, 0, 0.5);
        /* Black background with opacity */
        z-index: 2;
        /* Specify a stack order in case you're using a different order for other elements */
        cursor: pointer;
        /* Add a pointer on hover */
        align-items: center;
        justify-content: center;
    }

    /* .loader {
        border: 3px solid #f3f3f3;
        border-top: 3px solid #3498db;
        border-radius: 50%;
        width: 50px;
        height: 50px;
        animation: spin 2s linear infinite;
        margin-bottom: 50px;
    }*/

    @keyframes spin {
        0% {
            transform: rotate(0deg);
        }

        100% {
            transform: rotate(360deg);
        }
    }

    /* Flexible iFrame */

    .flexible-container {
        position: relative;
        padding-bottom: 56.25%;
        padding-top: 30px;
        height: 0;
        overflow: hidden;
    }

    .flexible-container iframe,
    .flexible-container object,
    .flexible-container embed {
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
    }
</style>

<body>
    <div class="container">
        <div class="row">
            @if (\Session::has('message'))
                <div class="alert alert-danger">
                    <ul>
                        <li>{!! \Session::get('message') !!}</li>
                    </ul>
                </div>
            @endif
            <div class="col-md-12">
                <div class="entry-content si-entry">
                    <div class="si-container">
                        <div id="loading"></div>
                        <span id="spiner" class="spiner"></span>
                        <div class="development-container">
                            @if ($data['plan']->status == 'Active')
                                <h2 class="payment-status" style="display: none;">Payment is successfully paid</h2>
                                <div class="development-inner">
                                    <div class="widget-heading">
                                        <h2 class="heading-title">{{ $data['plan']->name }}</h2>
                                        <h2>${{ $data['plan']->price }}</h2>
                                    </div>
                                    @if ($data['plan']->youtube_video_embed_code)
                                        {{-- <div class="video-inner">
                                            <div class="child">
                                                <iframe width="560" height="315"
                                                    src="{{ $data['plan']->youtube_video_embed_code }}"
                                                    title="YouTube video player" frameborder="0"
                                                    allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                                                    allowfullscreen>
                                                </iframe>
                                            </div>
                                        </div> --}}
                                        <div class="flexible-container">
                                            <iframe width="560" height="315"
                                                src="{{ $data['plan']->youtube_video_embed_code }}" frameborder="0"
                                                allowfullscreen>
                                            </iframe>
                                        </div>
                                    @endif()

                                    <a href="https://fundedfirst.com/terms-service/" target="_blank">Read Terms &
                                        Conditions</a>

                                    @if ($data['plan']->description)
                                        <h4 class="heading-title">{{ $data['plan']->description }}</h4>
                                    @endif()
                                    <div class="check-inner">
                                        <label for="terms">
                                            <input type="checkbox" id="terms"> I Accept the Terms and Condition<br>
                                        </label>
                                    </div>
                                    <div id="showButtons" style="display:none;">
                                        <button class=" btn btn-primary button-ds payinfull" id="payinfull_idd">Pay in full</button>
                                        @if ($data['plan']->user->tier1_toggled AND $data['plan']->user->tier2_toggled)
                                            <button class="btn btn-primary button-ds clickbutton" id="clickbutton_id_1">Get Financed</button><br>
                                        @elseif ($data['plan']->user->tier1_toggled)
                                            <button class="btn btn-primary button-ds clickbutton" id="clickbutton_id_1">Get Financed</button><br>
                                        @elseif ($data['plan']->user->tier2_toggled)
                                            <button class="btn btn-primary button-ds clickbutton" id="clickbutton_id_2">Get Financed</button><br>
                                        @else
                                            <button class="btn btn-primary button-ds clickbutton" id="payinfull_idd">Get Financed</button><br>
                                        @endif()                                    
                                    </div>
                                </div>

                                <form method="post" action="{{ route('planiframe.add') }}" id="formfinanced"
                                    onsubmit="return validate()" style="display:none;">
                                    {{-- @csrf() --}}
                                    <div class="getfiform">
                                        <input type="hidden" name="plan_id" id="plan_id"
                                            value="{{ $data['plan']->id }}">

                                        <input type="hidden" name="stripe_public_key" id="stripe_public_key"
                                            value="{{ $data['plan']->user->stripe_public_key }}">
                                        <input type="hidden" name="stripe_secret_key" id="stripe_secret_key"
                                            value="{{ $data['plan']->user->stripe_secret_key }}">
                                        <input type="hidden" name="plaid_public_key" id="plaid_public_key"
                                            value="{{ $data['plan']->user->plaid_public_key }}">
                                        <input type="hidden" name="plaid_secret_key" id="plaid_secret_key"
                                            value="{{ $data['plan']->user->plaid_secret_key }}">
                                        <input type="hidden" name="denefit_token" id="denefit_token"
                                            value="{{ $data['plan']->user->denefit_token }}">
                                        <input type="hidden" name="finmkt_key" id="finmkt_key"
                                            value="{{ $data['plan']->user->finmkt_key }}">
                                        <input type="hidden" name="client_id" id="client_id"
                                            value="{{ $data['plan']->user->id }}">
                                        <input type="hidden" name="tier1_toggle" id="tier1_toggle"
                                            value="{{ $data['plan']->user['tier1_toggled'] }}">
                                        <input type="hidden" name="tier2_toggle" id="tier2_toggle"
                                            value="{{ $data['plan']->user['tier2_toggled'] }}">

                                        <input type="hidden" name="down_payment" id="down_payment"
                                            value="{{ $data['plan']->down_payment }}">
                                        <input type="hidden" name="number_of_payment" id="number_of_payment"
                                            value="{{ $data['plan']->number_of_payment }}">

                                        <input type="hidden" name="paytype" id="paytype" value="">
                                        <input type="hidden" name="service_amount" id="service_amount"
                                            value="{{ $data['plan']->price }}">
                                        <input type="hidden" name="service_id" id="service_id"
                                            value="{{ $data['plan']->id }}">
                                        <div class="payment-form">
                                            <div class="form-group">

                                                <div class="row">                                                
                                                    <div class="col-md-6">
                                                        <div class="form-fields">
                                                            <label for="name">First Name:</label>
                                                            <input class="form-control" type="text"
                                                                id="customer_first_name" placeholder="First Name"
                                                                name="customer_first_name" value="">
                                                            <span class="fnameerror error"
                                                                style="font-size:14px; color: #ff0000;display:none;width:100%">First
                                                                name field is required.</span>
                                                            <span class="fnameerror-invalid error"
                                                                style="font-size:14px; color: #ff0000;display:none;width:100%">Invalid
                                                                first name.</span>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-fields">
                                                            <label for="name">Last Name:</label>
                                                            <input class="form-control" type="text"
                                                                id="customer_last_name" placeholder="Last Name"
                                                                name="customer_last_name" value="">
                                                            <span class="lnameerror error"
                                                                style="font-size:14px; color: #ff0000;display:none;">Last
                                                                name field is required.</span>
                                                            <span class="lnameerror-invalid error"
                                                                style="font-size:14px; color: #ff0000;display:none;">Invalid
                                                                last name.</span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-fields">
                                                            <label for="email">Email:</label>
                                                            <input class="form-control" type="text"
                                                                id="customer_email" placeholder="Email"
                                                                name="customer_email" value="">
                                                            <span class="emailerror error"
                                                                style="font-size:14px; color: #ff0000;display:none;">Email
                                                                Address is required.</span>
                                                            <span class="validemailerror error"
                                                                style="font-size:14px; color: #ff0000;display:none;">Please
                                                                add a valid email.</span>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-fields">
                                                            <label for="address">Address:</label>
                                                            <input class="form-control" type="text"
                                                                id="customer_address" placeholder="Address"
                                                                name="customer_address" value="">
                                                            <span class="addresserror error"
                                                                style="font-size:14px; color: #ff0000;display:none;">Address
                                                                field required.</span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-fields">
                                                            <label for="zipcode">Zipcode:</label>
                                                            <input class="form-control" type="text" id="zipcode"
                                                                placeholder="0000" name="zipcode" value="">
                                                            <span class="ziperror error"
                                                                style="font-size:14px; color: #ff0000;display:none;">Zipcode
                                                                field is required.</span>
                                                            <span class="ziperror-invalid error"
                                                                style="font-size:14px; color: #ff0000;display:none;">Please
                                                                enter a valid USA zipcode.</span>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-fields">
                                                            <label for="date_of_birth">Date of birth:</label>
                                                            <div id="datetimepicker1" class="datetimepicker1">
                                                                <input type="text" id="date_of_birth"
                                                                    name="date_of_birth" placeholder="11/12/2020"
                                                                    class="form-control">
                                                            </div>
                                                            <!-- <input class="form-control" type="date" value="1990-01-22" min="1970-01-03" max="2013-01-03" id="date_of_birth" name="date_of_birth" value="" > -->
                                                            <span class="birtherror error"
                                                                style="font-size:14px; color: #ff0000;display:none;">Birthdate
                                                                is required.</span>
                                                            <span class="birtherror-age error"
                                                                style="font-size:14px; color: #ff0000;display:none;">Minimum
                                                                age of 18 years is required.</span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-fields mb-0">
                                                            <label for="cellphone">Cell Phone:</label>
                                                            <input class="form-control" placeholder="111-111-1111"
                                                                type="text" id="customer_mobile"
                                                                name="customer_mobile" value="">
                                                            <span class="phoneerror error"
                                                                style="font-size:14px; color: #ff0000;display:none;">This
                                                                phone number field is required.</span>
                                                            <span class="phonelentgherror error"
                                                                style="font-size:14px; color: #ff0000;display:none;">Mobile
                                                                should be 10 character long.</span>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-fields submit-btn-holder mb-0">
                                                            <br>
                                                            <input id="submit-btn"
                                                                class="btn btn-dark loader-btn sub-btn" type="submit"
                                                                name="formsubmit" value="Submit" formtarget="_blank"
                                                                onclick="submitForm()">
                                                            {{-- <input id="submit-btn" class="btn btn-dark loader-btn sub-btn" type="submit" name="formsubmit" value="Submit"> --}}

                                                            <!-- <i class="fa fa-spinner fa-spin"></i> -->
                                                            <!--button class="button-ds goback" style="margin-left:5px;">Go Back</button-->
                                                        </div>
                                                        
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            @else
                                <center>This Plan is not available</center>
                            @endif
                        </div>
                    </div>
                    <input type="hidden" id="hideit" value="0">
                </div>
            </div>
            <!-- Google Tag Manager (noscript) -->
<!-- <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NPFFMLH"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript> -->
<!-- End Google Tag Manager (noscript) -->
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5D6B3RX"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
            <script src="{{ asset('assets/admin/theme/js/the-datepicker.js') }}"></script>
            <script>
                (function() {
                    const input = document.getElementById('date_of_birth');
                    const datepicker = new TheDatepicker.Datepicker(input);
                    datepicker.render();
                })();
            </script>
            <script>
                document.getElementById("spiner").style.display = "none";
                let isPayInFull = false;
                let isDenefitsRedirected = true
                let isSubmitted = false

                function submitForm() {
                    isSubmitted = true
                }

                setInterval(function() {
                    if ($("#customer_email").val()) {
                        // $.ajax({
                        //     async: true,
                        //     url: "/client/finmkt-loan-status/" + $("#customer_email").val(),
                        //     method: 'GET',
                        //     global:false,
                        //     success: function(data) {
                        //         console.log('Finmkt payment status:', data.success)
                        //         if (data.success) {
                        //             alert('Finmkt payment has been successful!')
                        //             window.location.href = "{{ route('finmktPaymentStatus', ['status' => 'success', 'message' => 'Finmkt payment has been successful!']) }}"
                        //             clearInterval(this)
                        //         } else {
                        //             if (isDenefitsRedirected && data.finmkt && isSubmitted) {
                        //                 let form = document.getElementById('formfinanced')
                        //                 console.log('re-submitting the denefits form')

                        //                 form.action = "{{ route('planiframe.add') }}" + "?finmkt=true"
                        //                 form.submit()
                        //                 isDenefitsRedirected = false
                        //             }
                        //         }
                        //     }
                        // })
                    }
                }, 2000)

                function validate() {
                    // $("#spiner").text('Loading...');

                    document.getElementById("spiner"), 'sp2';

                    console.log("saddam");
                    $(".error").hide();

                    var customer_first_name = $("#customer_first_name").val();
                    var customer_last_name = $("#customer_last_name").val();
                    var customer_email = $("#customer_email").val();
                    var customer_address = $("#customer_address").val();
                    var zipcode = $("#zipcode").val();
                    var date_of_birth = $("#date_of_birth").val();
                    // var start_date = $("#start_date").val();
                    var customer_mobile = $("#customer_mobile").val();
                    var paytype = $("#paytype").val();


                    if (customer_first_name == "") {
                        $(".fnameerror").show();
                        $(".fa-spin").hide();
                        return false;
                    }

                    if (customer_last_name == "") {
                        $(".lnameerror").show();
                        $(".fa-spin").hide();
                        return false;
                    }

                    if (customer_email == "") {
                        $(".emailerror").show();
                        $(".fa-spin").hide();
                        return false;
                    }
                    if (!validateEmail(customer_email)) {
                        $(".validemailerror").show();
                        $(".fa-spin").hide();
                        return false;
                    }
                    if (customer_address == "") {
                        $(".addresserror").show();
                        $(".fa-spin").hide();
                        return false;
                    }
                    if (zipcode == "") {
                        $(".ziperror").show();
                        $(".fa-spin").hide();
                        return false;
                    }
                    if (date_of_birth == "") {
                        $(".birtherror").show();
                        $(".fa-spin").hide();
                        return false;
                    }

                    // if (start_date == "") {
                    //     $(".starterror").show();
                    //     return false;
                    // }
                    if (customer_mobile == "") {
                        $(".phoneerror").show();
                        $(".fa-spin").hide();
                        return false;
                    }
                    if (customer_mobile.length > 10 || customer_mobile.length < 10) {
                        $(".phonelentgherror").show();
                        $(".fa-spin").hide();
                        return false;
                    }

                    var email = customer_email;
                    var emailFailed;



                    //$(".sub-btn").click(function() {


                    //});
                }

                function validateEmail(email) {
                    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
                    return regex.test(email);
                }

                const getAge = birthDate => Math.floor((new Date() - new Date(birthDate).getTime()) / 3.15576e+10)

                jQuery(document).ready(function($) {
                    $("#terms").click(function() {
                        if ($(this).prop("checked") == true) {
                            $("#showButtons").show();
                        } else if ($(this).prop("checked") == false) {
                            $("#showButtons").hide();
                        }
                    });
                    $(".clickbutton").click(function() {
                        isPayInFull = false
                        $("#formfinanced").show();
                        $('#paytype').val('getfinanced');
                        $(".development-inner").hide();
                        $('.sub-btn').removeAttr("type").attr("type", "submit");


                    });
                    $(".payinfull").click(function() {
                        isPayInFull = true
                        $("#formfinanced").show();
                        $('#paytype').val('payinfull');
                        $(".development-inner").hide();
                        $('.sub-btn').removeAttr("type").attr("type", "button");

                    });

                    $(".goback").click(function() {
                        $("#formfinanced").hide();
                        $(".development-inner").show();
                    });
                });
                var hit = 0;
                history.pushState(null, null, location.href);
                history.back();
                history.forward();
                window.onpopstate = function() {
                    if ($("#hideit").val() == 0) {
                        $("#formfinanced").hide();
                        $(".development-inner").show();
                    }
                    history.go(1);
                };

                //terms and conditions

                $(".read-terms-and-conditions").click(function() {
                    $(".terms-and-conditions-content").toggle();
                });

                //on submit
                $(".sub-btn").click(function() {


                    $(".sub-btn").attr('disabled', true);
                    $(".sub-btn").val('loading.....');



                    // $("#spiner").text('Loading... ');

                    console.log("carpe");
                    $(".error").hide();

                    var customer_first_name = $("#customer_first_name").val();
                    var customer_last_name = $("#customer_last_name").val();
                    var customer_email = $("#customer_email").val();
                    var customer_address = $("#customer_address").val();
                    var zipcode = $("#zipcode").val();
                    var date_of_birth = $("#date_of_birth").val();

                    // var start_date = $("#start_date").val();

                    var customer_mobile = $("#customer_mobile").val();
                    var paytype = $("#paytype").val();

                    //keys
                    var stripe_public_key = $("#stripe_public_key").val();
                    var stripe_secret_key = $("#stripe_secret_key").val();
                    var plaid_public_key = $("#plaid_public_key").val();
                    var plaid_secret_key = $("#plaid_secret_key").val();
                    var service_amount = $("#service_amount").val();
                    var denefit_token = $("#denefit_token").val();
                    var client_id = $("#client_id").val();
                    var plan_id = $("#plan_id").val();


                    if (customer_first_name == "") {
                        $(".fnameerror").show();
                        $(".fa-spin").hide();

                        $(".sub-btn").attr('disabled', false);
                        $(".sub-btn").val('Submit');
                        return false;
                    }

                    let isValidFirstName = /^[a-zA-Z ]{2,30}$/.test(customer_first_name)

                    if (!isValidFirstName) {
                        $(".fnameerror-invalid").show();
                        $(".fa-spin").hide();
                        $(".sub-btn").attr('disabled', false);
                        $(".sub-btn").val('Submit');
                        return false;
                    }


                    if (customer_last_name == "") {
                        $(".lnameerror").show();
                        $(".fa-spin").hide();
                        $(".sub-btn").attr('disabled', false);
                        $(".sub-btn").val('Submit');
                        return false;
                    }

                    let isValidLastName = /^[a-zA-Z ]{2,30}$/.test(customer_last_name)

                    if (!isValidLastName) {
                        $(".lnameerror-invalid").show();
                        $(".fa-spin").hide();
                        $(".sub-btn").attr('disabled', false);
                        $(".sub-btn").val('Submit');
                        return false;
                    }


                    if (customer_email == "") {
                        $(".emailerror").show();
                        $(".fa-spin").hide();
                        $(".sub-btn").attr('disabled', false);
                        $(".sub-btn").val('Submit');
                        return false;
                    }
                    if (!validateEmail(customer_email)) {
                        $(".validemailerror").show();
                        $(".fa-spin").hide();
                        $(".sub-btn").attr('disabled', false);
                        $(".sub-btn").val('Submit');
                        return false;
                    }
                    if (customer_address == "") {
                        $(".addresserror").show();
                        $(".fa-spin").hide();
                        $(".sub-btn").attr('disabled', false);
                        $(".sub-btn").val('Submit');
                        return false;
                    }
                    if (zipcode == "") {
                        $(".ziperror").show();
                        $(".fa-spin").hide();
                        $(".sub-btn").attr('disabled', false);
                        $(".sub-btn").val('Submit');
                        return false;
                    }

                    let isValidZipCode = /(^\d{5}$)|(^\d{5}-\d{4}$)/.test(zipcode)

                    if (!isValidZipCode) {
                        $(".ziperror-invalid").show();
                        $(".fa-spin").hide();
                        $(".sub-btn").attr('disabled', false);
                        $(".sub-btn").val('Submit');
                        return false;
                    }


                    if (date_of_birth == "") {
                        $(".birtherror").show();
                        $(".fa-spin").hide();
                        $(".sub-btn").attr('disabled', false);
                        $(".sub-btn").val('Submit');
                        return false;
                    }

                    let bd = getAge(date_of_birth.replace(/ /g, '').split('.').join('-').split("-").reverse().join("-"))

                    if (bd < 18) {
                        $(".birtherror-age").show();
                        $(".fa-spin").hide();
                        $(".sub-btn").attr('disabled', false);
                        $(".sub-btn").val('Submit');
                        return false;
                    }

                    if (customer_mobile == "") {
                        $(".phoneerror").show();
                        $(".fa-spin").hide();
                        $(".sub-btn").attr('disabled', false);
                        $(".sub-btn").val('Submit');
                        return false;
                    }
                    if (customer_mobile.length > 10 || customer_mobile.length < 10) {
                        $(".phonelentgherror").show();
                        $(".fa-spin").hide();
                        $(".sub-btn").attr('disabled', false);
                        $(".sub-btn").val('Submit');
                        return false;
                    }



                    //validate email via bounceless api

                    var email = $("#customer_email").val();
                    var emailFailed;
                    //$(".fa-spin").show();  
                    $(".sub-btn").attr('disabled', true);
                    $(".sub-btn").val('loading.....');

                    $.ajax({
                        async: false,
                        url: "/email-validation/" + email,
                        method: 'GET',
                        data: {},
                        global: false,
                        success: function(data) {

                            if (data == 'valid') {
                                emailFailed = false;
                                $(".validemailerror").hide();

                            } else {
                                emailFailed = true;
                                $('.validemailerror').text('Invalid Email Adrress.');
                                $(".validemailerror").show();
                            }
                            $(".sub-btn").attr('disabled', false);
                            $(".sub-btn").val('Submit');

                            // console.log(data);
                        },
                        error: function(status, error) {
                            var emailFailed = true;
                            $('.validemailerror').text(
                                'Error validating Email, please try again in a few minutes.');
                            $(".validemailerror").show();
                            $(".sub-btn").attr('disabled', false);
                            $(".sub-btn").val('Submit');
                            console.log("Error requesting API for email validation");

                        }
                    });

                    if (emailFailed) {
                        $(".fa-spin").hide();
                        $(".sub-btn").attr('disabled', false);
                        $(".sub-btn").val('Submit');
                        return false;

                    }


                    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
                    document.getElementById("loading").style.display = "block";
                    document.getElementById("spiner").style.display = "block";

                    if (isPayInFull) {
                        $.ajax({
                            /* the route pointing to the post function */
                            url: '/plan/plaidtoken',
                            type: 'POST',
                            /* send the csrf-token and the input to the controller */
                            data: {
                                _token: CSRF_TOKEN,
                                email: email,
                                plaid_public_key: plaid_public_key,
                                plaid_secret_key: plaid_secret_key,
                                client_id: client_id,
                                customer_first_name: customer_first_name,
                                customer_last_name: customer_last_name,
                                customer_address: customer_address,
                                zipcode: zipcode,
                                customer_mobile: customer_mobile,
                                client_id: client_id,
                                plan_id: plan_id
                            },
                            // dataType: 'JSON',
                            /* remind that 'data' is the response of the AjaxController */
                            success: function(response) {
                                console.log(response, 'check mash')
                                const configs = {
                                    // Pass the link_token generated in step 2.
                                    token: response.link_token,
                                    onLoad: function() {
                                        // The Link module finished loading.
                                    },
                                    onSuccess: function(public_token, metadata) {

                                        console.log('Public Token: ' + public_token);
                                        $('.payment-status').show();
                                        $("#formfinanced").hide();
                                        $(".development-inner").hide();
                                        document.getElementById("loading").style.display = "none";
                                        document.getElementById("spiner").style.display = "none";

                                        switch (metadata.accounts.length) {
                                            case 0:
                                                break;
                                            case 1:
                                                console.log('Customer-selected account ID: ' + metadata
                                                    .accounts[0].id);
                                                break;
                                            default:
                                        }

                                        var data = {
                                            'publicToken': public_token,
                                            'accountId': metadata.accounts[0].id,
                                            'stripeCustomer': customer_email,
                                            'firstName': customer_first_name,
                                            'lastName': customer_last_name,
                                            'phone': customer_mobile,
                                            'serviceAmount': service_amount
                                        };

                                        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
                                        $.ajax({
                                            /* the route pointing to the post function */
                                            url: '/plan/getPayment',
                                            type: 'POST',
                                            /* send the csrf-token and the input to the controller */
                                            data: {
                                                _token: CSRF_TOKEN,
                                                publicToken: public_token,
                                                accountId: metadata.accounts[0].id,
                                                stripeCustomer: customer_email,
                                                firstName: customer_first_name,
                                                lastName: customer_last_name,
                                                phone: customer_mobile,
                                                stripe_public_key: stripe_public_key,
                                                stripe_secret_key: stripe_secret_key,
                                                plaid_public_key: plaid_public_key,
                                                plaid_secret_key: plaid_secret_key,
                                                denefit_token: denefit_token,
                                                client_id: client_id,
                                                service_amount: service_amount,
                                                customer_id: response.customer_id
                                            },
                                            dataType: 'JSON',
                                            /* remind that 'data' is the response of the AjaxController */
                                            success: function(response) {
                                                console.log('done plaid', response);
                                            }
                                        });


                                    },
                                    onExit: async function(err, metadata) {
                                        if (err != null) {
                                            $(".fa-spin").hide();
                                            console.log('error')
                                        }

                                    },
                                };

                                var linkHandler = Plaid.create(configs);
                                linkHandler.open();


                            },

                            error: function() {
                                console.log("Error getting data");
                                //$(".fa-spin").hide();
                            },

                        });
                    }

                });
            </script>

        </div>
    </div>

</body>

</html>
