<x-iframe-header />
<style>
    body iframe {
        width: 100%;
        height: 135vh;
    }
</style>

<body>
    <iframe id="denefits_iframe" src="{{ $data['iframe_url'] }}">Your browser isn't compatible</iframe>
</body>
<script type="text/javascript">
    let customer_id = {{ $data['customer_id'] }};
    let service_amount = {{ $data['service_amount'] }};

    window.addEventListener('message', (evt) => {
        if (evt && evt.data && evt.data.type != 'webpackOk') {
            if (evt.origin == 'https://endpoint.denefits.com') {
                $.ajax({
                    async: false,
                    url: '{{ route('updateDenefitsPayment') }}',
                    method: 'GET',
                    data: {
                        customer_id,
                        service_amount
                    },
                })
            }
        }
    }, false);
</script>

</html>
