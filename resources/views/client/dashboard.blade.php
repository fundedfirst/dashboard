<?php
$title = "Dashboard";
?>
@extends('client.layout.app')
@section('content')

<?php
$notificationService = app('App\Services\NotificationService');
$helperService = app('App\Services\HelperService');
$authUser = Auth()->user();
?>

<div class="main-panel">
  <div class="content">
    <div class="panel-header bg-primary-gradient">
      <div class="page-inner">
        <div class="d-flex align-items-left align-items-md-center flex-column flex-md-row">
          <div>
            <h2 class="pb-2 fw-bold">Dashboard</h2>
          </div>
        </div>
      </div>
    </div>
    <div class="page-inner mt--5">
   <div class="row mt--2">
      <div class="col-md-12">
         <div class="card">
            <div class="card-body">
               <div class="row">
                  <div class="col-sm-6 col-md-4">
                     <a href="{{ route('client.customers') }}">
                        <div class="card card-stats mb-0 card-primary card-round">
                           <div class="card-body">
                              <div class="row">
                                 <div class="col-3">
                                    <div class="icon-big text-center">
                                       <i class="flaticon-users"></i>
                                    </div>
                                 </div>
                                 <div class="col-9 col-stats">
                                    <div class="numbers">
                                       <p class="card-category">Total Customers</p>
                                       <h4 class="card-title">{{$data['totalCustomers']}}</h4>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </a>
                  </div>
                  <div class="col-sm-3 col-md-4">
                     <a href="{{ route('client.customersPaments') }}">
                        <div class="card card-stats mb-0 card-info card-round">
                           <div class="card-body">
                              <div class="row">
                                 <div class="col-3">
                                    <div class="icon-big text-center">
                                     <i class="flaticon-coins text-success"></i>
                                    </div>
                                 </div>
                                 <div class="col-9 col-stats">
                                    <div class="numbers">
                                       <p class="card-category">Total Payments</p>
                                       <h4 class="card-title">$ {{$data['totalPayments']}}</h4>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </a>
                  </div>
                  <div class="col-sm-3 col-md-4">
                     <div class="card card-stats card-success card-round mb-0">
                        <div class="card-body">
                           <div class="row">
                              <div class="col-3">
                                 <div class="icon-big text-center">
                                    <i class="flaticon-analytics"></i>
                                 </div>
                              </div>
                              <div class="col-9 col-stats">
                                 <div class="numbers">
                                    <p class="card-category">Abandoned cart </p>
                                    <h4 class="card-title">{{$data['abandonedCarts']}}</h4>
                                 </div>
                                 <!-- <p>( customer payment not found in table )</p> -->
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <!-- Card With Icon States Background -->
               <!-- <div class="row">
                  <div class="col-sm-6 col-md-3">
                     <div class="card card-stats card-round mb-0">
                        <div class="card-body ">
                           <div class="row align-items-center">
                              <div class="col-icon">
                                 <div class="icon-big text-center icon-primary bubble-shadow-small">
                                    <i class="flaticon-agenda-1"></i>
                                 </div>
                              </div>
                              <div class="col col-stats ml-3 ml-sm-0">
                                 <div class="numbers">
                                    <p class="card-category">Clients</p>
                                    <h4 class="card-title">1</h4>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div> -->
               <!-- <div class="col col-stats ml-3 ml-sm-0">
                  <div class="numbers">
                    <p class="card-category">Clients</p>
                    <h4 >1</h4>
                  </div>
                </div> -->
            </div>
         </div>
      </div>

</div>
    </div>
    @endsection()