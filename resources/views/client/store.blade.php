<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <title>OCC - <?php echo isset($title) ? $title : 'Home'; ?></title>
    <link rel="icon" href="{{ asset('assets/admin/theme/img/cc-logo.png') }}" type="image/x-icon" />
    <meta content='width=device-width, initial-scale=1.0, shrink-to-fit=no' name='viewport' />
    <!-- Fonts and icons -->
    <script src="{{ asset('assets/client/theme/js/plugin/webfont/webfont.min.js') }}"></script>
    <script src="{{ asset('assets/admin/theme/js/clipboard.min.js') }}"></script>
    <script>
        WebFont.load({
            google: {
                "families": ["Lato:300,400,700,900"]
            },
            custom: {
                "families": ["Flaticon", "Font Awesome 5 Solid", "Font Awesome 5 Regular", "Font Awesome 5 Brands",
                    "simple-line-icons"
                ],
                urls: ['../assets/css/fonts.min.css']
            },
            active: function() {
                sessionStorage.fonts = true;
            }
        });
    </script>

    <!-- CSS Files -->
    <link rel="stylesheet" href="{{ asset('assets/client/theme/css/bootstrap.min.css') }}">
</head>
<style>
    .navbar {
        display: flex;
        align-items: center;
        padding: 50px;
    }

    nav {
        flex: 1;
        text-align: right;
    }

    nav ul {
        display: inline-block;
        list-style-type: none;
    }

    nav ul li {
        display: inline-block;
        margin-right: 20px;
    }

    a {
        text-decoration: none;
        color: #555;
    }

    p {
        color: #555;
    }

    .container {
        max-width: 1300px;
        margin: auto;
        padding-left: 25px;
        padding-right: 25px;
    }

    .row {
        display: flex;
        align-items: center;
        flex-wrap: wrap;
        justify-content: space-around;
    }

    .col-2 {
        flex-basis: 50%;
        min-width: 300px;
    }

    .col-2 img {
        max-width: 100%;
        padding: 50px 0;
    }

    .col-2 h1 {
        font-size: 50px;
        line-height: 60px;
        margin: 25px 0;
    }

    .btn {
        display: inline-block;
        background: transparent;
        border: 1px solid #9DC2FF;
        color: #2264D1;
        padding: 8px 30px;
        margin: 0 auto 80px;
    }

    .btn:hover {
        color: #9DC2FF;
    }

    .header {
        background: radial-gradient(#fff, #ffd6d6);
    }

    .header .row {
        margin-top: 70px;
    }

    .categories {
        margin: 70px 0;
    }

    .col-3 {
        flex-basis: 30%;
        min-width: 250px;
        margin-bottom: 30px;
    }

    .col-3 img {
        width: 100%;
    }

    .small-container {
        max-width: 1080px;
        margin: auto;
        padding-left: 25px;
        padding-right: 25px;
    }

    .col-4 {
        flex-basis: 25%;
        padding: 10px;
        min-width: 200px;
        margin-bottom: 50px;
        transition: transform 0.5s;
    }

    .col-4 img {
        width: 100%;
    }

    .title {
        text-align: center;
        margin: 0 auto 80px;
        position: relative;
        line-height: 60px;
        color: #555;
    }

    .title::after {
        content: "";
        background: #ff523b;
        width: 80px;
        height: 5px;
        border-radius: 5px;
        position: absolute;
        bottom: 0;
        left: 50%;
        transform: translate(-50%);
    }

    h4 {
        color: #555;
        font-weight: normal;
    }

    .col-4 p {
        font-size: 14px;
    }

    .rating .fas {
        color: #FB8200;
    }

    .rating .far {
        color: #FB8200;
    }

    /* .col-4:hover {
        transform: translateY(-5px);
    } */



    /* footer */
    .footer {
        background: #000;
        color: #8a8a8a;
        font-size: 14px;
        padding: 60px 0 20px;
    }

    .footer p {
        color: #8a8a8a;
    }

    .footer h3 {
        color: #ffffff;
        margin-bottom: 20px;
    }

    .footer-col-1,
    .footer-col-2,
    .footer-col-3,
    .footer-col-4 {
        min-width: 250px;
        margin-bottom: 20px;
    }

    .footer-col-1 {
        flex-basis: 30%;
    }

    .footer-col-2 {
        flex: 1;
        text-align: center;
    }

    .footer-col-2 img {
        width: 180px;
        margin-bottom: 20px;
    }

    .footer-col-3,
    .footer-col-4 {
        flex-basis: 12%;
        text-align: center;
    }

    ul {
        list-style-type: none;
    }

    .app-logo {
        margin-top: 20px;
    }

    .app-logo img {
        width: 140px;
    }

    .footer hr {
        border: none;
        background: #b5b5b5;
        height: 1px;
        margin: 20px 0;
    }

    .copyright {
        text-align: center;
    }

    .menu-icon {
        width: 28px;
        margin-left: 20px;
        display: none;
    }

    /* media query for menu */

    @media only screen and (max-width: 800px) {
        nav ul {
            position: absolute;
            top: 70px;
            left: 0;
            background: #333;
            width: 100%;
            overflow: hidden;
            transition: max-height 0.5s;
        }

        nav ul li {
            display: block;
            margin-right: 50px;
            margin-top: 10px;
            margin-bottom: 10px;
        }

        nav ul li a {
            color: #fff;
        }

        .menu-icon {
            display: block;
            cursor: pointer;
        }
    }

    /* all products page */

    .row-2 {
        justify-content: space-between;
        margin: 10px auto 50px;
    }

    select {
        border: 1px solid #9DC2FF;
        padding: 5px;
        margin-left: 5px
    }

    select:focus {
        outline: none;
    }

    .page-btn {
        margin: 0 auto 80px;
    }

    .page-btn span {
        display: inline-block;
        border: 1px solid #9DC2FF;
        margin-left: 10px;
        width: 40px;
        height: 40px;
        text-align: center;
        line-height: 40px;
        cursor: pointer;
    }

    /* .page-btn span:hover {
        background: #2264D1;
        color: #ffffff;
    } */

    /* single product details */
    input:focus {
        outline: none;
    }

    .small-img-row {
        display: flex;
        justify-content: space-between;
    }

    .small-img-col {
        flex-basis: 24%;
        cursor: pointer;
    }




    /* media query for less than 600 screen size */

    @media only screen and (max-width: 600px) {
        .row {
            text-align: center;
        }

        .col-2,
        .col-3,
        .col-4 {
            flex-basis: 100%;
        }

        .cart-info p {
            display: none;
        }
    }
</style>
<div class="container">
    <div class="navbar">
        <div class="logo">
            {{-- <a href="index.html"><img src="https://i.ibb.co/kDVwgwp/logo.png" alt="RedStore" width="125px" /></a> --}}
        </div>
        {{-- <form action="">
            <input type="text">
        </form> --}}
        {{-- <nav>
            <ul id="MenuItems">
                <li><a href="index.html">Home</a></li>
                <li><a href="product.html">Products</a></li>
                <li><a href="#">About</a></li>
                <li><a href="#">Contact</a></li>
                <li><a href="account.html">Account</a></li>
            </ul>

        </nav> --}}
        {{-- <a href="cart.html"><img src="https://i.ibb.co/PNjjx3y/cart.png" alt="" width="30px" height="30px" /></a>
        <img src="https://i.ibb.co/6XbqwjD/menu.png" alt="" class="menu-icon" onclick="menutoggle()" /> --}}
    </div>
</div>

<div class="small-container">
    <div class="row row-2">
        <div class="">
            <select>
                <option value="">Product Type</option>
                <option value="">Short by price</option>
                <option value="">Short by popularity</option>
                <option value="">Short by rating</option>
                <option value="">Short by sale</option>
            </select>
            <select>
                <option value="">Default Shorting</option>
                <option value="">Short by price</option>
                <option value="">Short by popularity</option>
                <option value="">Short by rating</option>
                <option value="">Short by sale</option>
            </select>
        </div>
        <div class="">
            <select>
                <option value="">Product Type</option>
                <option value="">Short by price</option>
                <option value="">Short by popularity</option>
                <option value="">Short by rating</option>
                <option value="">Short by sale</option>
            </select>
            <select>
                <option value="">Default Shorting</option>
                <option value="">Short by price</option>
                <option value="">Short by popularity</option>
                <option value="">Short by rating</option>
                <option value="">Short by sale</option>
            </select>
        </div>

    </div>
    <div class="row">
        <div class="col-4">
            <img src="https://i.ibb.co/47Sk9QL/product-1.jpg" alt="" />
            <h4>Red Printed T-shirt</h4>
            <div class="rating">
                <i class="fas fa-star"></i>
                <i class="fas fa-star"></i>
                <i class="fas fa-star"></i>
                <i class="fas fa-star"></i>
                <i class="far fa-star"></i>
            </div>
            <p>$4900</p>
            <a href="{{ route('iframe.store', ['price' => 4900, 'title' => 'Red Printed T-shirt', 'first_name' => 'francis', 'last_name' => 'Junior', 'email' => 'francis@mailinator.com']) }}"
                class="buy-button btn">Buy Now</a>
        </div>

        <div class="col-4">
            <img src="https://i.ibb.co/b7ZVzYr/product-2.jpg" alt="" />
            <h4>Black Sports Shoes</h4>
            <div class="rating">
                <i class="fas fa-star"></i>
                <i class="fas fa-star"></i>
                <i class="fas fa-star"></i>
                <i class="far fa-star"></i>
                <i class="far fa-star"></i>
            </div>
            <p>$900</p>
            <a href="{{ route('iframe.store', ['price' => 900, 'title' => 'Black Sports Shoes', 'first_name' => 'Tested', 'last_name' => 'Username', 'email' => 'Username@mailinator.com']) }}"
                class="buy-button btn">Buy Now</a>
        </div>

        <div class="col-4">
            <img src="https://i.ibb.co/KsMVr26/product-3.jpg" alt="" />
            <h4>Joggers for Men</h4>
            <div class="rating">
                <i class="fas fa-star"></i>
                <i class="fas fa-star"></i>
                <i class="fas fa-star"></i>
                <i class="fas fa-star"></i>
                <i class="fas fa-star-half-alt"></i>
            </div>
            <p>$7500</p>
            <a href="{{ route('iframe.store', ['price' => 7500, 'title' => 'Joggers for Men', 'first_name' => 'UserJoe', 'last_name' => 'Doe', 'email' => 'UserJoe@gmail.com']) }}"
                class="buy-button btn">Buy Now</a>
        </div>

        <div class="col-4">
            <img src="https://i.ibb.co/0cMfpcr/product-4.jpg" alt="" />
            <h4>Navy Blue T-shirt</h4>
            <div class="rating">
                <i class="fas fa-star"></i>
                <i class="fas fa-star"></i>
                <i class="far fa-star"></i>
                <i class="far fa-star"></i>
                <i class="far fa-star"></i>
            </div>
            <p>$700</p>
            <a href="{{ route('iframe.store', ['price' => 700, 'title' => 'Navy Blue  T-shirt', 'first_name' => 'Tested', 'last_name' => 'Username', 'email' => 'Username@mailinator.com']) }}"
                class="buy-button btn">Buy Now</a>
        </div>
    </div>

    <div class="row">
        <div class="col-4">
            <img src="https://i.ibb.co/47Sk9QL/product-1.jpg" alt="" />
            <h4>Red Printed T-shirt</h4>
            <div class="rating">
                <i class="fas fa-star"></i>
                <i class="fas fa-star"></i>
                <i class="fas fa-star"></i>
                <i class="fas fa-star"></i>
                <i class="far fa-star"></i>
            </div>
            <p>$4900</p>
            <a href="{{ route('iframe.store') }}" class="buy-button btn">Buy Now</a>
        </div>

        <div class="col-4">
            <img src="https://i.ibb.co/b7ZVzYr/product-2.jpg" alt="" />
            <h4>Red Printed T-shirt</h4>
            <div class="rating">
                <i class="fas fa-star"></i>
                <i class="fas fa-star"></i>
                <i class="fas fa-star"></i>
                <i class="far fa-star"></i>
                <i class="far fa-star"></i>
            </div>
            <p>$4900</p>
            <a href="{{ route('iframe.store') }}" class="buy-button btn">Buy Now</a>
        </div>

        <div class="col-4">
            <img src="https://i.ibb.co/KsMVr26/product-3.jpg" alt="" />
            <h4>Red Printed T-shirt</h4>
            <div class="rating">
                <i class="fas fa-star"></i>
                <i class="fas fa-star"></i>
                <i class="fas fa-star"></i>
                <i class="fas fa-star"></i>
                <i class="fas fa-star-half-alt"></i>
            </div>
            <p>$4900</p>
            <a href="{{ route('iframe.store') }}" class="buy-button btn">Buy Now</a>
        </div>

        <div class="col-4">
            <img src="https://i.ibb.co/0cMfpcr/product-4.jpg" alt="" />
            <h4>Red Printed T-shirt</h4>
            <div class="rating">
                <i class="fas fa-star"></i>
                <i class="fas fa-star"></i>
                <i class="far fa-star"></i>
                <i class="far fa-star"></i>
                <i class="far fa-star"></i>
            </div>
            <p>$4900</p>
            <a href="{{ route('iframe.store') }}" class="buy-button btn">Buy Now</a>
        </div>
    </div>

    <div class="page-btn">
        <span>1</span>
        <span>2</span>
        <span>3</span>
        <span>4</span>
        <span>&#8594;</span>
    </div>
</div>

<!-- Footer -->
{{-- <div class="footer">
    <div class="container">
        <div class="row">
            <div class="footer-col-1">
                <h3>Download Our App</h3>
                <p>Download App for Android and iso mobile phone.</p>
                <div class="app-logo">
                    <img src="https://i.ibb.co/KbPTYYQ/play-store.png" alt="" />
                    <img src="https://i.ibb.co/hVM4X2p/app-store.png" alt="" />
                </div>
            </div>

            <div class="footer-col-2">
                <img src="https://i.ibb.co/j3FNGj7/logo-white.png" alt="" />
                <p>
                    Our Purpose Is To Sustainably Make the Pleasure and Benefits of
                    Sports Accessible to the Many.
                </p>
            </div>

            <div class="footer-col-3">
                <h3>Useful Links</h3>
                <ul>
                    <li>Coupons</li>
                    <li>Blog Post</li>
                    <li>Return Policy</li>
                    <li>Join Affiliate</li>
                </ul>
            </div>

            <div class="footer-col-4">
                <h3>Follow us</h3>
                <ul>
                    <li>Facebook</li>
                    <li>Twitter</li>
                    <li>Instagram</li>
                    <li>YouTube</li>
                </ul>
            </div>
        </div>
        <hr />
        <p class="copyright">Copyright &copy; 2022</p>
    </div>
</div> --}}

<!-- js for toggle menu -->
<script>
    var MenuItems = document.getElementById('MenuItems');
    MenuItems.style.maxHeight = '0px';

    function menutoggle() {
        if (MenuItems.style.maxHeight == '0px') {
            MenuItems.style.maxHeight = '200px';
        } else {
            MenuItems.style.maxHeight = '0px';
        }
    }
</script>
