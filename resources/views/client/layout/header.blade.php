<!DOCTYPE html>
<html lang="en">

<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<title> Client OCC - <?php echo ((isset($title)) ? $title : 'Home'); ?></title>
	<link rel="icon" href="{{asset('assets/admin/theme/img/cc-logo.png')}}" type="image/x-icon" />
	<meta content='width=device-width, initial-scale=1.0, shrink-to-fit=no' name='viewport' />
	<!-- <link rel="stylesheet" href="{{asset('assets/client/custom.css')}}"> -->
	<link rel="stylesheet" href="{{asset('assets/admin/custom.css')}}">
	<link rel="stylesheet" href="{{asset('assets/css/fonts.min.css')}}">
	<!-- Fonts and icons -->
	<script src="{{asset('assets/client/theme/js/plugin/webfont/webfont.min.js')}}"></script>
	<script src="{{asset('assets/admin/theme/js/clipboard.min.js')}}"></script>
	<script>
		WebFont.load({
			google: {
				"families": ["Lato:300,400,700,900"]
			},
			custom: {
				"families": ["Flaticon", "Font Awesome 5 Solid", "Font Awesome 5 Regular", "Font Awesome 5 Brands", "simple-line-icons"],
				urls: ['../assets/css/fonts.min.css']
			},
			active: function() {
				sessionStorage.fonts = true;
			}
		});
	</script>

	<!-- CSS Files -->
	<link rel="stylesheet" href="{{asset('assets/client/theme/css/bootstrap.min.css')}}">
	<link rel="stylesheet" href="{{asset('assets/admin/theme/css/atlantis.min.css')}}">

	<!-- CSS Just for demo purpose, don't include it in your project -->
	<link rel="stylesheet" href="{{asset('assets/client/theme/css/demo.css')}}">
</head>

<body class="denifit_dashboard">

	<div class="main-header">
		<!-- Logo Header -->
		<div class="logo-header" data-background-color="blue">

			<a href="{{route('client.dashboard')}}" class="logo">
			<img src="{{asset('assets/admin/theme/img/cc-logo.png')}}" alt="navbar brand" class="navbar-brand" width="66px">
			</a>
			<!-- <button class="navbar-toggler sidenav-toggler ml-auto" type="button" data-toggle="collapse" data-target="collapse" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon">
					<i class="icon-menu"></i>
				</span>
			</button>
			<button class="topbar-toggler more"><i class="icon-options-vertical"></i></button>
			<div class="nav-toggle">
				<button class="btn btn-toggle toggle-sidebar">
					<i class="icon-menu"></i>
				</button>
			</div> -->
		</div>
		<!-- End Logo Header -->

		<!-- Navbar Header -->
		<nav class="navbar navbar-header navbar-expand-lg" data-background-color="blue2">

			<div class="container-fluid">
				<div class="collapse" id="search-nav">
					<!-- <form class="navbar-left navbar-form nav-search mr-md-3">
						<div class="input-group">
							<div class="input-group-prepend">
								<button type="submit" class="btn btn-search pr-1">
									<i class="fa fa-search search-icon"></i>
								</button>
							</div>
							<input type="text" placeholder="Type here to search" class="form-control">
						</div>
					</form> -->
				</div>
				<ul class="navbar-nav topbar-nav ml-md-auto align-items-center">
					<li class="nav-item toggle-nav-search hidden-caret">
						<a class="nav-link" data-toggle="collapse" href="#search-nav" role="button" aria-expanded="false" aria-controls="search-nav">
							<i class="fa fa-search"></i>
						</a>
					</li>
					<li class="nav-item dropdown hidden-caret">
						<a class="dropdown-toggle profile-pic" data-toggle="dropdown" href="#" aria-expanded="false">
							<div class="avatar-sm">
							@if(auth()->user()->profile_image)
                        <img src="{{auth()->user()->profile_image}}" class="avatar-img rounded-circle">
                        @else
						<img src="{{asset('assets/admin/theme/img/profile_placeholder.png')}}" alt="..." class="avatar-img rounded-circle">
                        @endif
							</div>
						</a>
						<ul class="dropdown-menu dropdown-user animated fadeIn">
							<div class="dropdown-user-scroll scrollbar-outer">
								<li>
									<div class="user-box">
										<div class="avatar-lg">
											@if(auth()->user()->profile_image)
					                        	<img src="{{auth()->user()->profile_image}}" class="avatar-img rounded">
					                        @else
												<img src="{{asset('assets/client/theme/img/profile_placeholder.png')}}" alt="image profile" class="avatar-img rounded">
					                        @endif
										</div>
										<div class="u-text">
											<h4>{{auth()->user()->full_name}}</h4>
											<p class="text-muted">{{auth()->user()->email}}</p><a href="{{route('client.editMyProfile')}}" class="btn btn-xs btn-secondary btn-sm">Edit Profile</a>
										</div>
									</div>
								</li>
								<li>
									<div class="dropdown-divider"></div>
									<a class="dropdown-item" href="{{route('client.myProfile')}}">My Profile</a>
									<a class="dropdown-item" href="{{route('client.logout')}}">Logout</a>
								</li>
							</div>
						</ul>
					</li>
				</ul>
			</div>
		</nav>
		<!-- End Navbar -->
	</div>

	<!-- Sidebar -->
	<div class="sidebar sidebar-style-2">
		<div class="sidebar-wrapper scrollbar scrollbar-inner">
			<div class="sidebar-content">
				<div class="user">
					<div class="avatar-sm float-left mr-2">
					@if(auth()->user()->profile_image)
                        <img src="{{auth()->user()->profile_image}}" class="avatar-img rounded-circle">
                        @else
						<img src="{{asset('assets/admin/theme/img/profile_placeholder.png')}}" alt="..." class="avatar-img rounded-circle">
                        @endif					</div>
					<div class="info">
						<a data-toggle="collapse" href="#collapseExample" aria-expanded="true">
							<span>
								{{auth()->user()->full_name}}
								<span class="user-level">Client</span>
								<span class="caret"></span>
							</span>
						</a>
						<div class="clearfix"></div>

						<div class="collapse in" id="collapseExample">
							<ul class="nav">
								<li>
									<a href="{{route('client.myProfile')}}">
										<span class="link-collapse">My Profile</span>
									</a>
								</li>


							</ul>
						</div>
					</div>
				</div>
				<ul class="nav nav-primary">
					<li @class(['nav-item', request()->routeIs('client.dashboard') ? 'active' : ''])>
						<a data-toggle="collapse" href="#dashboard" class="collapsed" aria-expanded="false">
							<i class="fas fa-home"></i>
							<p>Dashboard</p>
							<span class="caret"></span>
						</a>
						<div class="collapse" id="dashboard">
							<ul class="nav nav-collapse">

								<li>
									<a href="{{route('client.dashboard')}}">
										<span class="sub-item">Dashboard</span>
									</a>
								</li>
							</ul>
						</div>
					</li>
					

					<!-- <li class="nav-section">
						<span class="sidebar-mini-icon">
							<i class="fa fa-ellipsis-h"></i>
						</span>
						<h4 class="text-section">Plans</h4>
					</li> -->

					<li @class(['nav-item', request()->routeIs(['clientplans.index', 'clientplans.create']) ? 'active' : ''])>
						<a data-toggle="collapse" href="#plans">
							<i class="fas fa-money-check-alt"></i>
							<p>Plans</p>
							<span class="caret"></span>
						</a>
						<div class="collapse" id="plans">
							<ul class="nav nav-collapse">
								<li style="list-style-type: none;">
									<a href="{{route('clientplans.index')}}">
										<span class="sub-item">All Plans</span>
									</a>
								</li>
								<li>
									<a href="{{route('clientplans.create')}}">
										<span class="sub-item">Add Plan</span>
									</a>
								</li>
							</ul>
						</div>
					</li>

					<li @class(['nav-item', request()->routeIs(['client.customers', 'client.customersPaments']) ? 'active' : ''])>
						<a data-toggle="collapse" href="#reports">
							<i class="fas fa-file-alt"></i>
							<p>Reports</p>
							<span class="caret"></span>
						</a>
						<div class="collapse" id="reports">
							<ul class="nav nav-collapse">
								<li style="list-style-type: none;">
									<a href="{{route('client.customers')}}">
										<span class="sub-item">Customers</span>
									</a>
								</li>
								<li>
									<a href="{{route('client.customersPaments')}}">
										<span class="sub-item">Customer Payments</span>
									</a>
								</li>
								
							</ul>
						</div>
					</li>

				</ul>
			</div>
		</div>
	</div>
	<!-- End Sidebar -->