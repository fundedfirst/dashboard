<?php
$title = "Customer Payments";
?>
@extends('client.layout.app')
@section('content')
<?php
$notificationService = app('App\Services\NotificationService');
$helperService = app('App\Services\HelperService');
$authUser = Auth()->user();
?>
<style>
   .the-datepicker__body.the-datepicker__body--swipeable {
      overflow-y: scroll;
      height: 200px;
   }

   span.the-datepicker__deselect {
      display: none;
   }

   .modal.fade .modal-dialog {
      min-height: 100vh;
      display: flex;
      align-items: center;
      margin: 0 auto;
   }
</style>
<div class="main-panel">
   <div class="content">
      <div class="panel-header bg-primary-gradient">
         <div class="page-inner">
            <div class="d-flex align-items-left align-items-md-center flex-column flex-md-row">
               <div>
                  <h2 class="text-black pb-2 fw-bold mb-3">Customer Payments</h2>
               </div>
            </div>
         </div>
      </div>
      <div class="page-inner mt--5">
         <div class="row mt--2">
            <div class="col-md-12">
               <div class="card">
                  <div class="card-body">
                     @if ($message = Session::get('success'))
                     <div class="alert alert-success alert-block" id="alert_success_session">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <strong>{{ $message }}</strong>
                     </div>
                     @endif
                     @if ($message = Session::get('error'))
                     <div class="alert alert-danger alert-block">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <strong>{{ $message }}</strong>
                     </div>
                     @endif
                     <!-- Card With Icon States Background -->
                     <form action="{{route('client.customersPaments')}}">
                     <div class="row">
                    
                     <div class="col-lg-5 col-md-5">
                           <div class="form-group p-0">
                              <label for="from_date">From Date</label>
                              <div id="datetimepicker1" class="">
                                 <input type="date" id="from_date" name="from_date" placeholder="11/12/2020" class="form-control" value="<?php if(isset($_GET['from_date'])){ echo $_GET['from_date']; } ?>">
                              </div>
                           </div>
                        </div>

                        <div class="col-lg-5 col-md-5">
                           <div class="form-group p-0">
                              <label for="to_date">To Date</label>
                              <div id="datetimepicker2" class="">
                                 <input type="date" id="to_date" name="to_date" placeholder="11/12/2020" class="form-control" value="<?php if(isset($_GET['to_date'])){ echo $_GET['to_date']; } ?>">
                              </div>
                           </div>
                        </div>
                        <div class="col-lg-2 col-md-2 ">
                           <div class="form-group inline-element p-0 text-right mt-3">
                              <button typ="submit" class="effect effect-5 btn btn-primary">
                                 <i class="fa fa-angle-right" aria-hidden="true"></i>
                                 <span>Filter</span>
                              </button>
                           </div>
                        </div>
                     
                     </div>
                     </form>
                  </div>
               </div>
            </div>
         </div>

         <div class="row">
         
            <div class="col-md-12">
               <div class="table-responsive">
                              <table class="table align-middle all-plan-table mb-0">
                                 <thead class="table-secondary">
                                    <tr>
                                       <th>Customer Name</th>
                                       <th>Customer Phone</th>
                                       <th>Offer Name</th>
                                       <th>Amount Paid</th>
                                       <th>Payment type</th>
                                       <th>Date</th>
                                       <th>Status</th>
                                    </tr>
                                 </thead>
                                 <tbody>
                                 @if(count($data['customerPayments']))
                                 @foreach($data['customerPayments'] as $index => $payment)
                                    <tr>
                                       <td>
                                          {{$payment->customer->full_name}}
                                       </td>
                                       <td>
                                          {{$payment->customer->phone}}
                                       </td>
                                       <td>
                                          {{$payment->plan ? $payment->plan->name : '-'}}
                                       </td>
                                       <td>
                                          @if($payment->amount) ${{$payment->amount}} @else - @endif
                                       </td>
                                       <td>
                                          {{ ($payment->type =='finmkt' ? 'Tier 1' :  ($payment->type =='denefits' ? 'Tier 2' : $payment->type) )}}
                                       </td>
                                      
                                       
                                       <td>
                                          {{$payment->created_at}}
                                       </td>
                                       <td>
                                          {{$payment->status}}
                                       </td>
                                       
                                    </tr>
                                    @endforeach
                                    @endif()
                                 </tbody>
                              </table>
                              {{$data['customerPayments']->appends(request()->input())->links()}}
                           </div>
               
            </div>
         
      </div>
      <!-- delete Modal -->
      <!-- <div class="modal fade" id="deletePlan" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content text-center">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body ">
                <h2>Are you sure want to create this Reports?</h2>
                <form class="mt-3 mb-3" action="" method="POST">
                    <button type="button" class="btn btn-danger">No</button>
                    <button type="submit" class="btn btn-success">Confirm</button>
                </form>
            </div>
        </div>
    </div>
</div> -->
      <script src="{{asset('assets/admin/theme/js/the-datepicker.js')}}"></script>
      <script>
         (function() {
            // const from  = document.getElementById('from_date');
            // const to    = document.getElementById('to_date');

            // const fromDatePicker = new TheDatepicker.Datepicker(from);
            // const toDatePicker   = new TheDatepicker.Datepicker(to);
            // fromDatePicker.render();
            // toDatePicker.render();
         })();
      </script>
      @endsection()