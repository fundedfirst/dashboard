<?php
$title = 'Add Plan';
?>
@extends('client.layout.app')
@section('content')

    <?php
    $notificationService = app('App\Services\NotificationService');
    $helperService = app('App\Services\HelperService');
    $authUser = Auth()->user();
    ?>
    <style>
        .switch {
            position: relative;
            display: inline-block;
            width: 35px;
            height: 25px;
        }

        .switch input {
            opacity: 0;
            width: 0;
            height: 0;
        }

        .slider {
            position: absolute;
            cursor: pointer;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            background-color: #ccc;
            -webkit-transition: .4s;
            transition: .4s;
        }

        .slider:before {
            position: absolute;
            content: "";
            height: 20px;
            width: 20px;
            left: 1px;
            bottom: 3px;
            background-color: white;
            -webkit-transition: .4s;
            transition: .4s;
        }

        input:checked+.slider {
            background-color: #2196F3;
        }

        input:focus+.slider {
            box-shadow: 0 0 1px #2196F3;
        }

        input:checked+.slider:before {
            -webkit-transform: translateX(15px);
            -ms-transform: translateX(15px);
            transform: translateX(15px);
        }

        /* Rounded sliders */
        .slider.round {
            border-radius: 34px;
        }

        .slider.round:before {
            border-radius: 50%;
        }
    </style>
    <div class="main-panel">
        <div class="content">
            <div class="page-inner">
                <div class="page-header">
                    <h4 class="page-title">Plan</h4>
                    <ul class="breadcrumbs">
                        <li class="nav-home">
                            <a href="{{ route('client.dashboard') }}">
                                <i class="flaticon-home"></i>
                            </a>
                        </li>
                        <li class="separator">
                            <i class="flaticon-right-arrow"></i>
                        </li>

                        <li class="nav-item">
                            <a href="javascript:;">Create A New Plan</a>
                        </li>
                    </ul>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <div class="card-title">Create A New Payment Plan</div>
                            </div>
                            <div class="card-body">
                                @if ($message = Session::get('success'))
                                    <div class="alert alert-success alert-block" id="alert_success_session">
                                        <button type="button" class="close" data-dismiss="alert">×</button>
                                        <strong>{{ $message }}</strong>
                                    </div>
                                @endif
                                @if ($message = Session::get('error'))
                                    <div class="alert alert-danger alert-block">
                                        <button type="button" class="close" data-dismiss="alert">×</button>
                                        <strong>{{ $message }}</strong>
                                    </div>
                                @endif

                                @if ($errors->any())
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif


                                <form action="{{ route('clientplans.store') }}" method="POST"
                                    enctype="multipart/form-data">
                                    @csrf
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <label for="">Offer Name</label>
                                                <input required class="form-control" type="text" name="name"
                                                    placeholder="Name">
                                            </div>
                                            <div class="col-md-6">
                                                <label for="">Offer Price</label>
                                                <input required min="1" class="form-control" type="number"
                                                    name="price" placeholder="Price">
                                            </div>

                                        </div>

                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <label for="">Initial Customer Payment To Get Started</label>
                                                <input required class="form-control" min="1" type="number"
                                                    name="down_payment" placeholder="Down payment">
                                            </div>
                                            <div class="col-md-6">
                                                <label for="">Months for Payment Installments</label>
                                                <input required class="form-control" min="2" max="24"
                                                    type="number" name="number_of_payment" placeholder="Number of payment">
                                            </div>

                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <label for="">VSL Link (YouTube/ Vimeo/ Wistia/ Video Embed
                                                    Code)</label>
                                                <input class="form-control" type="text" name="youtube_video_embed_code"
                                                    placeholder="Youtube video embed code">
                                            </div>
                                            <div class="col-md-6">
                                                <label for="">URL to Client Terms and Conditions Link (open in new
                                                    tab or popup)</label>
                                                <input id="terms_and_conditions" class="form-control" type="text"
                                                    name="terms_and_conditions"
                                                    placeholder="Place terms & condition link here">

                                            </div>

                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <label for="">Success Url</label>
                                                <input class="form-control" type="text" name="success_url"
                                                    placeholder="Success Url">
                                            </div>
                                            <!-- <div class="col-md-6">
                                                                <label for="">VSL Link</label>
                                                            <input  class="form-control" type="text" name="vsl_link" placeholder="VSL Link">

                                                            </div> -->

                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <label for="">Offer Description (10 word limit)</label>
                                                <textarea name="description" class="form-control" placeholder="Description"></textarea>

                                            </div>

                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <label for="image">Image</label>
                                                <input id="image" class="form-control" type="file" name="image"
                                                    accept="image/*">

                                            </div>

                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-6">

                                                <label for="status">Status</label>
                                                <select class="form-control" name="status">
                                                    <option selected value="active">Active</option>
                                                    <option value="inactive">Inactive</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <button type="submit" class="btn btn-primary">Add</button>
                                            </div>

                                        </div>
                                    </div>

                            </div>
                            </form>


                        </div>

                    </div>
                </div>

            </div>

        </div>
    </div>

    </div>
    </div>
    </div>


@endsection()
