<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Fonts -->
    <link rel="stylesheet" href="{{asset('assets/admin/custom.css')}}">
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,600" rel="stylesheet" type="text/css">
    <link rel="icon" href="Favicon.png">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
    <title>OCC - Client Login</title>
    <link rel="icon" href="{{asset('assets/admin/theme/img/cc-logo.png')}}" type="image/x-icon" />
    <style>
        @import url(https://fonts.googleapis.com/css?family=Raleway:300,400,600);

        .navbar-laravel {
            box-shadow: 0 2px 4px rgba(0, 0, 0, .04);
        }

        .navbar-brand,
        .nav-link,
        .my-form,
        .login-form {
            font-family: Raleway, sans-serif;
        }

        .my-form {
            padding-top: 1.5rem;
            padding-bottom: 1.5rem;
        }

        .login-form {
            padding-top: 1.5rem;
            padding-bottom: 1.5rem;
        }
        .navbar, .card-header{
            background-color: #1269db!important;
           
        }
        .navbar-brand, .nav-link, .card-header{
            color: white !important;
        }
    </style>
</head>

    <body>
        <nav class="navbar navbar-expand-lg navbar-light navbar-laravel">
            <div class="container">
                <a class="navbar-brand" href="{{route('home')}}">Home</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item">
                        </li>
                    </ul>
                </div>
            </div>
        </nav>

        <main class="my-form login_form">
            <div class="container authentication-card" style="background-color:#1269db;margin-top:30px;">
                <div class="row justify-content-center">
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 bg-login">
                        <img src="{{asset('assets/admin/theme/img/login-img.jpg')}}" alt="login-img" width="100%">
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="">
                                    <h2>Client Sign In</h2>
                                    <p>See your growth and get consulting support!</p>
                                </div>
                                @if ($message = Session::get('success'))
                                <div class="alert alert-success alert-block" id="alert_success_session">
                                    <button type="button" class="close" data-dismiss="alert">×</button>
                                    <strong>{{ $message }}</strong>
                                </div>
                                @endif
                                @if ($message = Session::get('error'))
                                <div class="alert alert-danger alert-block">
                                    <button type="button" class="close" data-dismiss="alert">×</button>
                                    <strong>{{ $message }}</strong>
                                </div>
                                @endif

                                @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                                @endif
                                <form action="{{route('client.login')}}" method="POST">
                                    {{csrf_field()}}

                                    <div class="form-group">
                                        <label for="email_address" class="col-form-label">E-Mail Address</label>
                                        
                                        <input required value="{{ old('email')}}" type="text" id="email_address" class="form-control" name="email">
                                        
                                    </div>

                                    <div class="form-group">
                                        <label for="password" class="col-form-label">Password</label>
                                        
                                            <input required type="password" id="password" class="form-control" name="password">
                                    
                                    </div>
                                    <div class="row" style="margin:0;">
                                        <div class="col-lg-6">
                                            <div class="form-check form-switch">
                                                <input class="form-check-input" type="checkbox" name="remember_me" id="flexSwitchCheckChecked">
                                                <label class="form-check-label" for="flexSwitchCheckChecked">Remember Me</label>
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group text-right">
                                                <a href="{{route('password.request')}}">&nbsp;&nbsp; Forgot Password?</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group mb-0">
                                        <button type="submit" class="btn btn-primary full_width_btn">
                                            Login
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        </main>

        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
        
    </body>

</html>