<?php
   $title = "Error";
   ?>
@extends('admin.layout.asset-app')
@section('content')
<?php
   $notificationService = app('App\Services\NotificationService');
   $helperService = app('App\Services\HelperService');
   $authUser = Auth()->user();
   ?>
<div class="main-panel">
   <div class="content">
      <div class="page-inner mt--5">
         <div class="card mb-0">
            <div class="card-body">
               <div class="error-404 ">
                  <div class="container">
                     <div class="row d-flex align-items-center justify-content-center">
                        <div class="col-xl-5">
                           <div class="card-body">
                              <h1 class="display-1"><span class="text-warning">5</span><span class="text-danger">0</span><span class="text-primary">0</span></h1>
                              <h2 class="font-weight-bold display-4">Sorry, unexpected error</h2>
                              <p>Looks like you are lost!
                                 <br>May be you are not connected to the internet!
                              </p>
                              <div class="mt-5">	<a href="{{route('home')}}" class="btn btn-lg btn-primary px-md-5 radius-30">Go Home</a>
                                 <a href="{{route('home')}}" class="btn btn-lg btn-outline-dark ms-3 px-md-5 radius-30">Back</a>
                              </div>
                           </div>
                        </div>
                        <div class="col-xl-7">
                           <img src="{{asset('assets/admin/theme/img/505-error.png')}}" class="img-fluid" alt="">
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
@endsection()