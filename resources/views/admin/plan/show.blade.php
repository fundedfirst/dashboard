<?php
$title = "Plan Detail";
?>
@extends('admin.layout.app')
@section('content')

<?php
$notificationService = app('App\Services\NotificationService');
$helperService = app('App\Services\HelperService');
$authUser = Auth()->user();
?>


<div class="main-panel">
    <div class="content">
        <div class="page-inner">
            <div class="page-header">
                <h4 class="page-title">Plan</h4>
                <ul class="breadcrumbs">
                    <li class="nav-home">
                        <a href="{{route('admin.dashboard')}}">
                            <i class="flaticon-home"></i>
                        </a>
                    </li>
                    <li class="separator">
                        <i class="flaticon-right-arrow"></i>
                    </li>

                    <li class="nav-item">
                        <a href="javascript:;">Plan</a>
                    </li>
                </ul>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="card-title">Plan Details</div>
                        </div>
                        <div class="card-body">

                            @if ($message = Session::get('success'))
                            <div class="alert alert-success alert-block" id="alert_success_session">
                                <button type="button" class="close" data-dismiss="alert">×</button>
                                <strong>{{ $message }}</strong>
                            </div>
                            @endif
                            @if ($message = Session::get('error'))
                            <div class="alert alert-danger alert-block">
                                <button type="button" class="close" data-dismiss="alert">×</button>
                                <strong>{{ $message }}</strong>
                            </div>
                            @endif

                            @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                            @endif

                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-6">
                                        <label for="plan_name">Plan Name</label>
                                            <input required class="form-control" type="text" id="plan_name" name="" value="{{$data['plan']->name}}" disabled>
                                        </div>
                                        <div class="col-md-6">
                                        <label for="last_name">Price</label>
                                            <input required class="form-control" type="text" id="plan_price" name="" value="{{$data['plan']->price}}" disabled>
                                        </div>

                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                    <div class="col-md-6">
                                            <label for="desc">Description</label>
                                            <textarea disabled required class="form-control" id="desc" >{{$data['plan']->description}}</textarea>
                                        </div>
                                        <div class="col-md-6">
                                            <label for="status">Status</label>
                                            <input required class="form-control" type="text" id="status" name="" value="{{$data['plan']->status}}" disabled>
                                        </div>
                        
                                    
                                    </div>
                                </div>
                                <div class="form-group">
                                    <a href="{{route('plans.edit', $data['plan']->id)}}"> <button type="button" class="btn btn-primary">Edit</button></a>
                                </div>
                        </div>
                     



                        </div>

                    </div>
                </div>

            </div>

        </div>
    </div>

</div>
</div>
</div>

@endsection()