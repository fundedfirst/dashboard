<?php
   $title = "Plans";
   ?>
@extends('admin.layout.app')
@section('content')
<?php
   $notificationService = app('App\Services\NotificationService');
   $helperService = app('App\Services\HelperService');
   $authUser = Auth()->user();
   ?>
<div class="main-panel">
   <div class="content">
      <div class="page-inner">
         <div class="page-header">
            <h4 class="page-title">Plans</h4>
            <ul class="breadcrumbs">
               <li class="nav-home">
                  <a href="{{route('admin.dashboard')}}">
                     <i class="flaticon-home"></i>
                  </a>
               </li>
               <li class="separator">
                  <i class="flaticon-right-arrow"></i>
               </li>
               <li class="nav-item">
                  <a href="javascript:;">Plans</a>
               </li>
            </ul>
         </div>
         <div class="row">
            <div class="col-md-12">
               <div class="card">
                  <div class="card-header">
                     <div class="">All Plans</div>
                  </div>
                  <div class="card-body">
                     @if ($message = Session::get('success'))
                     <div class="alert alert-success alert-block" id="alert_success_session">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <strong>{{ $message }}</strong>
                     </div>
                     @endif
                     @if ($message = Session::get('error'))
                     <div class="alert alert-danger alert-block">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <strong>{{ $message }}</strong>
                     </div>
                     @endif
                     @if ($errors->any())
                     <div class="alert alert-danger">
                        <ul>
                           @foreach ($errors->all() as $error)
                           <li>{{ $error }}</li>
                           @endforeach
                        </ul>
                     </div>
                     @endif
                     <div class="row  align-items-center">
                        <div class="col-lg-12">
                           <div class="table-responsive">
                              <table id="active_table" class="table align-middle all-plan-table mb-0">
                                 <thead  class="table-secondary">
                                    <tr>
                                       <th>Offer Name</th>
                                       <th>Total Price</th>
                                       <th>Down Payment</th>
                                       <th>Actions</th>
                                    </tr>
                                 </thead>
                                 <tbody>
                                 @if(count($data['plans']))
                                 @foreach($data['plans'] as $index => $plan)
                                    <tr>
                                       <td>
                                          <div class="d-flex align-items-center gap-3 cursor-pointer">
                                             <input class="" type="checkbox">
                                             <img src="{{asset('assets/admin/theme/img/avatar-1.png')}}" class="" width="35" height="35" alt="">
                                             <div class="">
                                                <h3 class="fw-bold">{{$plan->name}}</h3>
                                             </div>
                                          </div>
                                       </td>
                                       <td>
                                          <div class="price-value">
                                             <div class="value ">
                                                <span class="currency">$</span>
                                                <span class="amount">{{$plan->price}}</span>
                                                <span class="month">{{$plan->number_of_payment}}</span>
                                             </div>
                                          </div>
                                       </td>
                                       <td>
                                          <ul class="pricing-content">
                                             <li>Number of payment</li>
                                          </ul>
                                       </td>
                                       <td>
                                          <div class="table-actions d-flex align-items-center gap-3 fs-6">
                                             <textarea name="" class="clickcopyborad" rows="30" cols="40"/>
                                                <iframe src="{{env('APP_URL')}}{{'/plan/'}}{{$plan->id}}"></iframe>
                                             </textarea>
                                             <span class="copied-tooltip animated fadeInUp">
                                                URL Copied :)
                                             </span>
                                             <!-- <input type="text" class="clickcopyborad" id="occ-clipboard"  value="{{env('APP_URL')}}{{'/plan/'}}{{$plan->id}}"> -->
                                             <a href="{{route('plans.edit', $plan->id)}}" class="">
                                                <i class="fas fa-pencil-alt"></i>
                                             </a>
                                             <button type="button" title="" class="delete-btn " data-original-title="Delete" data-toggle="modal" data-target="#deletePlan-{{$plan->id}}"> <i class="fas fa-trash-alt"></i> </button>
                                             <!-- <button class="btn btn-clipboard" data-clipboard-action="copy" data-clipboard-target="#occ-clipboard">
                                                <i class="fa fa-clone" aria-hidden="true"></i>
                                             </button> -->
                                          </div>
                                       </td>
                                    </tr>
                                    @endforeach
                                    @endif()
                                 </tbody>
                              </table>
                           </div>
                        </div>
                        <!-- delete Modal -->
                        <div class="modal fade" id="deletePlan-{{$plan->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                           <div class="modal-dialog" role="document">
                              <div class="modal-content">
                                 <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Are you sure want to delete this Plan?</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                       <span aria-hidden="true">&times;</span>
                                    </button>
                                 </div>
                                 <div class="modal-body">
                                    {{$plan->name}}
                                 </div>
                                 <div class="modal-footer">
                                    <form action="{{route('plans.destroy', $plan->id)}}" method="POST">
                                       @method('delete')
                                       @csrf
                                       <button type="submit" class="btn btn-success">Confirm</button>
                                    </form>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
@endsection()