<?php
   $title = "Edit Plan";
   ?>
@extends('admin.layout.app')
@section('content')
<?php
   $notificationService = app('App\Services\NotificationService');
   $helperService = app('App\Services\HelperService');
   $authUser = Auth()->user();
   ?>
<div class="main-panel">
   <div class="content">
      <div class="page-inner">
         <div class="page-header">
            <h4 class="page-title">Plan</h4>
            <ul class="breadcrumbs">
               <li class="nav-home">
                  <a href="{{route('admin.dashboard')}}">
                  <i class="flaticon-home"></i>
                  </a>
               </li>
               <li class="separator">
                  <i class="flaticon-right-arrow"></i>
               </li>
               <li class="nav-item">
                  <a href="javascript:;">Edit A Plan</a>
               </li>
            </ul>
         </div>
         <div class="row">
            <div class="col-md-12">
               <div class="card">
                  <div class="card-header">
                     <div class="card-title">Edit Existing Payment Plan</div>
                  </div>
                  <div class="card-body">
                     @if ($message = Session::get('success'))
                     <div class="alert alert-success alert-block" id="alert_success_session">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <strong>{{ $message }}</strong>
                     </div>
                     @endif
                     @if ($message = Session::get('error'))
                     <div class="alert alert-danger alert-block">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <strong>{{ $message }}</strong>
                     </div>
                     @endif
                     @if ($errors->any())
                     <div class="alert alert-danger">
                        <ul>
                           @foreach ($errors->all() as $error)
                           <li>{{ $error }}</li>
                           @endforeach
                        </ul>
                     </div>
                     @endif
                     <form action="{{route('plans.update', $data['plan']->id)}}" method="POST" enctype="multipart/form-data">
                        @method('put')
                        @csrf
                        <div class="form-group">
                           <div class="row">
                              <div class="col-md-6">
                                 <label for="plan_name">Offer Name</label>
                                 <input required class="form-control" type="text" id="plan_name" name="name" value="{{$data['plan']->name}}" >
                              </div>
                              <div class="col-md-6">
                                 <label for="last_name">Offer Price</label>
                                 <input required class="form-control" type="text" id="plan_price" name="price" value="{{$data['plan']->price}}" >
                              </div>
                           </div>
                        </div>
                        <div class="form-group">
                           <div class="row">
                              <div class="col-md-6">
                                 <label for="">Initial Customer Payment To Get Started</label>
                                 <input required class="form-control" type="text" name="down_payment" value="{{$data['plan']->down_payment}}">
                              </div>
                              <div class="col-md-6">
                                 <label for="">Months for Payment Installments</label>
                                 <input required class="form-control" type="number" name="number_of_payment" value="{{$data['plan']->number_of_payment}}">
                              </div>
                           </div>
                        </div>
                        <div class="form-group">
                           <div class="row">
                              <div class="col-md-6">
                                 <label for="">VSL Link [YouTube/ Vimeo/ Wistia/ Video Embed Code]</label>
                                 <input required class="form-control" type="text" name="youtube_video_embed_code" value="{{$data['plan']->youtube_video_embed_code}}">
                              </div>
                              <div class="col-md-6">
                                 <label for="status">Status</label>
                                 <select class="form-control" name="status">
                                    <option <?php if($data['plan']->status =='Active'){echo 'selected';}?> value="active">Active</option>
                                    <option <?php if($data['plan']->status =='Inactive'){echo 'selected';}?> value="inactive">Inactive</option>
                                 </select>
                              </div>
                           </div>
                        </div>
                        <div class="form-group">
                           <div class="row">
                              <div class="col-md-12">
                                 <label for="desc">Offer Description (10 word limit)</label>
                                 <textarea required class="form-control" id="desc" name="description">{{$data['plan']->description}}</textarea>
                              </div>
                           </div>
                        </div>
                        <div class="form-group">
                           <button type="submit" class="btn btn-primary">Update</button>
                        </div>
                     </form>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
</div>
</div>
</div>
@endsection()