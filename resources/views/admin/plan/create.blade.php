<?php
$title = 'Add Plan';
?>
@extends('admin.layout.app')
@section('content')
    <?php
    $notificationService = app('App\Services\NotificationService');
    $helperService = app('App\Services\HelperService');
    $authUser = Auth()->user();
    ?>
    <div class="main-panel">
        <div class="content">
            <div class="page-inner">
                <div class="page-header">
                    <h4 class="page-title">Plan</h4>
                    <ul class="breadcrumbs">
                        <li class="nav-home">
                            <a href="{{ route('admin.dashboard') }}">
                                <i class="flaticon-home"></i>
                            </a>
                        </li>
                        <li class="separator">
                            <i class="flaticon-right-arrow"></i>
                        </li>
                        <li class="nav-item">
                            <a href="javascript:;">Create A New Plan</a>
                        </li>
                    </ul>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <div class="card-title">Create A New Payment Plan</div>
                            </div>
                            <div class="card-body">
                                @if ($message = Session::get('success'))
                                    <div class="alert alert-success alert-block" id="alert_success_session">
                                        <button type="button" class="close" data-dismiss="alert">×</button>
                                        <strong>{{ $message }}</strong>
                                    </div>
                                @endif
                                @if ($message = Session::get('error'))
                                    <div class="alert alert-danger alert-block">
                                        <button type="button" class="close" data-dismiss="alert">×</button>
                                        <strong>{{ $message }}</strong>
                                    </div>s
                                @endif
                                @if ($errors->any())
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif
                                <form action="{{ route('plans.store') }}" method="POST" enctype="multipart/form-data">
                                    @csrf
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <label for="">Offer Name</label>
                                                <input required class="form-control" type="text" name="name"
                                                    placeholder="Name">
                                            </div>
                                            <div class="col-md-6">
                                                <label for="">Offer Price</label>
                                                <input required class="form-control" type="number" name="price"
                                                    placeholder="Price">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <label for="">Initial Customer Payment To Get Started</label>
                                                <input required class="form-control" type="text" name="down_payment"
                                                    placeholder="Down payment">
                                            </div>
                                            <div class="col-md-6">
                                                <label for="">Months for Payment Installments</label>
                                                <input required class="form-control" type="number" name="number_of_payment"
                                                    placeholder="Number of payment">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <label for="">Video Link [YouTube/ Vimeo/ Wistia/ Video Embed
                                                    Code)</label>
                                                <input class="form-control" type="text" name="youtube_video_embed_code"
                                                    placeholder="Youtube video embed code">
                                            </div>
                                            <div class="col-md-6">
                                                <label for="">Offer Description (10 word limit)</label>
                                                <textarea name="description" class="form-control" placeholder="Description"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group pt-0">
                                        <button type="submit" class="btn btn-primary">Add</button>
                                    </div>
                            </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    </div>
    </div>
@endsection()
