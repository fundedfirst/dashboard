<?php
   $title = "notFound";
   ?>
@extends('admin.layout.asset-app')
@section('content')
<?php
   $notificationService = app('App\Services\NotificationService');
   $helperService = app('App\Services\HelperService');
   $authUser = Auth()->user();
   ?>
<div class="main-panel">
   <div class="content">
      <div class="page-inner">
         <div class="card mb-0">
            <div class="card-body">
               <div class="error-404">
                  <div class="container">
                     <div class="row d-flex align-items-center justify-content-center">
                        <div class="col col-xl-5">
                           <div class="card-body">
                              <h1 class="display-1">
                                 <span class="text-danger">4</span>
                                 <span class="text-primary">0</span>
                                 <span class="text-success">4</span>
                              </h1>
                              <h2 class="font-weight-bold display-4">Lost in Space</h2>
                              <p>
                                 You have reached the edge of the universe.
                                 <br>The page you requested could not be found.
                                 <br>Dont'worry and return to the previous page.
                              </p>
                              <div class="mt-5"> 
                                 <a href="{{route('home')}}" class="btn btn-primary btn-lg px-md-5 radius-30">Go Home</a>
                                 <a href="{{route('home')}}" class="btn btn-outline-dark btn-lg ms-3 px-md-5 radius-30">Back</a>
                              </div>
                           </div>
                        </div>
                        <div class="col-xl-7">
                           <img src="{{asset('assets/admin/theme/img/404-error.png')}}" class="img-fluid" alt="">
                        </div>
                        <!--end row-->
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
@endsection()