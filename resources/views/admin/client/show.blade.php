<?php
$title = 'Client Detail';
?>
@extends('admin.layout.app')
@section('content')
    <?php
    $notificationService = app('App\Services\NotificationService');
    $helperService = app('App\Services\HelperService');
    $authUser = Auth()->user();
    ?>
    <div class="main-panel">
        <div class="content">
            <div class="page-inner">
                <div class="page-header">
                    <h4 class="page-title">Client</h4>
                    <ul class="breadcrumbs">
                        <li class="nav-home">
                            <a href="{{ route('admin.dashboard') }}">
                                <i class="flaticon-home"></i>
                            </a>
                        </li>
                        <li class="separator">
                            <i class="flaticon-right-arrow"></i>
                        </li>

                        <li class="nav-item">
                            <a href="javascript:;">Client</a>
                        </li>
                    </ul>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <div class="card-title">Client Details</div>
                            </div>
                            <div class="card-body">
                                @if ($message = Session::get('success'))
                                    <div class="alert alert-success alert-block" id="alert_success_session">
                                        <button type="button" class="close" data-dismiss="alert">×</button>
                                        <strong>{{ $message }}</strong>
                                    </div>
                                @endif
                                @if ($message = Session::get('error'))
                                    <div class="alert alert-danger alert-block">
                                        <button type="button" class="close" data-dismiss="alert">×</button>
                                        <strong>{{ $message }}</strong>
                                    </div>
                                @endif

                                @if ($errors->any())
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif

                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label for="first_name">First Name</label>
                                            <input required class="form-control" type="text" id="first_name"
                                                name="first_name" value="{{ $data['client']->first_name }}" disabled>
                                        </div>
                                        <div class="col-md-5">
                                            <label for="last_name">Last Name</label>
                                            <input required class="form-control" type="text" id="last_name"
                                                name="last_name" value="{{ $data['client']->last_name }}" disabled>
                                        </div>

                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label for="email">Email</label>
                                            <input required class="form-control" id="email" type="email"
                                                name="email" value="{{ $data['client']->email }}" disabled>
                                        </div>
                                    </div>
                                </div>
                                @if(isset($data['company']->company_name))
                                <div class="border my-3"></div>
                                    <div class="form-group">
                                        <div class="row">
                                            <h3><b>Company Details</b></h3>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <label for="company_name">Company Name</label>
                                                <input class="form-control" id="company_name" type="text"
                                                    name="company_name" value="{{ $data['company']->company_name }}" disabled>
                                            </div>
                                            <div class="col-md-6">
                                                <label for="company_address">Company Address</label>
                                                <input class="form-control" id="company_address" type="text"
                                                    name="company_address" value="{{ $data['company']->company_address }}" disabled>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <label for="ein_number">EIN Number</label>
                                                <input class="form-control" id="ein_number" type="text"
                                                    name="ein_number" value="{{ $data['company']->ein_number }}" disabled>
                                            </div>
                                            <div class="col-md-6">
                                                <label for="soi">State Of Incorporation</label>
                                                <input class="form-control" id="soi" type="text"
                                                    name="soi" value="{{ $data['company']->state_of_incorporation }}" disabled>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <label for="city">City</label>
                                                <input class="form-control" id="city" type="text"
                                                    name="city" value="{{ $data['company']->city }}" disabled>
                                            </div>
                                            <div class="col-md-6">
                                                <label for="zip_code">Zip Code</label>
                                                <input class="form-control" id="zip_code" type="text"
                                                    name="zip_code" value="{{ $data['company']->zip_code }}" disabled>
                                            </div>
                                        </div>
                                    </div>
                                    @endif
                                <div class="border my-3"></div>

                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label for="plaid_public_key">Plaid Public Key</label>
                                            <input disabled required class="form-control" id="plaid_public_key"
                                                type="text" name="plaid_public_key"
                                                value="{{ $data['client']->plaid_public_key }}">
                                        </div>
                                        <div class="col-md-6">
                                            <label for="plaid_secret_key">Plaid Secret Key</label>
                                            <input disabled required class="form-control" id="plaid_secret_key"
                                                type="text" name="plaid_secret_key"
                                                value="{{ $data['client']->plaid_secret_key }}">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label for="denefit_token">Tier 2 (Funding Live Token)</label>
                                            <input disabled required class="form-control" id="denefit_token" type="text"
                                                name="denefit_token" value="{{ $data['client']->denefit_token }}">
                                        </div>
                                        <div class="col-md-6">
                                            <label for="plaid_public_key">Webhook Url</label>
                                            <input disabled required class="form-control" id="webhook_url" type="text"
                                                name="webhook_url" value="{{ $data['client']->webhook_url }}">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label for="finmkt_user_id">Tier 1 User ID</label>
                                            <input class="form-control" id="finmkt_user_id" type="text"
                                                name="finmkt_user_id" value="{{ $data['client']->finmkt_user_id }}"
                                                disabled>
                                        </div>
                                        <div class="col-md-6">
                                            <label for="finmkt_base_url">Tier 1 Base URL</label>
                                            <input class="form-control" id="finmkt_base_url" type="text"
                                                name="finmkt_base_url" value="{{ $data['client']->finmkt_base_url }}"
                                                disabled>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label for="finmkt_username">Tier 1 Username</label>
                                            <input class="form-control" id="finmkt_username" type="text"
                                                name="finmkt_username" value="{{ $data['client']->finmkt_username }}"
                                                disabled>
                                        </div>
                                        <div class="col-md-6">
                                            <label for="finmkt_password">Tier 1 Password</label>
                                            <input class="form-control" id="finmkt_password" type="text"
                                                name="finmkt_password" value="{{ $data['client']->finmkt_password }}"
                                                disabled>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label for="tier1_toggled">Tier 1 Redirect</label>
                                            <input class="form-control" id="tier1_toggled" type="text"
                                                name="tier1_toggled"
                                                value="{{ $data['client']->tier1_toggled ? 'Yes' : 'No' }}" disabled>
                                        </div>
                                        <div class="col-md-6">
                                            <label for="tier2_toggled">Tier 2 Redirect</label>
                                            <input class="form-control" id="tier2_toggled" type="text"
                                                name="tier2_toggled"
                                                value="{{ $data['client']->tier2_toggled ? 'Yes' : 'No' }}" disabled>
                                        </div>
                                    </div>
                                </div><br>

                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label for="dynamic_price_key">Dynamic Price API key</label>
                                            <input class="form-control" id="dynamic_price_key" type="text"
                                                name="dynamic_price_key" value="{{ $data['client']->dynamic_price_key }}"
                                                readonly>
                                        </div>
                                    </div>
                                </div><br>
                                <a href="{{ route('clients.edit', $data['client']->id) }}"> <button type="button"
                                        class="btn btn-primary">Edit</button></a>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    </div>
    </div>


@endsection()
