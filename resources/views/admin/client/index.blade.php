<?php
$title = 'Clients';
?>
@extends('admin.layout.app')
@section('content')
    <?php
    $notificationService = app('App\Services\NotificationService');
    $helperService = app('App\Services\HelperService');
    $authUser = Auth()->user();
    ?>
    <div class="main-panel">
        <div class="content">
            <div class="page-inner">
                <div class="page-header">
                    <h4 class="page-title">Clients</h4>
                    <ul class="breadcrumbs">
                        <li class="nav-home">
                            <a href="{{ route('admin.dashboard') }}">
                                <i class="flaticon-home"></i>
                            </a>
                        </li>
                        <li class="separator">
                            <i class="flaticon-right-arrow"></i>
                        </li>
                        <li class="nav-item">
                            <a href="javascript:;">Clients</a>
                        </li>
                    </ul>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <div class="card-title">All Clients</div>
                            </div>
                            <div class="card-body">
                                @if ($message = Session::get('success'))
                                    <div class="alert alert-success alert-block" id="alert_success_session">
                                        <button type="button" class="close" data-dismiss="alert">×</button>
                                        <strong>{{ $message }}</strong>
                                    </div>
                                @endif
                                @if ($message = Session::get('error'))
                                    <div class="alert alert-danger alert-block">
                                        <button type="button" class="close" data-dismiss="alert">×</button>
                                        <strong>{{ $message }}</strong>
                                    </div>
                                @endif
                                @if ($errors->any())
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif
                                <div class="table-responsive">
                                    <!-- <label class="float-right">
                                        search:
                                        <input type="search" class="form-control from-control-sm" id="searchdata">                                        
                                    </label> -->
                                    <table id="basic-datatablesnew" class="display mb-0 table table-striped table-hover">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>First Name</th>
                                                <th>Last Name</th>
                                                <th>Email</th>
                                                <th>Status</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @if (count($data['clients']))
                                                @foreach ($data['clients'] as $index => $client)
                                                    <tr>
                                                        <td>{{ ++$index }}</td>
                                                        <td>{{ $client['first_name'] }}</td>
                                                        <td>{{ $client['last_name'] }}</td>
                                                        <td>{{ $client['email'] }}</td>
                                                        <td>{{ $client['status'] }}</td>
                                                        <td class="flex-section">
                                                            <a href="{{ route('clients.show', $client['id']) }}"><i
                                                                    class="fa fa-eye"></i></a>
                                                            <span>|</span>
                                                            <button type="button" title=""
                                                                class="btn btn-link btn-danger p-0"
                                                                data-original-title="Delete" data-toggle="modal"
                                                                data-target="#deleteClient-{{ $client['id'] }}"> <i
                                                                    class="fa fa-times"></i> </button>
                                                        </td>
                                                    </tr>
                                                    <!-- delete Modal -->
                                                    <div class="modal fade" id="deleteClient-{{ $client['id'] }}"
                                                        tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                                        aria-hidden="true">
                                                        <div class="modal-dialog" role="document">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <h5 class="modal-title" id="exampleModalLabel">Are you
                                                                        sure want to delete this Client?</h5>
                                                                    <button type="button" class="close"
                                                                        data-dismiss="modal" aria-label="Close">
                                                                        <span aria-hidden="true">&times;</span>
                                                                    </button>
                                                                </div>
                                                                <div class="modal-body">
                                                                    {{ $client['first_name'] }}
                                                                </div>
                                                                <div class="modal-footer">

                                                                    <form
                                                                        action="{{ route('clients.destroy', $client['id']) }}"
                                                                        method="POST">
                                                                        @method('delete')
                                                                        @csrf
                                                                        <button type="submit"
                                                                            class="btn btn-success">Confirm</button>
                                                                    </form>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                @endforeach()
                                            @endif()
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    </div>
@endsection()
