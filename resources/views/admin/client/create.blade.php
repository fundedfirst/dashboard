<?php
$title = 'Add Client';
?>
@extends('admin.layout.app')
@section('content')
    <?php
    $notificationService = app('App\Services\NotificationService');
    $helperService = app('App\Services\HelperService');
    $authUser = Auth()->user();
    ?>
    <style>
        .switch {
            position: relative;
            display: inline-block;
            width: 60px;
            height: 34px;
        }

        .switch input {
            opacity: 0;
            width: 0;
            height: 0;
        }

        .slider {
            position: absolute;
            cursor: pointer;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            background-color: #ccc;
            -webkit-transition: .4s;
            transition: .4s;
        }

        .slider:before {
            position: absolute;
            content: "";
            height: 26px;
            width: 26px;
            left: 4px;
            bottom: 4px;
            background-color: white;
            -webkit-transition: .4s;
            transition: .4s;
        }

        input:checked+.slider {
            background-color: #2196F3;
        }

        input:focus+.slider {
            box-shadow: 0 0 1px #2196F3;
        }

        input:checked+.slider:before {
            -webkit-transform: translateX(26px);
            -ms-transform: translateX(26px);
            transform: translateX(26px);
        }

        /* Rounded sliders */
        .slider.round {
            border-radius: 34px;
        }

        .slider.round:before {
            border-radius: 50%;
        }
    </style>


    <div class="main-panel">
        <div class="content">
            <div class="page-inner">
                <div class="page-header">
                    <h4 class="page-title">Clients</h4>
                    <ul class="breadcrumbs">
                        <li class="nav-home">
                            <a href="{{ route('admin.dashboard') }}">
                                <i class="flaticon-home"></i>
                            </a>
                        </li>
                        <li class="separator">
                            <i class="flaticon-right-arrow"></i>
                        </li>

                        <li class="nav-item">
                            <a href="javascript:;">Clients</a>
                        </li>
                    </ul>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <div class="card-title">Add New Client</div>
                            </div>
                            <div class="card-body">


                                @if ($message = Session::get('success'))
                                    <div class="alert alert-success alert-block" id="alert_success_session">
                                        <button type="button" class="close" data-dismiss="alert">×</button>
                                        <strong>{{ $message }}</strong>
                                    </div>
                                @endif
                                @if ($message = Session::get('error'))
                                    <div class="alert alert-danger alert-block">
                                        <button type="button" class="close" data-dismiss="alert">×</button>
                                        <strong>{{ $message }}</strong>
                                    </div>
                                @endif

                                @if ($errors->any())
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif


                                <form action="{{ route('clients.store') }}" method="POST" enctype="multipart/form-data">
                                    @csrf
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <input required class="form-control" type="text" name="first_name"
                                                    placeholder="First Name">
                                            </div>
                                            <div class="col-md-6">
                                                <input required class="form-control" type="text" name="last_name"
                                                    placeholder="Last Name">
                                            </div>

                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <input required class="form-control" type="email" name="email"
                                                    placeholder="Email">
                                            </div>
                                            <div class="col-md-6">
                                                <input required class="form-control" type="password" name="password"
                                                    placeholder="Password">
                                            </div>

                                        </div>
                                    </div>
                                    <div class="border my-3"></div>

                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <label for="plaid_public_key">Plaid Public Key</label>
                                                <input class="form-control" id="plaid_public_key" type="text"
                                                    name="plaid_public_key">
                                            </div>
                                            <div class="col-md-6">
                                                <label for="plaid_secret_key">Plaid Secret Key</label>
                                                <input class="form-control" id="plaid_secret_key" type="text"
                                                    name="plaid_secret_key">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <label for="denefit_token">Tier 2 (Funding Live Token)</label>
                                                <input class="form-control" id="denefit_token" type="text"
                                                    name="denefit_token">
                                            </div>
                                            <div class="col-md-6">
                                                <label for="plaid_public_key">Webhook Url</label>
                                                <input class="form-control" id="webhook_url" type="text"
                                                    name="webhook_url">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <label for="finmkt_user_id">Tier 1 User ID</label>
                                                <input class="form-control" id="finmkt_user_id" type="text"
                                                    name="finmkt_user_id">
                                            </div>
                                            <div class="col-md-6">
                                                <label for="finmkt_base_url">Tier 1 Base URL</label>
                                                <input class="form-control" id="finmkt_base_url" type="text"
                                                    name="finmkt_base_url">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <label for="finmkt_username">Tier 1 Username</label>
                                                <input class="form-control" id="finmkt_username" type="text"
                                                    name="finmkt_username">
                                            </div>
                                            <div class="col-md-6">
                                                <label for="finmkt_password">Tier 1 Password</label>
                                                <input class="form-control" id="finmkt_password" type="text"
                                                    name="finmkt_password">
                                            </div>
                                        </div>
                                    </div> <br>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <label>Enable for Dynamic Pricing</label> <br>
                                                <input type="hidden" name="dynamic_pricing" class="dynamic_pricing">
                                                <label class="switch">
                                                    <input id="dynamicPriceToggle" onclick="toggleDynamicPrice()"
                                                        type="checkbox">
                                                    <span class="slider round"></span>
                                                </label>
                                            </div>
                                            <div class="col-md-6">
                                                <label for="dynamic_key">Dynamic Price API key</label>
                                                <input class="form-control" id="dynamic_key" type="text"
                                                    name="dynamic_price_key" class="dynamic_price_key" readonly>
                                            </div>
                                        </div>
                                    </div><br>
                                    <div class="form-group mt-5">
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    </div>
    </div>

    <script>
        function generateKey(length) {
            var result = '';
            var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
            var charactersLength = characters.length;
            for (var i = 0; i < length; i++) {
                result += characters.charAt(Math.floor(Math.random() * charactersLength));
            }
            return result;
        }

        function getToggle1Value() {
            var toggleValue = document.getElementById('toggle1').checked ? 1 : 0;
            $(".tier1_toggle").val(toggleValue);
        }

        function getToggle2Value() {
            var toggleValue = document.getElementById('toggle2').checked ? 1 : 0;
            $(".tier2_toggle").val(toggleValue);
        }

        function toggleDynamicPrice() {
            var toggleValue = document.getElementById('dynamicPriceToggle').checked ? 1 : 0;
            $(".dynamic_pricing").val(toggleValue);

            console.log($("#dynamic_key").val().length)
            console.log($(".dynamic_pricing").val())

            var dynamicKeyValue = $("#dynamic_key").val();


            if (toggleValue && dynamicKeyValue.length < 1) {
                console.log("create Key")
                let key = generateKey(30);
                console.log(key)
                $("#dynamic_key").val(key)
                return
            }

            if (!toggleValue && dynamicKeyValue.length > 1) {
                console.log("remove Key")
                $("#dynamic_key").val("")
                return
            }

        }
    </script>
@endsection()
