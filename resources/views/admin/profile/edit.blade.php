<?php
   $title = "Edit My Profile";
   ?>
@extends('admin.layout.app')
@section('content')
<?php
   $notificationService = app('App\Services\NotificationService');
   $helperService = app('App\Services\HelperService');
   $authUser = Auth()->user();
   ?>
<div class="main-panel user-edit-bg">
    <div class="content">
        <div class="page-header bg-primary-gradient user-edit-bg">
            <div class="page-inner">
                <div class="page-header">
                    <h4 class="page-title">Profile</h4>
                    <ul class="breadcrumbs">
                        <li class="nav-home">
                            <a href="{{route('admin.dashboard')}}">
                                <i class="flaticon-home"></i>
                            </a>
                        </li>
                        <li class="separator">
                            <i class="flaticon-right-arrow"></i>
                        </li>
                        <li class="nav-item">
                            <a href="javascript:;">Profile</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="page-inner">
        <div class="row">
            <div class="col-lg-8">
                <div class="card">
                    <div class="card-header">
                        <div class="card-title">Edit Profile</div>
                    </div>
                    <div class="card-body">
                        @if ($message = Session::get('success'))
                        <div class="alert alert-success alert-block" id="alert_success_session">
                            <button type="button" class="close" data-dismiss="alert">×</button>
                            <strong>{{ $message }}</strong>
                        </div>
                        @endif
                        @if ($message = Session::get('error'))
                        <div class="alert alert-danger alert-block">
                            <button type="button" class="close" data-dismiss="alert">×</button>
                            <strong>{{ $message }}</strong>
                        </div>
                        @endif
                        @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                            </ul>
                        </div>
                        @endif
                        <form action="{{route('admin.updateMyProfile')}}" method="POST" enctype="multipart/form-data">
                            @method('put')
                            @csrf
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label for="first_name">First Name</label>
                                        <input required class="form-control" type="text" id="first_name" name="first_name" value="{{auth()->user()->first_name}}">
                                    </div>
                                    <div class="col-md-6">
                                        <label for="last_name">Last Name</label>
                                        <input required class="form-control" type="text" id="last_name" name="last_name" value="{{auth()->user()->last_name}}" >
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label for="email">Email</label>
                                        <input disabled required class="form-control" id="email" type="email" value="{{auth()->user()->email}}" >
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary">Update</button>
                            </div>
                       
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="card shadow-sm border-0 overflow-hidden">
                    <div class="card-body">
                        <div class="profile-avatar text-center">
                            @if(auth()->user()->profile_image)
                            <img src="{{auth()->user()->profile_image}}" class="rounded-circle shadow" width="120" height="120" alt="">
                            @else
                            <img src="{{asset('assets/admin/theme/img/avatar-1.png')}}" class="rounded-circle shadow" width="120" height="120" alt="">

                            @endif
                            <div class="file_upload text-center">
                                <input type="file" id="profile_image" name="profile_image">
                            </div>
                        </div>
                        </form>
                        <div class="d-flex align-items-center justify-content-around mt-5 gap-3">
                            <!-- <div class="text-center">
                                <h4 class="mb-0">45</h4>
                                <p class="mb-0 text-secondary">Friends</p>
                            </div>
                            <div class="text-center">
                                <h4 class="mb-0">15</h4>
                                <p class="mb-0 text-secondary">Photos</p>
                            </div>
                            <div class="text-center">
                                <h4 class="mb-0">86</h4>
                                <p class="mb-0 text-secondary">Comments</p>
                            </div> -->
                        </div>
                        <div class="text-center mt-4">
                            <h4 class="mb-1">{{auth()->user()->full_name}}</h4>
                            <!-- <p class="mb-0 text-secondary">Sydney, Australia</p> -->
                            <div class="mt-4"></div>
                            <!-- <h6 class="mb-1">HR Manager - Codervent Technology</h6> -->
                            <!-- <p class="mb-0 text-secondary">University of Information Technology</p> -->
                        </div>
                        <hr>
                        <div class="text-start">
                            <!-- <h5 class="">About</h5> -->
                            <!-- <p class="mb-0">It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem. -->
                            </p>
                        </div>
                    </div>
                    <!-- <ul class="list-group list-group-flush">
                        <li class="list-group-item d-flex justify-content-between align-items-center bg-transparent border-top">
                            Followers
                            <span class="badge bg-primary rounded-pill">95</span>
                        </li>
                        <li class="list-group-item d-flex justify-content-between align-items-center bg-transparent">
                            Following
                            <span class="badge bg-primary rounded-pill">75</span>
                        </li>
                        <li class="list-group-item d-flex justify-content-between align-items-center bg-transparent">
                            Templates
                            <span class="badge bg-primary rounded-pill">14</span>
                        </li>
                    </ul> -->
                </div>
            </div>
        </div>
    </div>
</div>
@endsection()