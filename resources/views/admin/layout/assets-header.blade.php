<!DOCTYPE html>
<html lang="en">

<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<title> Admin OCC - <?php echo ((isset($title)) ? $title : 'Home'); ?></title>
	<meta content='width=device-width, initial-scale=1.0, shrink-to-fit=no' name='viewport' />
	<link rel="icon" href="{{asset('assets/admin/theme/img/cc-logo.png')}}" type="image/x-icon" />
	<link rel="stylesheet" href="{{asset('assets/admin/custom.css')}}">
	<link rel="stylesheet" href="{{asset('assets/css/fonts.min.css')}}">
	<!-- Fonts and icons -->
	<script src="{{asset('assets/admin/theme/js/plugin/webfont/webfont.min.js')}}"></script>
	<script src="{{asset('assets/admin/theme/js/clipboard.min.js')}}"></script>
	<script>
		WebFont.load({
			google: {
				"families": ["Lato:300,400,700,900"]
			},
			custom: {
				"families": ["Flaticon", "Font Awesome 5 Solid", "Font Awesome 5 Regular", "Font Awesome 5 Brands", "simple-line-icons"],
				urls: ['../assets/css/fonts.min.css']
			},
			active: function() {
				sessionStorage.fonts = true;
			}
		});
	</script>

	<!-- CSS Files -->
	<link rel="stylesheet" href="{{asset('assets/admin/theme/css/bootstrap.min.css')}}">
	<link rel="stylesheet" href="{{asset('assets/admin/theme/css/atlantis.min.css')}}">

	<!-- CSS Just for demo purpose, don't include it in your project -->
	<link rel="stylesheet" href="{{asset('assets/admin/theme/css/demo.css')}}">
</head>
<style>
	.logo-header {
		box-shadow: none;
		border-bottom:none;
	}
	.btn-lg{
		border-radius:30px;
	}
</style>
<!-- denifit_dashboard start -->
<body class="denifit_dashboard page-not-found">

	<div class="main-header">
		<!-- Logo Header -->
		<div class="logo-header" data-background-color="blue">
			<a href="{{route('admin.dashboard')}}" class="logo">
				<img src="{{asset('assets/admin/theme/img/cc-logo.png')}}" alt="navbar brand" class="navbar-brand" width="66px">
			</a>
		</div>
		<!-- End Logo Header -->

		<!-- Navbar Header -->
		<nav class="navbar navbar-header navbar-expand-lg" data-background-color="blue2">
			<div class="container-fluid">
				<ul class="navbar-nav topbar-nav ml-md-auto align-items-center">
					<li class="nav-item dropdown hidden-caret">
						<a class="dropdown-toggle profile-pic" data-toggle="dropdown" href="#" aria-expanded="false">
							<div class="avatar-sm">
								<img src="{{asset('assets/admin/theme/img/profile_placeholder.png')}}" alt="..." class="avatar-img rounded-circle">
							</div>
						</a>
						<ul class="dropdown-menu dropdown-user animated fadeIn">
							<div class="dropdown-user-scroll scrollbar-outer">
								<li>
									<div class="user-box">
										<div class="avatar-lg"><img src="{{asset('assets/admin/theme/img/profile_placeholder.png')}}" alt="image profile" class="avatar-img rounded"></div>
										<div class="u-text">
											<h4>{{auth()->user()->full_name}}</h4>
											<p class="text-muted">{{auth()->user()->email}}</p><a href="{{route('admin.editMyProfile')}}" class="btn btn-xs btn-secondary btn-sm">Edit Profile</a>
										</div>
									</div>
								</li>
								<li>
									<div class="dropdown-divider"></div>
									<a class="dropdown-item" href="{{route('admin.myProfile')}}">My Profile</a>
									<a class="dropdown-item" href="{{route('admin.logout')}}">Logout</a>
								</li>
							</div>
						</ul>
					</li>
				</ul>
			</div>
		</nav>
	</div>