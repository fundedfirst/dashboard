<script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>
<script>
	var appUrl = '<?php echo url("/"); ?>';
</script>
<script src="{{asset('assets/admin/custom.js')}}"></script>
<script src="//cdn.ckeditor.com/4.16.2/standard/ckeditor.js"></script>
<script>


	$('.clickcopyborad').click(function() {
		
		/* Get the text field */
	var copyText = $(this);

	var $temp = $("<input>");
  $("body").append($temp);
  $temp.val($(copyText).val()).select();
  document.execCommand("copy");	
 
	});

</script>
</div>
</div>

<!--   Core JS Files   -->
<script src="{{asset('assets/admin/theme/js/core/jquery.3.2.1.min.js')}}"></script>
<script src="{{asset('assets/admin/theme/js/core/popper.min.js')}}"></script>
<script src="{{asset('assets/admin/theme/js/core/bootstrap.min.js')}}"></script>

<!-- jQuery UI -->
<script src="{{asset('assets/admin/theme/js/plugin/jquery-ui-1.12.1.custom/jquery-ui.min.js')}}"></script>
<script src="{{asset('assets/admin/theme/js/plugin/jquery-ui-touch-punch/jquery.ui.touch-punch.min.js')}}"></script>

<!-- jQuery Scrollbar -->
<script src="{{asset('assets/admin/theme/js/plugin/jquery-scrollbar/jquery.scrollbar.min.js')}}"></script>


<!-- Chart JS -->
<script src="{{asset('assets/admin/theme/js/plugin/chart.js/chart.min.js')}}"></script>

<!-- jQuery Sparkline -->
<script src="{{asset('assets/admin/theme/js/plugin/jquery.sparkline/jquery.sparkline.min.js')}}"></script>

<!-- Chart Circle -->
<script src="{{asset('assets/admin/theme/js/plugin/chart-circle/circles.min.js')}}"></script>

<!-- Datatables -->
<script src="{{asset('assets/admin/theme/js/plugin/datatables/datatables.min.js')}}"></script>

<!-- Bootstrap Notify -->
<script src="{{asset('assets/admin/theme/js/plugin/bootstrap-notify/bootstrap-notify.min.js')}}"></script>

<!-- jQuery Vector Maps -->

<!-- Sweet Alert -->
<script src="{{asset('assets/admin/theme/js/plugin/sweetalert/sweetalert.min.js')}}"></script>

<!-- Atlantis JS -->
<script src="{{asset('assets/admin/theme/js/atlantis.min.js')}}"></script>

<!-- Atlantis DEMO methods, don't include it in your project! -->
<script src="{{asset('assets/admin/theme/js/setting-demo.js')}}"></script>
<script src="{{asset('assets/admin/theme/js/demo.js')}}"></script>
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/clipboard.js/2.0.0/clipboard.min.js"></script> -->
<script>
	$(document).ready(function() {
		$('#basic-datatables').DataTable({});
	});
	// for copy text
	$( document ).ready(function() {

	new ClipboardJS('.btn-clipboard');

	});
	$(function(){
        $(document).on('click','iframe',function(){ this.select(); });
    });
	
</script>
<script type="text/javascript">
function select_all()
	{
		var text_val = document.getElementById('t1');
		text_val.focus();
		text_val.select();
		document.execCommand("Copy");
	}
</script>
</body>

</html>