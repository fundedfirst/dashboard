<!DOCTYPE html>
<html lang="en">

<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<title> Admin OCC - <?php echo ((isset($title)) ? $title : 'Home'); ?></title>
	<meta content='width=device-width, initial-scale=1.0, shrink-to-fit=no' name='viewport' />
	<link rel="icon" href="{{asset('assets/admin/theme/img/cc-logo.png')}}" type="image/x-icon" />
	<link rel="stylesheet" href="{{asset('assets/admin/custom.css')}}">
	<link rel="stylesheet" href="{{asset('assets/css/fonts.min.css')}}">
	<link href="{{asset('assets/admin/theme/css/the-datepicker.css')}}" rel="stylesheet">
	<!-- Fonts and icons -->
	<script src="{{asset('assets/admin/theme/js/plugin/webfont/webfont.min.js')}}"></script>
	<script src="{{asset('assets/admin/theme/js/clipboard.min.js')}}"></script>
	<script src="cdn.datatables.net/1.13.2/css/jquery.dataTables.min.css"></script>
	<script>
		WebFont.load({
			google: {
				"families": ["Lato:300,400,700,900"]
			},
			custom: {
				"families": ["Flaticon", "Font Awesome 5 Solid", "Font Awesome 5 Regular", "Font Awesome 5 Brands", "simple-line-icons"],
				urls: ['../assets/css/fonts.min.css']
			},
			active: function() {
				sessionStorage.fonts = true;
			}
		});
	</script>

	<!-- CSS Files -->
	<link rel="stylesheet" href="{{asset('assets/admin/theme/css/bootstrap.min.css')}}">
	<link rel="stylesheet" href="{{asset('assets/admin/theme/css/atlantis.min.css')}}">

	<!-- CSS Just for demo purpose, don't include it in your project -->
	<link rel="stylesheet" href="{{asset('assets/admin/theme/css/demo.css')}}">
</head>
<!-- denifit_dashboard start -->
<body class="denifit_dashboard">

	<div class="main-header">
		<!-- Logo Header -->
		<div class="logo-header" data-background-color="blue">
			<a href="{{route('admin.dashboard')}}" class="logo">
				<img src="{{asset('assets/admin/theme/img/cc-logo.png')}}" alt="navbar brand" class="navbar-brand" width="66px">
			</a>
			
		</div>
		<!-- End Logo Header -->

		<!-- Navbar Header -->
		<nav class="navbar navbar-header navbar-expand-lg" data-background-color="blue2">
			<div class="container-fluid">
				<div class="collapse" id="search-nav">
					<!-- <form class="navbar-left navbar-form nav-search mr-md-3">
						<div class="input-group">
							<div class="input-group-prepend">
								<button type="submit" class="btn btn-search pr-1">
									<i class="fa fa-search search-icon"></i>
								</button>
							</div>
							<input type="text" placeholder="Type here to search" class="form-control">
						</div>
					</form> -->
				</div>
				<ul class="navbar-nav topbar-nav ml-md-auto align-items-center">
					<li class="nav-item toggle-nav-search hidden-caret">
						<a class="nav-link" data-toggle="collapse" href="#search-nav" role="button" aria-expanded="false" aria-controls="search-nav">
							<i class="fas fa-search"></i>
						</a>
					</li>

					<!-- <li class="nav-item dropdown hidden-caret">
						<a class="nav-link dropdown-toggle" href="#" id="notifDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							<i class="fa fa-bell"></i>
							<span class="notification">4</span>
						</a>
						<ul class="dropdown-menu notif-box animated fadeIn" aria-labelledby="notifDropdown">
							<li>
								<div class="dropdown-title">No new notification</div>
							</li>

							<li>
								<a class="see-all" href="javascript:void(0);">See all notifications<i class="fa fa-angle-right"></i> </a>
							</li>
						</ul>
					</li> -->
					<!-- <li class="nav-item dropdown hidden-caret">
						<a class="nav-link" data-toggle="dropdown" href="#" aria-expanded="false">
							<i class="fas fa-layer-group"></i>
						</a>
						<div class="dropdown-menu quick-actions quick-actions-info animated fadeIn">
							<div class="quick-actions-header">
								<span class="title mb-1">Quick Actions</span>
								<span class="subtitle op-8">Shortcuts</span>
							</div>
							<div class="quick-actions-scroll scrollbar-outer">
								<div class="quick-actions-items">
									<div class="row m-0">
										<a class="col-6 col-md-4 p-0" href="">
											<div class="quick-actions-item">
												<i class="flaticon-pen"></i>
												<span class="text">Create Client</span>
											</div>
										</a>
									</div>
								</div>
							</div>
						</div>
					</li> -->
					<li class="nav-item dropdown hidden-caret">
						<a class="dropdown-toggle profile-pic" data-toggle="dropdown" href="#" aria-expanded="false">
							<div class="avatar-sm">
							@if(auth()->user()->profile_image)
                            <img src="{{auth()->user()->profile_image}}" class="avatar-img rounded-circle">
                            @else
							<img src="{{asset('assets/admin/theme/img/profile_placeholder.png')}}" alt="..." class="avatar-img rounded-circle">

                            @endif
							</div>
						</a>
						<ul class="dropdown-menu dropdown-user animated fadeIn">
							<div class="dropdown-user-scroll scrollbar-outer">
								<li>
									<div class="user-box">
										<div class="avatar-lg"><img src="{{asset(auth()->user()->profile_image ?? 'assets/admin/theme/img/profile_placeholder.png')}}" alt="image profile" class="avatar-img rounded"></div>
										<div class="u-text">
											<h4>{{auth()->user()->full_name}}</h4>
											<p class="text-muted">{{auth()->user()->email}}</p><a href="{{route('admin.editMyProfile')}}" class="btn btn-xs btn-secondary btn-sm">Edit Profile</a>
										</div>
									</div>
								</li>
								<li>
									<div class="dropdown-divider"></div>
									<a class="dropdown-item" href="{{route('admin.myProfile')}}">My Profile</a>
									<a class="dropdown-item" href="{{route('admin.logout')}}">Logout</a>
								</li>
							</div>
						</ul>
					</li>
				</ul>
			</div>
		</nav>
		<!-- End Navbar -->
	</div>

	<!-- Sidebar -->
	<div class="sidebar sidebar-style-2">
		<div class="sidebar-wrapper scrollbar scrollbar-inner">
			<div class="sidebar-content">
				<div class="user">
					<div class="avatar-sm float-left mr-2">
						@if(auth()->user()->profile_image)
                        <img src="{{auth()->user()->profile_image}}" class="avatar-img rounded-circle">
                        @else
						<img src="{{asset('assets/admin/theme/img/profile_placeholder.png')}}" alt="..." class="avatar-img rounded-circle">
                        @endif

					</div>
					<div class="info">
						<a data-toggle="collapse" href="#collapseExample" aria-expanded="true">
							<span>
								{{auth()->user()->full_name}}
								<span class="user-level">Administrator</span>
								<span class="caret"></span>
							</span>
						</a>
						<div class="clearfix"></div>
						<div class="collapse in" id="collapseExample">
							<ul class="nav">
								<li>
									<a href="{{route('admin.myProfile')}}">
										<span class="link-collapse">My Profile</span>
									</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<ul class="nav nav-primary">
					<li class="nav-item active">
						<a data-toggle="collapse" href="#dashboard" class="collapsed" aria-expanded="false">
							<i class="fas fa-home"></i>
							<p>Dashboard</p>
							<span class="caret"></span>
						</a>
						<div class="collapse" id="dashboard">
							<ul class="nav nav-collapse">

								<li>
									<a href="{{route('admin.dashboard')}}">
										<span class="sub-item">Dashboard</span>
									</a>
								</li>
							</ul>
						</div>
					</li>
					<!-- <li class="nav-section">
						<span class="sidebar-mini-icon">
							<i class="fa fa-ellipsis-h"></i>
						</span>
						<h4 class="text-section">Clients</h4>
					</li> -->
					<li class="nav-item">
						<a data-toggle="collapse" href="#forms">
							<i class="fas fa-users"></i>
							<p>Clients</p>
							<span class="caret"></span>
						</a>
						<div class="collapse" id="forms">
							<ul class="nav nav-collapse">
								<li style="list-style-type: none;">
									<a href="{{route('clients.index')}}">
										<span class="sub-item">All Clients</span>
									</a>
								</li>
								<li>
									<a href="{{route('clients.create')}}">
										<span class="sub-item">Add Client</span>
									</a>
								</li>
							</ul>
						</div>
					</li>
					@if(auth()->user()->role == 'super_admin')
					<li class="nav-item">
						<a data-toggle="collapse" href="#admins">
							<i class="fas fa-users"></i>
							<p>Sub Admins</p>
							<span class="caret"></span>
						</a>
						<div class="collapse" id="admins">
							<ul class="nav nav-collapse">
								<li style="list-style-type: none;">
									<a href="{{route('admin.subAdmins')}}">
										<span class="sub-item">All Admins</span>
									</a>
								</li>
								<li>
									<a href="{{route('admin.createAdmin')}}">
										<span class="sub-item">Add Admin</span>
									</a>
								</li>
							</ul>
						</div>
					</li>
					@endif()
				</ul>
			</div>
		</div>
	</div>
	<!-- End Sidebar -->