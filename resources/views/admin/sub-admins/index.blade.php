<?php
   $title = "Sub Admins";
   ?>
@extends('admin.layout.app')
@section('content')
<?php
   $notificationService = app('App\Services\NotificationService');
   $helperService = app('App\Services\HelperService');
   $authUser = Auth()->user();
   ?>
<div class="main-panel">
   <div class="content">
      <div class="page-inner">
         <div class="page-header">
            <h4 class="page-title">Sub Admins</h4>
            <ul class="breadcrumbs">
               <li class="nav-home">
                  <a href="{{route('admin.dashboard')}}">
                     <i class="flaticon-home"></i>
                  </a>
               </li>
               <li class="separator">
                  <i class="flaticon-right-arrow"></i>
               </li>
               <li class="nav-item">
                  <a href="javascript:;">Sub Admins</a>
               </li>
            </ul>
         </div>
         <div class="row">
            <div class="col-md-12">
               <div class="card">
                  <div class="card-header">
                     <div class="">All Sub Admins</div>
                  </div>
                  <div class="card-body">
                     @if ($message = Session::get('success'))
                     <div class="alert alert-success alert-block" id="alert_success_session">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <strong>{{ $message }}</strong>
                     </div>
                     @endif
                     @if ($message = Session::get('error'))
                     <div class="alert alert-danger alert-block">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <strong>{{ $message }}</strong>
                     </div>
                     @endif
                     @if ($errors->any())
                     <div class="alert alert-danger">
                        <ul>
                           @foreach ($errors->all() as $error)
                           <li>{{ $error }}</li>
                           @endforeach
                        </ul>
                     </div>
                     @endif
                     <div class="row  align-items-center">
                        <div class="col-lg-12">
                           <div class="table-responsive">
                              <table id="active_table" class="table align-middle all-plan-table mb-0">
                                 <thead  class="table-secondary">
                                    <tr>
                                       <th>Name</th>
                                       <th>Email</th>
                                       <th>Status</th>
                                       <th>Actions</th>
                                    </tr>
                                 </thead>
                                 <tbody>
                                 @if(count($data['subAdmins']))
                                 @foreach($data['subAdmins'] as $index => $admin)
                                    <tr>
                                       <td>
                                       {{$admin->full_name}}
                                       </td>
                                       <td>
                                       {{$admin->email}}
                                       </td>
                                       <td>
                                       {{$admin->status}}
                                       </td>
                                       <td>
                                    <a href="{{route('admin.editAdmin', $admin->id)}}"><i class="far fa-edit"></i></a>
                                       </td>
                                       
                                    </tr>
                                    @endforeach
                                    @endif()
                                 </tbody>
                              </table>
                              {{$data['subAdmins']->links()}}
                           </div>
                        </div>
                        <!-- delete Modal -->
                       
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
@endsection()