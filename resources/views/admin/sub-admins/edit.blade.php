<?php
$title = "Edit Sub Admin";
?>
@extends('admin.layout.app')
@section('content')

<?php
// print_r($data);die;
$notificationService = app('App\Services\NotificationService');
$helperService = app('App\Services\HelperService');
$authUser = Auth()->user();
?>


<div class="main-panel">
    <div class="content">
        <div class="page-inner">
            <div class="page-header">
                <h4 class="page-title">Sub Admin</h4>
                <ul class="breadcrumbs">
                    <li class="nav-home">
                        <a href="{{route('admin.dashboard')}}">
                            <i class="flaticon-home"></i>
                        </a>
                    </li>
                    <li class="separator">
                        <i class="flaticon-right-arrow"></i>
                    </li>

                    <li class="nav-item">
                        <a href="javascript:;">Sub Admin</a>
                    </li>
                </ul>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="card-title">Edit Sub Admin</div>
                        </div>
                        <div class="card-body">


                            @if ($message = Session::get('success'))
                            <div class="alert alert-success alert-block" id="alert_success_session">
                                <button type="button" class="close" data-dismiss="alert">×</button>
                                <strong>{{ $message }}</strong>
                            </div>
                            @endif
                            @if ($message = Session::get('error'))
                            <div class="alert alert-danger alert-block">
                                <button type="button" class="close" data-dismiss="alert">×</button>
                                <strong>{{ $message }}</strong>
                            </div>
                            @endif

                            @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                            @endif
                            <form action="{{route('admin.updateAdmin')}}" method="POST" enctype="multipart/form-data">
                                @method('put')
                                @csrf
                                <input type="hidden" name="id" value="{{$data->id}}">
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-5">
                                        <label for="first_name">First Name</label>
                                            <input required class="form-control" type="text" id="first_name" name="first_name" value="{{$data->first_name}}">
                                        </div>
                                        <div class="col-md-5">
                                        <label for="last_name">Last Name</label>
                                            <input required class="form-control" type="text" id="last_name" name="last_name" value="{{$data->last_name}}" >
                                        </div>

                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                    <div class="col-md-5">
                                            <label for="status">Status</label>
                                            <select class="form-control" name="status">
                                                <option value="active" <?php if($data->status == 'Active'){ echo 'selected'; } ?> >Active</option>
                                                <option value="inactive" <?php if($data->status == 'Inactive'){ echo 'selected'; } ?> >In Active</option>
                                            </select>
                                        </div>
                                   
                                       
                                        

                                    </div>
                                </div>
                                <button type="submit" class="btn btn-primary">Update</button>
                            </form>

                        </div>
                        
                     



                        </div>

                    </div>
                </div>

            </div>

        </div>
    </div>

</div>
</div>
</div>

@endsection()