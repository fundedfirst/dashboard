<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <link rel="icon" href="{{asset('assets/admin/theme/img/cc-logo.png')}}" type="image/x-icon" />
        <title>{{'OCC Password Reset'}}</title>

        <link rel="stylesheet" href="{{asset('assets/admin/custom.css')}}">
        <link rel="dns-prefetch" href="https://fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,600" rel="stylesheet" type="text/css">
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap">

        <!-- Styles -->
        <!-- <link rel="stylesheet" href="{{ asset('css/app.css') }}"> -->

        <!-- Scripts -->
        <script src="{{ asset('js/app.js') }}" defer></script>
    </head>
    <body>
        <div class="login_form" style="min-height:100vh">
            <div class="container authentication-card">
                <div class="row ustify-content-center align-items">
                    <div class="col-lg-7 col-md-6 col-sm-12 col-xs-12">
                        <img src="{{asset('assets/admin/theme/img/forgot-password-frent-img.jpg')}}" alt="login-img" width="100%">
                    </div>
                    <div class="col-lg-5 col-md-6 col-sm-12 col-xs-12" style="border-left:1px dashed #0259e6">
                        <div class="card">
                            <div class="card-body forgot-passowrd">
                                <div class="font-sans text-gray-900 antialiased">
                                    {{ $slot }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
