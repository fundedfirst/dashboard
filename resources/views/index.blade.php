<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>OCC - Login</title>
    <link rel="icon" href="{{ asset('assets/admin/theme/img/cc-logo.png') }}" type="image/x-icon" />
    <link rel="stylesheet" href="{{ asset('assets/admin/custom.css') }}">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
</head>

<body>
    <div class="admin-login login_form">
        <div class="container authentication-card" style="background-color:#fff">
            <div class="row align-items">
                <div class="col-lg-7">
                    <img src="{{ asset('assets/admin/theme/img/auth-img-register3.png') }}" alt="login-img"
                        width="100%">
                </div>
                <div class="col-lg-5 col-left-patteren">
                    <div class="card">
                        <div class="">
                            <h2>Sign In</h2>
                            <p>See your growth and get consulting support!</p>
                        </div>
                        <div class="login-separater text-center">
                            <span>Admin & Client Login</span>
                            <hr>
                        </div>
                        <div class="">
                            <a href="{{ route('admin.login') }}" class="card-link admin-link-btn">
                                Admin Login
                            </a>
                            <a href="{{ route('client.login') }}" class="card-link client-link-btn">
                                Client Login
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</body>

</html>
