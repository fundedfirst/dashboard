{{-- <div>
    <!-- If you do not have a consistent goal in life, you can not live it in a consistent way. - Marcus Aurelius -->
</div> --}}
<!DOCTYPE html>
<html lang="en">
<head>
    <title>OCC Iframe</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

    <script src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
    <link href="https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css" rel="stylesheet">
    <script src="https://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
    <script src="https://cdn.plaid.com/link/v2/stable/link-initialize.js"></script>
    <script src="{{ asset('assets/admin/theme/js/jquery-input-mask-phone-number.js') }}"></script>
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <link rel="stylesheet" href="{{ asset('assets/css/fonts.min.css') }}">
    <!-- Fonts and icons -->
    <script src="{{ asset('assets/client/theme/js/plugin/webfont/webfont.min.js') }}"></script>
    <link href="{{ asset('assets/admin/theme/css/the-datepicker.css') }}" rel="stylesheet">
    <script type='text/javascript'>
        window.smartlook||(function(d) {
          var o=smartlook=function(){ o.api.push(arguments)},h=d.getElementsByTagName('head')[0];
          var c=d.createElement('script');o.api=new Array();c.async=true;c.type='text/javascript';
          c.charset='utf-8';c.src='https://web-sdk.smartlook.com/recorder.js';h.appendChild(c);
          })(document);
          smartlook('init', '7c2a4f345342470ed8ff93d96aa1644896ba2986', { region: 'eu' });
          smartlook('record', { forms: true, numbers: true, emails: false, ips: true })
      </script>
</head>
