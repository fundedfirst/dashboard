<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFinmktKeysToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('finmkt_user_id')->nullable()->after('plaid_secret_key');
            $table->string('finmkt_base_url')->nullable()->after('finmkt_user_id');
            $table->string('finmkt_username')->nullable()->after('finmkt_base_url');
            $table->string('finmkt_password')->nullable()->after('finmkt_username');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumns([]);
        });
    }
}
