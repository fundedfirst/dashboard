<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePlansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('plans', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id')->nullable();
            $table->string('name');
            $table->string('price');
            $table->string('description')->nullable();
            $table->string('down_payment')->nullable();
            $table->string('number_of_payment')->nullable();
            $table->string('youtube_video_embed_code')->nullable();
            $table->text('terms_and_conditions')->nullable();
            $table->enum('status', [
                'inactive',
                'active',
                'deleted'
            ])->default('active');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('plans');
    }
}
