<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterUserTableDynamicPricing extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->boolean('is_dynamic_price_enabled')->default(false);
            $table->string('dynamic_price_key')->nullable()->uniqid();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('users', function (Blueprint $table) {
            //
            if (Schema::hasTable('users')) {

                if (Schema::hasColumn('users', 'is_dynamic_price_enabled')) {
                    $table->dropColumn('is_dynamic_price_enabled');
                }
                if (Schema::hasColumn('users', 'dynamic_price_key')) {
                    $table->dropColumn('dynamic_price_key');
                }
            }
        });
    }
}
