<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterTableAddColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('plans', function (Blueprint $table) {
            //
            $table->text('success_url')->nullable(true);
            // $table->text('vsl_link')->nullable(true);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('plans', function (Blueprint $table) {
            //
            if(Schema::hasTable('plans')){
                // if(Schema::hasColumn('plans','vsl_link')){
                //     $table->dropColumn('vsl_link');
                // }
                if(Schema::hasColumn('plans','success_url')){
                    $table->dropColumn('success_url');
                }
            }
        });
    }
}
