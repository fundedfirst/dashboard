<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_details', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id')->nullable();
            $table->string('email')->unique();
            $table->string('country')->nullable();
            $table->string('state')->nullable();
            $table->string('city')->nullable();
            $table->string('address')->nullable();
            $table->string('zip_code')->nullable();
            $table->string('ssn')->nullable();
            $table->string('company_name')->nullable();
            $table->string('ein_number')->nullable();
            $table->string('state_of_incorporation')->nullable();
            $table->string('offer_name')->nullable();
            $table->string('offer_desc')->nullable();
            $table->string('offer_price')->nullable();
            $table->string('offer_down_payment')->nullable();
            $table->string('optin')->nullable();
            $table->string('vsl')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_details');
    }
}
