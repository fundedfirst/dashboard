<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->boolean('tier1_toggled')->default(true);
            $table->boolean('tier2_toggled')->default(true);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('users', function (Blueprint $table) {
            //
            if (Schema::hasTable('users')) {

                if (Schema::hasColumn('users', 'tier1_toggled')) {
                    $table->dropColumn('tier1_toggled');
                }
                if (Schema::hasColumn('users', 'tier2_toggled')) {
                    $table->dropColumn('tier2_toggled');
                }
            }
        });
    }
}
