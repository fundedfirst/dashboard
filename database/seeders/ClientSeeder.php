<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class ClientSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'role_id'       => 3,
            'first_name'    => 'Zasmine',
            'last_name'     => 'zendes',
            'status'        => 'active',
            'email'         => 'zazzyjmendes@gmail.com',
            'password'      => Hash::make('12345678'),
        ]);
    }
}
