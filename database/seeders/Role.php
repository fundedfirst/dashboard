<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;
class Role extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $currentDateAndTime = date('Y-m-d H:i:s');
        $data = [
            ['name'=>'super_admin',   'status'=> 'active', 'created_at'=>$currentDateAndTime, 'updated_at'=>$currentDateAndTime],
            ['name'=>'admin',   'status'=> 'active', 'created_at'=>$currentDateAndTime, 'updated_at'=>$currentDateAndTime],
            ['name'=>'client',  'status'=> 'active', 'created_at'=>$currentDateAndTime, 'updated_at'=>$currentDateAndTime],            
        ];
        $add = DB::table('roles')->insert($data);
    }
}
