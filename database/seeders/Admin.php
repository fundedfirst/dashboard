<?php

namespace Database\Seeders;

use App\Models\Role;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class Admin extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //find admin role
        $roles =  Role::whereName('super_admin')->first();
        if (!$roles) {
            //create admin role
            $adminDataToAdd['name']     =   'super_admin';
            $adminDataToAdd['status']   =   'active';
            $roles = Role::create($adminDataToAdd);
        }
        $adminRoleId = $roles->id;
       User::create([
            'role_id'       => $adminRoleId,
            'first_name'    => 'Super Admin',
            'last_name'     => 'Denefit',
            'status'        => 'active',
            'email'         => 'admin@yopmail.com',
            'password'      => Hash::make('admin123'),
        ]);
    }
}
